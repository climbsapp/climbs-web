import { defineMessages } from 'react-intl';

export const messages = defineMessages({
  valueGenderMale: {
    id: 'view.profile.modify.value.gender.male',
    defaultMessage: 'Male'
  },
  valueGenderFemale: {
    id: 'view.profile.modify.value.gender.female',
    defaultMessage: 'Female'
  },
  valueGenderOther: {
    id: 'view.profile.modify.value.gender.other',
    defaultMessage: 'Other'
  },
  dateFormat: {
    id: 'view.form.dateFormat',
    defaultMessage: 'MM/DD/YYYY'
  }
});

export function getGenderOptions(formatMessage) {
  return [
    { value: 'MALE', label: formatMessage(messages.valueGenderMale) },
    { value: 'FEMALE', label: formatMessage(messages.valueGenderFemale) },
    { value: 'OTHER', label: formatMessage(messages.valueGenderOther) }
  ];
};

export const datePickerProps = {

};