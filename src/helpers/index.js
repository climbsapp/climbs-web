export function increaseBrightness(hex, percent) {
  // strip the leading # if it's there
  hex = hex.replace(/^\s*#|\s*$/g, '');

  // convert 3 char codes --> 6, e.g. `E0F` --> `EE00FF`
  if (hex.length === 3){
      hex = hex.replace(/(.)/g, '$1$1');
  }

  const r = parseInt(hex.substr(0, 2), 16),
        g = parseInt(hex.substr(2, 2), 16),
        b = parseInt(hex.substr(4, 2), 16);

  const luminance = (0.2126*r + 0.7152*g + 0.0722*b);
  if (luminance > 200) {
    return '#' +
       ((0|(1<<8) + r + (256 + r) * percent / 100).toString(16)).substr(1) +
       ((0|(1<<8) + g + (256 + g) * percent / 100).toString(16)).substr(1) +
       ((0|(1<<8) + b + (256 + b) * percent / 100).toString(16)).substr(1);
  }

  return '#' +
     ((0|(1<<8) + r + (256 - r) * percent / 100).toString(16)).substr(1) +
     ((0|(1<<8) + g + (256 - g) * percent / 100).toString(16)).substr(1) +
     ((0|(1<<8) + b + (256 - b) * percent / 100).toString(16)).substr(1);
}

export function dateToDateString(date) {
  const year = date.getFullYear();
  const month = zeroPadDate(date.getMonth() + 1);
  const day = zeroPadDate(date.getDate());

  return year + '-' + month + '-' + day;
}

export function dateToTimeString(date) {
  const hours = zeroPadDate(date.getHours());
  const minutes = zeroPadDate(date.getMinutes());
  return hours + ':' + minutes;
}

export function durationString(intl, commonMessages, startDate, endDate) {
  const { formatDate, formatMessage, formatTime } = intl;
  const startDateString = formatDate(startDate, { year: 'numeric', month: 'numeric', day: 'numeric' });
  const endDateString = formatDate(endDate, { year: 'numeric', month: 'numeric', day: 'numeric' });
  const startTime = formatTime(startDate, { hour: 'numeric', minute: 'numeric' });
  const endTime = formatTime(endDate, { hour: 'numeric', minute: 'numeric' });

  // Is same date?
  if (startDateString === endDateString) {
      return formatMessage(commonMessages.sameDateDurationString, { date: startDateString, startTime: startTime, endTime: endTime });
    } else {
      return formatMessage(commonMessages.normalDurationString, { startDate: startDateString, startTime: startTime, endDate: endDateString, endTime: endTime });
    }
}

const minuteMillis = 60 * 1000;
const hourMillis = 60 * minuteMillis;
const dayMillis = 24 * hourMillis;

export function dateDiffString(startDate, endDate) {
  let diffMillis = endDate.getTime() - startDate.getTime();
  let diffString = '';
  if (diffMillis > dayMillis) {
    const days = Math.floor(diffMillis / dayMillis);
    diffString += days + 'd ';
    diffMillis -= (days * dayMillis);
  }

  if (diffMillis > hourMillis) {
    const hours = Math.floor(diffMillis / hourMillis);
    diffString += hours + 'h ';
    diffMillis -= (hours * hourMillis);
  }

  if (diffMillis > minuteMillis) {
    const minutes = Math.floor(diffMillis / minuteMillis);
    diffString += minutes + 'min';
    diffMillis -= (minutes * minuteMillis);
  }

  return diffString;
}

function zeroPadDate(date) {
  return ('0' + date).slice(-2);
}
