import { increaseBrightness } from './';

export function getColorStyle(color) {
  const { hex, hex2, hex3 } = color;

  let background;
  let border;

  if (!hex) {
    background = '';
    border = '#333';
  } else if (!hex2 && !hex3) {
    background = `#${hex}`;
    border = `#${hex}`;
  } else if (hex2 && !hex3) {
    background = `linear-gradient(135deg, #${hex} 0%, #${hex} 50%, #${hex2} 51%, #${hex2} 100%)`;
    border = '#333';
  } else if (hex2 && hex3) {
    background = `linear-gradient(135deg, #${hex} 0%, #${hex} 33%, #${hex2} 34%, #${hex2} 66%, #${hex3} 67%, #${hex3} 100%)`;
    border = '#333';
  }

  const requiresDarkBorder = (hex && hex.toLowerCase() === 'ffffff')
    || (hex2 && hex2.toLowerCase() === 'ffffff')
    || (hex3 && hex3.toLowerCase() === 'ffffff');
  return {
    border: requiresDarkBorder ? '1px solid #333' : '1px solid ' + border,
    background: background,
    color: hex ? increaseBrightness('#' + hex, 75) : '#333'
  };
}