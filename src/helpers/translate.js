import { defineMessages } from 'react-intl';

export function translate(catalog) {
  return (key) => {
    return catalog[key];
  };
}

function resolveLanguageFromBrowser() {
  if (navigator && navigator.language) {
    let language = navigator.language;
    if (language.indexOf('-')) {
      language = language.split('-')[0];
    }

    return language.toLowerCase();
  } else {
    return null;
  }
}

/**
 * Returns users language.
 */
export function defaultLanguage() {
  if (localStorage.getItem('language') !== null) {
    return localStorage.getItem('language');
  }

  const navigatorLanguage = resolveLanguageFromBrowser();
  if (['en', 'fi', 'cs'].indexOf(navigatorLanguage) > -1) {
    return navigatorLanguage;
  }

  return 'en';
}

export function saveLanguage(language) {
  // Validate?
  localStorage.setItem('language', language);
}

export const commonMessages = defineMessages({
  sameDateDurationString: {
    id: 'common.duration.samedate',
    defaultMessage: '{date} {startTime} to {endTime}'
  },
  normalDurationString: {
    id: 'common.duration.normal',
    defaultMessage: '{startDate} {startTime} to {endDate} {endTime}'
  }
});
