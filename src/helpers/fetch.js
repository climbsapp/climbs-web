export const JWT_TOKEN = 'jwtToken';
export const FB_TOKEN = 'fbToken';

export function auth(params = {}) {
  if (localStorage.getItem(FB_TOKEN) === 'true') {
    return Object.assign({}, params, {
      credentials: 'include'
    });
  } else if (localStorage.getItem(JWT_TOKEN)) {
    const headers = Object.assign({}, params.headers || {}, {
      'Authorization': 'Bearer ' + localStorage.getItem('jwtToken')
    });
    return Object.assign({}, params, { headers: headers });
  } else {
    return params;
  }
}
