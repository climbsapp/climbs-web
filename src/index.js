import 'react-dates/initialize';
import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import App from './views/app/App.jsx';
import AppLoggedIn from './views/app/AppLoggedIn.jsx';
import EnsureLoggedInContainer from './views/app/EnsureLoggedInContainer';
import { Login, Register, ForgotPassword } from './views/login';
import { NewGymRoot, CreateGym, FAQView } from './views/newgym';
import { Dashboard, MyGymsRoot, ManageRoutes, RoutesList, RouteStats,
         AddWallOverlay, EditWallOverlay, WallPictureOverlay,
         AddRouteOverlay, EditRouteOverlay, RouteInfoOverlay,
         RouteSettersView, EditLocationOverlay, LocationHeaderPictureOverlay,
         AddAreaOverlay, EditAreaOverlay, TopoPdfOverlay } from './views/mygyms';
import { EventsRoot, EventRegistrationOverlay, FutureEvents, EventResults, PastEventDetails } from './views/events';
import { HomeRoot, WelcomeView, Leaderboards, Statistics } from './views/home';
import { ProfileRoot, ProfileInfoView, ModifyProfile, ModifyPassword } from './views/profile';
import { Router, Route, hashHistory, IndexRedirect } from 'react-router';
import configureStore from './store';
import ReactGA from 'react-ga';
import Highcharts from 'react-highcharts';

import './index.css';
import 'react-dates/lib/css/_datepicker.css';

ReactGA.initialize('UA-72828881-2');

const store = configureStore();
const logPageView = () => {
  ReactGA.set({ page: window.location.hash.substring(1) });
  ReactGA.pageview(window.location.hash.substring(1));
};

const render = () => {
  ReactDOM.render((
    <Provider store={store}>
      <Router history={hashHistory} onUpdate={logPageView}>
        <Route path="/" component={ App }>
          <Route path="login" component={ Login } />
          <Route path="register" component={ Register } />
          <Route path="forgot" component={ ForgotPassword } />
          <Route component={ EnsureLoggedInContainer }>
            <Route component={ AppLoggedIn }>
              <IndexRedirect to="home" />
              <Route path="home" component={ HomeRoot }>
                <IndexRedirect to="welcome" />
                <Route path="welcome" component={ WelcomeView } />
                <Route path="leaderboards" component={ Leaderboards } />
                <Route path="statistics" component={ Statistics } />
              </Route>
              <Route path="events" component={ EventsRoot }>
                <IndexRedirect to="upcoming" />
                <Route path="upcoming" component={ FutureEvents }>
                  <Route path=":eventHash/register" component={ EventRegistrationOverlay } />
                </Route>
                <Route path="results" component={ EventResults } />
                <Route path="results/:eventHash" component={ PastEventDetails } />
              </Route>
              <Route path="newgym" component={ NewGymRoot }>
                  <IndexRedirect to="create" />
                  <Route path="create" component={ CreateGym } />
                  <Route path="faq" component={ FAQView } />
              </Route>
              <Route path="mygyms/:locationId" component={ MyGymsRoot }>
                  <IndexRedirect to="manage" />
                  <Route path="dashboard" component={ Dashboard }>
                    <Route path="edit" component={ EditLocationOverlay } />
                    <Route path="editheaderimage" component={ LocationHeaderPictureOverlay } />
                  </Route>
                  <Route path="routes" component={ RoutesList }>
                    <Route path="info/:routeId" component={ RouteInfoOverlay } />
                    <Route path="edit/:routeId" component={ EditRouteOverlay } />
                  </Route>
                  <Route path="routestats" component={ RouteStats } />
                  <Route path="manage" component={ ManageRoutes }>
                    <Route path="addwall/:areaId" component={ AddWallOverlay } />
                    <Route path="editwall/:wallId" component={ EditWallOverlay } />
                    <Route path="wallpicture/:wallId" component={ WallPictureOverlay } />
                    <Route path="addroute/:wallId" component={ AddRouteOverlay } />
                    <Route path="editroute/:routeId" component={ EditRouteOverlay } />
                    <Route path="addarea" component={ AddAreaOverlay } />
                    <Route path="editarea/:areaId" component={ EditAreaOverlay } />
                    <Route path="topo/:wallId" component={ TopoPdfOverlay } />
                  </Route>
                  <Route path="routesetters" component={ RouteSettersView } />
              </Route>
              <Route path="profile" component={ ProfileRoot }>
                <IndexRedirect to="info" />
                <Route path="info" component={ ProfileInfoView } />
                <Route path="modify" component={ ModifyProfile } />
                <Route path="password" component={ ModifyPassword } />
              </Route>
            </Route>
          </Route>
        </Route>
      </Router>
    </Provider>
  ),
    document.getElementById('root')
  );
}

Highcharts.Highcharts.setOptions({
  global: {
    // timezoneOffset: +1,
    useUTC: false
  }
});

if (!global.Intl) {
  require.ensure([
      'intl',
      'intl/locale-data/jsonp/en.js'
  ], (require) => {
    require('intl');
    require('intl/locale-data/jsonp/en.js');
    render();
  });
} else {
  render();
}
