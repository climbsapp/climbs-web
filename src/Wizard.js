import React, { Component } from 'react';
import './Wizard.css';

class Wizard extends Component {
  constructor() {
    super();

    this.state = {
      activeStep: 0,
      steps: [{
        key: '1',
        title: 'Set name',
        content: this.firstStep
      }, {
        key: '2',
        title: 'Add first route',
        content: this.routeStep
      }, {
        key: '3',
        title: 'Done',
        content: this.thirdStep
      }],
      locationName: '',
      areaName: 'Main Area',
      wallName: 'Forty-Five'
    };

    //this.nextStep = this.nextStep.bind(this);
  }

  render() {
    return (
      <div className="Wizard row">
        <div className="Wizard-steps col s4">
          <h2>Steps to add gym</h2>
          { this.state.steps.map((step, index) =>
              <div key={step.key} className={"Wizard-step " + (index === this.state.activeStep ? 'active' : '')}>
                {(index+1)}. {step.title}
              </div>
          )}
        </div>

        <div className="col s8">
          {this.state.steps[this.state.activeStep].content()}
        </div>
      </div>
    );
  }

  setLocationName = (event) => {
    let locationName = event.target.value;
    this.setState((prevState) => ({
      locationName: locationName
    }));
  }

  setAreaName = (event) => {
    let areaName = event.target.value;
    this.setState((prevState) => ({
      areaName: areaName
    }));
  }

  setWallName = (event) => {
    let wallName = event.target.value;
    this.setState((prevState) => ({
      wallName: wallName
    }));
  }

  nextStep = (e) => {
    this.setState((prevState) => ({
      activeStep: prevState.activeStep + 1
    }));
  }

  firstStep = () => {
    return (
      <div className="Wizard-content">
        <h2>Welcome to supercharge your gym!</h2>
        <p>
          We aint got all day so lets get started! You are about to add
          new climbing gym to Climbs. We will guide you through all the steps so
          no need to worry.
        </p>
        <p>
          In few simple steps we will explain and help you set up your and
          guide you how to add your very first route. Soon you will see your
          climbing gym appear on your smartphone but dont worry, you will not
          go live before you decide you are ready.
        </p>
        <p>
          We will start by setting the <strong>name</strong> of your gym.
        </p>
        <input type="string" placeholder="e.g. Joe's Climbing Garden" value={this.state.locationName} onChange={this.setLocationName} />
        <p>
          This is something you can later change if necessary so worry aside.
        </p>
        <div className="Wizard-buttons">
          <a className="Button primary" href="#" onClick={this.nextStep}>Next</a>
        </div>
      </div>
    );
  }

  routeStep = () => {
    return (
      <div className="Wizard-content">
        <h2>Lets crack your internals!</h2>
        <p>
          In Climbs, your gym will be arranged into a certain hierarchy. Location
          consists of one or more areas. An area consists of walls into which
          the routes are attached to. You probably already view your place in
          this way. Lets pour it into Climbs.
        </p>
        <p>
          To make this more simple, I created first sector, wall and route for
          you. Lets look into these more now.
        </p>
        <h3>Areas</h3>
        <p>
          Usually smaller gyms do well with just one area but larger places tend
          to have more. One area could be called Main Area. Multiple areas are
          usually described as First Floor, Second Floor, Junior Area etc.
        </p>
        <p>
          I took the liberty of naming your first area <strong>Main Area
          </strong> but please change it to your liking.
        </p>
        <input type="string" value={this.state.areaName} onChange={this.setAreaName} />
        <h3>Walls</h3>
        <p>
          Each wall is attached into an area. I added your first wall under
          the {this.state.areaName} and called it Forty-Five as you probably have something like
          that built, dont you? But please change it if you like.
        </p>
        <input type="string" value={this.state.wallName} onChange={this.setWallName}></input>
        <h3>Routes</h3>
        <p>
          Finally! As in real life and in Climbs, routes are attached into walls.
          I have added one route to the sample wall, {this.state.wallName}.
        </p>
        <div className="Wizard-buttons">
          <a className="Button primary" href="#" onClick={this.nextStep}>Next</a>
        </div>
      </div>
    );
  }

  thirdStep = () => {
    return (
      <div className="Wizard-content">
        <h2>We are done!</h2>
        <p>
          It was this easy. I will now let you move forward to the real management
          console to do all the really hard work. Before it, here are some words
          of advice.
        </p>
        <p>
          Now that you are set up with some sample data to get started with, there
          are few more things you want to know.
        </p>
        <h3>Route setters</h3>
        <p>
          As you probably have more route setters than just yourself, you can invite
          them to your team of administrators for {this.state.locationName}. After they
          have created their Climbs accounts and received invites, they can start right
          away.
        </p>
        <h3>Grading system</h3>
        <p>
          Climbs supports pretty much all major grading systems there are. I have selected
          Fontainebleu-grades for you but please do change the default if that aint your
          cup of tea.
        </p>
        <select>
          <option>Fontainebleu</option>
          <option>V-grades</option>
        </select>
        <div className="Wizard-buttons">
          <a className="Button primary" href="#" onClick={this.createLocation}>Done</a>
        </div>
      </div>
    );
  }
}

export default Wizard;
