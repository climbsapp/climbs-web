import React, { Component } from 'react';
import { connect } from 'react-redux';
import { defineMessages, injectIntl } from 'react-intl';
import { loadEvents } from '../../../actions/events';
import { ViewHeader } from '../../../components/header/ViewHeader';
import FutureEventItem from './FutureEventItem';

const messages = defineMessages({
  futureEventsTitle: {
    id: 'view.events.upcoming.title',
    defaultMessage: 'Upcoming events'
  },
  futureEventsNoEventsMessage: {
    id: 'view.events.upcoming.empty',
    defaultMessage: 'Sorry! The event horizon looks blank at the moment. There are no planned events added to the system yet.'
  }
});

class FutureEvents extends Component {
  componentWillMount() {
    this.props.loadEvents();
  }

  renderEvents() {
    const { events } = this.props;
    if (events.length <= 0) {
      const { formatMessage } = this.props.intl;
      return (
        <div className="NoDataContainer">
          { formatMessage(messages.futureEventsNoEventsMessage) }
        </div>
      );
    }

    return events.map(event => {
      return <FutureEventItem event={event} key={'event-' + event.hashId} />;
    });
  }

  render() {
    const { formatMessage } = this.props.intl;
    return (
      <div className="FutureEvents">
        { this.props.children }
        <ViewHeader title={ formatMessage(messages.futureEventsTitle) } />
        <div className="Content-Area">
          { this.renderEvents() }
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state, props) => {
  const { futureEvents, events } = state.events;
  return {
    events: [...futureEvents.map(eventHash => events[eventHash])].sort((a, b) => b.startDate - a.startDate)
  };
}

const mapDispatchToProps = (dispatch) => {
  return {
    loadEvents: () => dispatch(loadEvents())
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(injectIntl(FutureEvents));
