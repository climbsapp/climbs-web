import React, { Component } from 'react';
import { PropTypes } from 'prop-types';
import { hashHistory } from 'react-router';
import { defineMessages, injectIntl } from 'react-intl';
import { durationString } from '../../../helpers';
import { commonMessages } from '../../../helpers/translate';
import { Button } from '../../../components';
import './FutureEventItem.css';

const messages = defineMessages({
  actionRegister: {
    id: 'action.title.register',
    defaultMessage: 'REGISTER'
  },
  actionEdit: {
    id: 'action.title.changeRegistration',
    defaultMessage: 'CHANGE'
  },
  registrationClosesIn: {
    id: 'view.events.upcoming.registration.closing',
    defaultMessage: 'Closes in {relativeTime}'
  },
  registrationOpensIn: {
    id: 'view.events.upcoming.registration.opening',
    defaultMessage: 'Opens in {relativeTime}'
  }
});

class FutureEventItem extends Component {
  getDurationString() {
    const { intl } = this.props;
    const { startDate, endDate } = this.props.event;
    return durationString(intl, commonMessages, startDate, endDate);
  }

  gotoRegistration = () => {
    const { hashId } = this.props.event;
    hashHistory.push('/events/upcoming/' + hashId + '/register');
  }

  getRegistrationInfo() {
    const { event } = this.props;
    const { formatRelative, formatMessage } = this.props.intl;
    if (event.registrationOpen) {
      return formatMessage(messages.registrationClosesIn, {
        relativeTime: formatRelative(event.registrationEnd)
      });
    } else {
      return formatMessage(messages.registrationOpensIn, {
        relativeTime: formatRelative(event.registrationStart)
      });
    }
  }

  render() {
    const { event } = this.props;
    const { registrationOpen, userRegistered } = event;
    const { formatMessage } = this.props.intl;
    const registerLabel = !userRegistered ? formatMessage(messages.actionRegister) : formatMessage(messages.actionEdit);

    return (
      <div className="FutureEventItem">
        <div className="EventHeading">
          <div className="Name">{ event.name }
            <span className="EventDate">{ this.getDurationString() }</span>
          </div>
          <div className="Subheading">
            { event.info }
          </div>
        </div>
        <div className="EventActions">
          <Button label={registerLabel} disabled={!registrationOpen} onClick={this.gotoRegistration} />
          <div className="RegistrationInfo">{ this.getRegistrationInfo() }</div>
        </div>
      </div>
    );
  }

  static propTypes = {
    event: PropTypes.object.isRequired
  }
}

export default injectIntl(FutureEventItem);
