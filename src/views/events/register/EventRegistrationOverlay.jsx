import React, { Component } from 'react';
import { connect } from 'react-redux';
import { defineMessages, injectIntl } from 'react-intl';
import { withRouter } from 'react-router';
import { registerToEvent, unregister } from '../../../actions/events';
import { FormInput, FormChipsInput } from '../../../components/forms/Inputs';
import { Button } from '../../../components';
import OverlayForm from '../../../components/modal/OverlayForm';
import { getGenderOptions } from '../../../helpers/commonOptions';
import registerImg from '../../../images/icons/account-plus-ffffff.svg';

const messages = defineMessages({
  registerTitle: {
    id: 'view.events.register.registerTitle',
    defaultMessage: 'Register'
  },
  updateTitle: {
    id: 'view.events.register.updateTitle',
    defaultMessage: 'Update registration'
  },
  subtitle: {
    id: 'view.events.register.subtitle',
    defaultMessage: 'to {eventName}'
  },
  helpInfo: {
    id: 'view.events.register.help.info',
    defaultMessage: 'Please check your profile details before submitting the registration.'
  },
  helpInputs: {
    id: 'view.events.register.help.inputs',
    defaultMessage: 'Your year of birth and gender will be used to automatically register you into correct category.'
  },
  helpCompetition: {
    id: 'view.events.register.help.competition',
    defaultMessage: 'At the competition, use the Climbs mobile app to tick your sends within the competition time frame.'
  },
  formEmail: {
    id: 'view.events.register.form.label.email',
    defaultMessage: 'Email'
  },
  formYearOfBirth: {
    id: 'view.events.register.form.label.yearOfBirth',
    defaultMessage: 'Year of birth'
  },
  formGender: {
    id: 'view.events.register.form.label.gender',
    defaultMessage: 'Gender'
  },
  formExtraSeries: {
    id: 'view.events.register.form.label.extraSeries',
    defaultMessage: 'Special series'
  },
  submitRegister: {
    id: 'view.events.register.form.submit.register',
    defaultMessage: 'Register'
  },
  submitUnregister: {
    id: 'view.events.register.form.submit.unregister',
    defaultMessage: 'Unregister'
  },
  submitUpdate: {
    id: 'view.events.register.form.submit.update',
    defaultMessage: 'Update registration'
  }
});

class EventRegistrationOverlay extends Component {
  state = {
    yearOfBirth: '',
    gender: '',
    extraSeriesId: undefined
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.profile) {
      this.setState({ 
        yearOfBirth: nextProps.profile.yearOfBirth, 
        email: nextProps.profile.email,
        gender: nextProps.profile.gender,
        extraSeriesId: nextProps.event.userRegisteredExtraSeriesId
      });
    }
  }

  register = () => {
    const { eventId } = this.props.event;
    const { profile } = this.props;
    const { yearOfBirth, gender, extraSeriesId } = this.state;
    const registration = {
      userId: profile.userId,
      yearOfBirth: yearOfBirth,
      gender: gender,
      name: profile.name
    };

    this.props.registerToEvent(eventId, registration, extraSeriesId)
      .then(() => {
        this.props.router.push('/events/upcoming/');
      });
  }

  unregister = () => {
    const { eventId } = this.props.event;
    this.props.unregister(eventId)
      .then(() => {
        this.props.router.push('/events/upcoming/');
      });
  }

  getGenderOptions() {
    const { formatMessage } = this.props.intl;
    return getGenderOptions(formatMessage);
  }

  getExtraSeriesOptions() {
    const { event } = this.props;
    return event.extraSeries.map(es => { return { value: es.extraSeriesId, label: es.name }; });
  }

  isFormValid() {
    return true;
  }

  renderHelp() {
    const { formatMessage } = this.props.intl;
    return (
      <div>
        <p>{ formatMessage(messages.helpInfo) }</p>
        <p>{ formatMessage(messages.helpInputs) }</p>
        <p>{ formatMessage(messages.helpCompetition) }</p>
      </div>
    );
  }

  setExtraSeries = (value, nextValue) => {
    this.setState({ extraSeriesId: nextValue });
  }

  setYearOfBirth = (event) => {
    this.setState({ yearOfBirth: event.target.value });
  }

  setGender = (value) => {
    this.setState({ gender: value });
  }

  render() {
    const { formatMessage } = this.props.intl;
    const { event } = this.props;
    const { email } = this.props.profile;
    const { userRegistered } = event;
    const registerTitle = !userRegistered ? formatMessage(messages.registerTitle) : formatMessage(messages.updateTitle);
    const registerLabel = !userRegistered ? formatMessage(messages.submitRegister) : formatMessage(messages.submitUpdate);
    const subtitle = formatMessage(messages.subtitle, { eventName: event.name });
    return (
      <OverlayForm title={registerTitle} subtitle={subtitle} iconImg={registerImg} help={this.renderHelp()}>
        <FormInput name="email" label={ formatMessage(messages.formEmail) } value={email} disabled={true} />
        <FormInput name="yearOfBirth" label={ formatMessage(messages.formYearOfBirth) } onChange={this.setYearOfBirth} value={this.state.yearOfBirth} type="number" />
        <FormChipsInput name="gender" label={ formatMessage(messages.formGender) } onSelect={this.setGender} selected={this.state.gender} options={this.getGenderOptions()} />
        <FormChipsInput name="extraSeries" label={ formatMessage(messages.formExtraSeries) } onSelect={this.setExtraSeries} selected={this.state.extraSeriesId} options={this.getExtraSeriesOptions()} type="optional" shouldRender={event.extraSeries.length > 0} />
        <div className="Overlay-Footer">
          <Button disabled={!this.isFormValid()} loading={this.props.isLoading}
            label={registerLabel} onClick={this.register} />
          <Button disabled={!this.isFormValid() || !userRegistered} loading={this.props.isLoading}
            label={ formatMessage(messages.submitUnregister) } onClick={this.unregister} />
        </div>
      </OverlayForm>
    )
  }
}

const mapStateToProps = (state, props) => {
  const { eventHash } = props.params;
  return {
    event: state.events.events[eventHash] || { extraSeries: [] },
    profile: state.profile.data || { email: '' },
    isLoading: false
  };
}

const mapDispatchToProps = (dispatch) => {
  return {
    registerToEvent: (eventId, registration, extraSeriesId) => dispatch(registerToEvent(eventId, registration, extraSeriesId)),
    unregister: (eventId) => dispatch(unregister(eventId))
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(injectIntl(EventRegistrationOverlay)));
