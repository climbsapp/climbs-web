import React, { Component } from 'react';
import { connect } from 'react-redux';
import { defineMessages, injectIntl } from 'react-intl';
import { ViewHeader } from '../../../components/header/ViewHeader';
import PastEventItem from './PastEventItem';

const messages = defineMessages({
  pastEventsTitle: {
    id: 'view.events.past.title',
    defaultMessage: 'Past events'
  }
});

class PastEvents extends Component {
  renderEvents() {
    const { pastEvents } = this.props;
    return pastEvents.map(event => {
      return <PastEventItem event={event} key={'event-' + event.hashId} />;
    });
  }

  render() {
    const { formatMessage } = this.props.intl;
    return (
      <div>
        <ViewHeader title={ formatMessage(messages.pastEventsTitle) } />
        <div className="Content-Area">
          { this.renderEvents() }
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state, props) => {
  const { pastEvents, events } = state.events;
  return {
    pastEvents: [...pastEvents.map(eventHash => events[eventHash])].sort((a, b) => b.startDate - a.startDate)
  };
}

export default connect(mapStateToProps)(injectIntl(PastEvents));
