import React, { Component } from 'react';
import { connect } from 'react-redux';
import { loadEvents } from '../../../actions/events';
import OngoingEvents from './OngoingEvents';
import PastEvents from './PastEvents';

class EventResults extends Component {
  componentWillMount() {
    this.props.loadEvents();
  }

  render() {
    return (
      <div className="PastEvents">
        <OngoingEvents />
        <PastEvents />
      </div>
    );
  }
}

const mapStateToProps = (state, props) => {
  return { };
}

const mapDispatchToProps = (dispatch) => {
  return {
    loadEvents: () => dispatch(loadEvents())
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(EventResults);
