import React, { Component } from 'react';
import { connect } from 'react-redux';
import { defineMessages, injectIntl } from 'react-intl';
import { ViewHeader } from '../../../components/header/ViewHeader';
import PastEventItem from './PastEventItem';

const messages = defineMessages({
  ongoingEventsTitle: {
    id: 'view.events.ongoing.title',
    defaultMessage: 'Ongoing events'
  }
});

class OngoingEvents extends Component {
  renderEvents() {
    const { events } = this.props;
    return events.map(event => {
      return <PastEventItem event={event} key={'event-' + event.hashId} />;
    });
  }

  render() {
    const { events } = this.props;
    if (events.length === 0) {
      return null;
    }

    const { formatMessage } = this.props.intl;
    return (
      <div>
        <ViewHeader title={ formatMessage(messages.ongoingEventsTitle) } />
        <div className="Content-Area">
          { this.renderEvents() }
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state, props) => {
  const { ongoingEvents, events } = state.events;
  return {
    events: [...ongoingEvents.map(eventHash => events[eventHash])]
      .sort((a, b) => b.startDate - a.startDate)
  };
}

export default connect(mapStateToProps)(injectIntl(OngoingEvents));
