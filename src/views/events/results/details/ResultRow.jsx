import React, { Component } from 'react';
import { PropTypes } from 'prop-types';
import { defineMessages, injectIntl } from 'react-intl';

const messages = defineMessages({
  eventResultPointAbbr: {
    id: 'view.events.details.points',
    defaultMessage: '{score}p.'
  }
});

class ResultRow extends Component {
  render() {
    const { formatMessage } = this.props.intl;
    const { ranking, name, score } = this.props;
    return (
      <li className="ResultRow">
        <div className="row">
          <div className="col s9">
            <div className="Ranking">{ ranking }</div>
            <div className="Name">{ name }</div>
          </div>
          <div className="col s3">
            <div className="Points">{ formatMessage(messages.eventResultPointAbbr, { score }) }</div>
          </div>
        </div>
      </li>
    );
  }

  static propTypes = {
    ranking: PropTypes.number.isRequired,
    name: PropTypes.string.isRequired,
    score: PropTypes.number.isRequired
  }
}

export default injectIntl(ResultRow);
