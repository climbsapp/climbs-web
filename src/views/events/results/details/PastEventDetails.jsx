import React, { Component } from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';
import { ViewHeader } from '../../../../components/header/ViewHeader';
import { dateToDateString, dateToTimeString } from '../../../../helpers';
import { loadEvent } from '../../../../actions/events';
import Category from './Category';
import PastEventChart from './PastEventChart';

class PastEventDetails extends Component {
  componentWillMount() {
    this.props.loadEvent(this.props.params.eventHash);
  }

  getDurationString() {
    const { startDate, endDate } = this.props.event;
    const startDateString = dateToDateString(startDate);
    const endDateString = dateToDateString(endDate);
    const startTime = dateToTimeString(startDate);
    const endTime = dateToTimeString(endDate);

    // Is same date?
    if (startDateString === endDateString) {
      return startDateString + ' ' + startTime + ' to ' + endTime;
    } else {
      return startDateString + ' ' + startTime + ' to ' + endDateString + ' ' + endTime;
    }
  }

  renderCategories() {
    const { event } = this.props;
    return event.categories.map(category =>
      <Category category={category} key={'cat-' + category.categoryName} />);
  }

  renderStatistics() {
    const { event } = this.props;
    return <PastEventChart eventId={event.eventId} />;
  }

  render() {
    const { event } = this.props;
    return (
      <div className="PastEventDetails">
        <ViewHeader title={event.name} />
        <div className="Content-Area">
          <div className="row">
            <div className="col s7">
              { this.renderCategories() }
            </div>
            <div className="col s5">
              { this.renderStatistics() }
            </div>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state, props) => {
  const event = state.events.events[props.params.eventHash] || { name: '', categories: [] };
  return {
    event: event
  };
}

const mapDispatchToProps = (dispatch) => {
  return {
    loadEvent: (hash) => dispatch(loadEvent(hash))
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(PastEventDetails));
