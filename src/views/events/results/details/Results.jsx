import React, { Component } from 'react';
import { PropTypes } from 'prop-types';
import { defineMessages, injectIntl } from 'react-intl';
import ResultRow from './ResultRow';

const messages = defineMessages({
  eventResultsExpand: {
    id: 'view.events.details.results.expand',
    defaultMessage: 'Show all...'
  }
});

class Results extends Component {
  state = {
    expanded: false
  }

  expand = () => {
    this.setState({ expanded: true });
  }

  render() {
    const { formatMessage } = this.props.intl;
    const { results } = this.props;
    const { expanded } = this.state;
    const resultRows = results.map((score, index) => {
      if (expanded || index < 3) {
        return <ResultRow key={'res-' + score.id} ranking={score.ranking} name={score.name} score={score.score} />;
      } else {
        return undefined;
      }
    });

    const expand = expanded || results.length < 4 ? undefined : <li className="Expand" onClick={this.expand}>{ formatMessage(messages.eventResultsExpand) }</li>;
    return <ul className="Results">{ resultRows }{expand}</ul>;
  }

  static propTypes = {
    results: PropTypes.array.isRequired
  }
}

export default injectIntl(Results);
