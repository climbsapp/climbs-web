import React, { Component } from 'react';
import { PropTypes } from 'prop-types';
import { defineMessages, injectIntl } from 'react-intl';
import Results from './Results';
import './ResultRow.css';

const messages = defineMessages({
  eventCategoryName: {
    id: 'view.events.details.category',
    defaultMessage: '{categoryName} ({participants} participants)'
  }
});

class Category extends Component {
  render() {
    const { category } = this.props;
    const { formatMessage } = this.props.intl;
    const categoryTitle = formatMessage(messages.eventCategoryName, {
      categoryName: category.categoryName,
      participants: category.results.length
    });
    return (
      <div className="Category">
        <div className="Name">{ categoryTitle }</div>
        <Results results={category.results} />
      </div>
    );
  }

  static propTypes = {
    category: PropTypes.object.isRequired
  }
}

export default injectIntl(Category);
