import React, { Component } from 'react';
import { PropTypes } from 'prop-types';
import { injectIntl } from 'react-intl';
import { commonMessages } from '../../../helpers/translate';
import { durationString } from '../../../helpers';
import { Link } from 'react-router'
import './PastEventItem.css';

class PastEventItem extends Component {
  getDurationString() {
    const { intl } = this.props;
    const { startDate, endDate } = this.props.event;
    return durationString(intl, commonMessages, startDate, endDate);
  }

  render() {
    const { event } = this.props;
    return (
      <div className="PastEventItem">
        <Link to={'/events/results/' + event.hashId}>
          <div className="EventHeading">
            <div className="Name">{ event.name }</div>
            <div className="Subheading">{ this.getDurationString() }</div>
          </div>
        </Link>
      </div>
    );
  }

  static propTypes = {
    event: PropTypes.object.isRequired
  }
}

export default injectIntl(PastEventItem);
