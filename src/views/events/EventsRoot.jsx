import React, { Component } from 'react';
import { defineMessages, injectIntl } from 'react-intl';
import { SubNav, SubNavItem } from '../../components/navigation/SubNav';
import registerImg from '../../images/icons/account-plus-9e9e9e.svg';
import pastEventsImg from '../../images/icons/history-9e9e9e.svg';
//import createEventImg from '../../images/icons/calendar-plus-9e9e9e.svg';

const messages = defineMessages({
  subNavItemUpcomingEventsTitle: {
    id: 'view.events.subnav.upcoming',
    defaultMessage: 'Upcoming'
  },
  subNavItemPastEventsTitle: {
    id: 'view.events.subnav.past',
    defaultMessage: 'Results'
  },
  subNavItemCreateEventTitle: {
    id: 'view.events.subnav.create',
    defaultMessage: 'Create event'
  }
});

class EventsRoot extends Component {

  render() {
    const { formatMessage } = this.props.intl;
    return (
      <div className="Dashboard">
        <div className="row">
          <div className="col l2 m3 s4">
            <SubNav>
              <SubNavItem text={ formatMessage(messages.subNavItemUpcomingEventsTitle) } img={registerImg}
                path="events/upcoming" location={this.props.location} />
              <SubNavItem text={ formatMessage(messages.subNavItemPastEventsTitle) } img={pastEventsImg}
                path="events/results" location={this.props.location} />
                {/*
              <SubNavItem text={ formatMessage(messages.subNavItemCreateEventTitle) } img={createEventImg}
                path="events/create" location={this.props.location} />
                */}
            </SubNav>
          </div>
          <div className="col l10 m9 s8">
            { this.props.children }
          </div>
        </div>
      </div>
    )
  }
}

export default injectIntl(EventsRoot);
