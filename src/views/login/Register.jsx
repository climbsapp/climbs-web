import React, { Component } from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';
import { Button } from '../../components';
import { Sign, SignFormInput } from '../../components';
import { register } from '../../actions/login';

class Register extends Component {
  state = {
    name: '',
    email: '',
    password: '',
    error: null
  }

  setName = (event) => {
    this.setState({ name: event.target.value, error: null });
  }

  setEmail = (event) => {
    this.setState({ email: event.target.value, error: null });
  }

  setPassword = (event) => {
    this.setState({ password: event.target.value, error: null });
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.isLoggedIn) {
      this.props.router.push('/');
    }
  }

  submit = () => {
    this.props.register(this.state)
      .then(() => {
        this.props.router.push('/');
      })
      .catch((error) => {
        console.log("ERROR!", error);
        this.setState({ error: error });
      });
  }

  componentDidMount() {
    document.addEventListener('keydown', this.handleKeyPress);
  }

  componentWillUnmount() {
    document.removeEventListener('keydown', this.handleKeyPress);
  }

  handleKeyPress = (e) => {
    if (e.keyCode === 13) {
      this.submit();
    }
  }

  gotoLogin = () => {
    this.props.router.push('/login');
  }

  facebookLogin = () => {
    this.props.authenticateFacebook();
    window.location = '/auth/facebook';
  }

  getNameError() {
    const { error } = this.state;
    if (!error) {
      return null;
    }

    if (error.errorCode === 'VALIDATION_FAILED' && error.message === 'firstName') {
      return 'Name is something that is required';
    }

    return null;
  }

  getPasswordError() {
    const { error } = this.state;
    if (!error) {
      return null;
    }

    if (error.errorCode === 'VALIDATION_FAILED' && error.message === 'password') {
      return 'Password is something not to be left out';
    }

    return null;
  }

  getEmailError() {
    const { error } = this.state;
    if (!error) {
      return null;
    }

    if (error.errorCode === 'VALIDATION_FAILED' && error.message === 'email') {
      return 'Thats not valid email';
    } else if (error.errorCode === 'EMAIL_ALREADY_IN_USE') {
      return 'This email has already been registered';
    }
    return null;
  }

  render() {
    const { name, email, password, error } = this.state;
    const nameError = this.getNameError();
    const emailError = this.getEmailError();
    const passwordError = this.getPasswordError();
    return (
      <Sign onFacebookLogin={this.facebookLogin} title="Sign up" alternative="Login" onAlternativeClick={this.gotoLogin}>
        <SignFormInput type="text" name="name" placeholder="Name" value={name} onChange={this.setName} error={error} errorMessage={nameError} />
        <SignFormInput type="text" name="email" placeholder="Email" value={email} onChange={this.setEmail} error={error} errorMessage={emailError} />
        <SignFormInput type="password" name="password" placeholder="Password" value={password} onChange={this.setPassword} error={error} errorMessage={passwordError} />
        <div className="col s12">
          <Button label="Register" disabled={false} onClick={this.submit} />
        </div>
      </Sign>
    );
  }
}

const mapStateToProps = (state, props) => {
  return {
    isLoggedIn: state.profile.isLoggedIn
  };
}

const mapDispatchToProps = (dispatch) => {
  return {
    register: (account) => dispatch(register(account))
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(Register));
