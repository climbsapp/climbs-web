import React, { Component } from 'react';
import { connect } from 'react-redux';
import { withRouter, Link } from 'react-router';
import { Button } from '../../components';
import { Sign, SignFormInput } from '../../components';
import Notifications from '../../views/notifications/Notifications';
import { authenticate, authenticateFacebook } from '../../actions/login';

class Login extends Component {
  state = {
    username: '',
    password: '',
    error: null
  }

  setUsername = (event) => {
    this.setState({ username: event.target.value, error: null });
  }

  setPassword = (event) => {
    this.setState({ password: event.target.value, error: null });
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.isLoggedIn) {
      this.props.router.push('/');
    }
  }

  componentDidMount() {
    document.addEventListener('keydown', this.handleKeyPress);
  }

  componentWillUnmount() {
    document.removeEventListener('keydown', this.handleKeyPress);
  }

  handleKeyPress = (e) => {
    if (e.keyCode === 13) {
      this.submit();
    }
  }

  submit = () => {
    const { username, password } = this.state;
    this.props.authenticate({ username, password })
      .then(() => {
        this.props.router.push('/');
      })
      .catch((error) => {
        this.setState({ error: error });
      });
  }

  gotoSignup = () => {
    this.props.router.push('/register');
  }

  facebookLogin = () => {
    this.props.authenticateFacebook();
    window.location = '/auth/facebook';
  }

  render() {
    const { username, password, error } = this.state;
    const { isAuthenticating } = this.props;
    return (
      <Sign onFacebookLogin={this.facebookLogin} title="Login" 
            alternative="Sign up" onAlternativeClick={this.gotoSignup}>
        <SignFormInput type="text" name="email" placeholder="Email"
          value={username} onChange={this.setUsername} error={error}
          errorMessage="Email/password combination did not match" />
        <SignFormInput type="password" name="password" placeholder="Password"
          value={password} onChange={this.setPassword} error={error} />
        <div className="col s12">
          <div className="ForgotPassword">
            <Link to="/forgot">Forgot password?</Link>
          </div>
        </div>
        <div className="col s12">
          <Button label="Login" loading={isAuthenticating}
            disabled={false} onClick={this.submit} />
        </div>
        <Notifications />
      </Sign>
    );
  }
}

const mapStateToProps = (state, props) => {
  return {
    isLoggedIn: state.profile.isLoggedIn,
    isAuthenticating: state.profile.isAuthenticating
  };
}

const mapDispatchToProps = (dispatch) => {
  return {
    authenticate: (credentials) => dispatch(authenticate(credentials)),
    authenticateFacebook: () => dispatch(authenticateFacebook())
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(Login));
