import React, { Component } from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';
import { Button } from '../../components';
import { Sign, SignFormInput } from '../../components';
import { requestPassword } from '../../actions/login';

class ForgotPassword extends Component {
  state = {
    email: '',
    error: null
  }

  setEmail = (event) => {
    this.setState({ email: event.target.value });
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.isLoggedIn) {
      this.props.router.push('/');
    }
  }

  submit = () => {
    this.props.requestPassword(this.state.email)
      .then(() => this.setState({ email: '' }))
      .then(() => this.props.router.push('/'))
      .catch(error => this.setState({ error: error }));
  }

  facebookLogin = () => {
    this.props.authenticateFacebook();
    window.location = '/auth/facebook';
  }

  isValidEmail = () => {
    const { email } = this.state;
    return (email || '').match(/.+@.+/); // Not too complicated for good reason
  }

  getEmailError() {
    const { error } = this.state;
    if (!error) {
      return null;
    }

    if (error.errorCode === 'EMAIL_NOT_FOUND') {
      return 'That email has not been registered';
    }
  }

  gotoLogin = () => {
    this.props.router.push('/login');
  }

  render() {
    const { email, error } = this.state;
    const { isRequestingPassword } = this.props;
    const errorMessage = this.getEmailError();
    return (
      <Sign onFacebookLogin={this.facebookLogin} title="Forgot password" alternative="Login" onAlternativeClick={this.gotoLogin}>
        <SignFormInput type="text" name="email" placeholder="Email" value={email}
          onChange={this.setEmail} error={error} errorMessage={errorMessage} />
        <div className="col s12">
          <Button label="Reset lost password" loading={isRequestingPassword}
            disabled={!this.isValidEmail()} onClick={this.submit} />
        </div>
      </Sign>
    );
  }
}

const mapStateToProps = (state, props) => {
  return {
    isLoggedIn: state.profile.isLoggedIn,
    isRequestingPassword: state.profile.isRequestingPassword
  };
}

const mapDispatchToProps = (dispatch) => {
  return {
    requestPassword: (email) => dispatch(requestPassword(email))
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(ForgotPassword));
