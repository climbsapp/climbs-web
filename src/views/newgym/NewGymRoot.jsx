import React, { Component } from 'react';
import { defineMessages, injectIntl } from 'react-intl';
import { SubNav, SubNavItem } from '../../components/navigation/SubNav';
import createGymImg from '../../images/icons/map-marker-plus.svg';
import faqImg from '../../images/icons/help-circle.svg';

const messages = defineMessages({
  subNavItemNewGym: {
    id: 'view.newgym.subnav.newgym',
    defaultMessage: 'Add new gym'
  },
  subNavItemFaq: {
    id: 'view.newgym.subnav.faq',
    defaultMessage: 'FAQ'
  }
});

class NewGymRoot extends Component {

  render() {
    const { formatMessage } = this.props.intl;
    return (
      <div className="NewGym">
        <div className="row">
          <div className="col l2 m3 s4">
            <SubNav>
              <SubNavItem text={formatMessage(messages.subNavItemNewGym)} img={createGymImg}
                path="newgym/create" location={this.props.location} />
              <SubNavItem text={formatMessage(messages.subNavItemFaq)} img={faqImg}
                path="newgym/faq" location={this.props.location} />
            </SubNav>
          </div>
          <div className="col l9 m9 s8">
            { this.props.children }
          </div>
        </div>
      </div>
    )
  }
}

export default injectIntl(NewGymRoot);
