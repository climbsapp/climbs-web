export { default as NewGymRoot } from './NewGymRoot';
export { default as CreateGym } from './create/CreateGym';
export { default as FAQView } from './faq/FAQ';
