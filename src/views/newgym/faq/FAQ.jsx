import React, { Component } from 'react';
import { defineMessages, injectIntl } from 'react-intl';
import { ViewHeader } from '../../../components/header/ViewHeader';
import './FAQ.css';

const messages = defineMessages({
  title: {
    id: 'view.newgym.faq.title',
    defaultMessage: 'Frequently asked questions'
  },
  questionOne: {
    id: 'view.newgym.faq.q.who',
    defaultMessage: 'Who is behind all this?'
  },
  answerOne: {
    id: 'view.newgym.faq.a.who',
    defaultMessage: 'Climbs is brainchild of Mikko Nieminen who started the project in 2013, cooperating with Kiipeilyareena gym to replace their Excel-chaos. Along the years Kalle Karjalainen and Kalle Karikoski joined to create the native apps for climbers.'
  },
  questionTwo: {
    id: 'view.newgym.faq.q.cost',
    defaultMessage: 'What does it cost?'
  },
  answerTwo: {
    id: 'view.newgym.faq.a.cost',
    defaultMessage: 'Nothing. No hidden costs whatsoever.'
  },
  questionThree: {
    id: 'view.newgym.faq.q.catch',
    defaultMessage: 'What is the catch? Nothing is free.'
  },
  answerThree: {
    id: 'view.newgym.faq.a.catch',
    defaultMessage: 'We are still in the phase, where the feedback from gyms outweights the small costs this brings to us. We want to be best in what we do and to get there we need gyms to help us achieve that. We guarantee that if we change the price, from that day on you still go for free at least one year.'
  }
});

class FaqView extends Component {

  render() {
    const { formatMessage } = this.props.intl;
    return (
      <div className="FaqView">
        <ViewHeader title={formatMessage(messages.title)} />
        <div className="Content-Area">
          <QA q={formatMessage(messages.questionOne)}
              a={formatMessage(messages.answerOne)} />
          <QA q={formatMessage(messages.questionTwo)}
              a={formatMessage(messages.answerTwo)} />
          <QA q={formatMessage(messages.questionThree)}
              a={formatMessage(messages.answerThree)} />
        </div>
      </div>
    )
  }
}

class QA extends Component {
  render() {
    return (
      <div className="row">
        <div className="col s12">
          <div className="Question">
            {this.props.q}
          </div>
          <div className="Answer">
            {this.props.a}
          </div>
        </div>
      </div>
    );
  }
}

export default injectIntl(FaqView);
