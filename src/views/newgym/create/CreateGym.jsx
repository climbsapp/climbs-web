import React, { Component } from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';
import { defineMessages, injectIntl } from 'react-intl';
import { postLocation } from '../../../actions/createGym';
import { ViewHeader, ChipsInput, MapInput } from '../../../components';
import Button from '../../../components/buttons/Button';
import './CreateGym.css';

const messages = defineMessages({
  newGymTitle: {
    id: 'view.newgym.title', 
    defaultMessage: 'Add new gym'
  },
  newGymFormName: {
    id: 'view.newgym.form.gymname', 
    defaultMessage: 'Gym name'
  },
  newGymFormNamePlaceholder: {
    id: 'view.newgym.form.gymname.placeholder', 
    defaultMessage: 'e.g. MonkeyBiz'
  },
  newGymFormRouteGradingSystem: {
    id: 'view.newgym.form.grading.routes', 
    defaultMessage: 'Route grading system'
  },
  newGymFormBoulderGradingSystem: {
    id: 'view.newgym.form.grading.boulders', 
    defaultMessage: 'Boulder grading system'
  },
  newGymFormLocation: {
    id: 'view.newgym.form.location', 
    defaultMessage: 'Gym location'
  },
  actionAddGym: {
    id: 'view.newgym.action.add',
    defaultMessage: 'Create gym'
  },
  newGymHelpP1: {
    id: 'view.newgym.help.paragraphOne',
    defaultMessage: 'Welcome to add your gym to Climbs. Just begin by giving us the name of your gym.'
  },
  newGymHelpP2: {
    id: 'view.newgym.help.paragraphTwo',
    defaultMessage: 'This will create the gym for you and you can start using the management console. Your gym will not show to other climbers using Climbs apps before you are ready to open up.'
  },
  newGymHelpP3: {
    id: 'view.newgym.help.paragraphThree',
    defaultMessage: 'Route grading system is the grading system used at your gym for route walls and boulder grading system logically for boulder problems.'
  },
  newGymHelpP4: {
    id: 'view.newgym.help.paragraphFour',
    defaultMessage: 'To set your gym location, move the map around and center your venue location to the marker position.'
  },
  gradingTitleFrench: {
    id: 'view.newgym.grading.french',
    defaultMessage: 'French'
  },
  gradingTooltipFrench: {
    id: 'view.newgym.grading.french.tooltip',
    defaultMessage: '6a, 6b,..'
  },
  gradingTitleYDS: {
    id: 'view.newgym.grading.yds',
    defaultMessage: 'YDS'
  },
  gradingTooltipYDS: {
    id: 'view.newgym.grading.yds.tooltip',
    defaultMessage: '5.10a, 5.10b,..'
  },
  gradingTitleBritish: {
    id: 'view.newgym.grading.british',
    defaultMessage: 'British'
  },
  gradingTooltipBritish: {
    id: 'view.newgym.grading.british.tooltip',
    defaultMessage: 'E4, E5..'
  },
  gradingTitleUIAA: {
    id: 'view.newgym.grading.uiaa',
    defaultMessage: 'UIAA'
  },
  gradingTooltipUIAA: {
    id: 'view.newgym.grading.uiaa.tooltip',
    defaultMessage: 'VI, VII,..'
  },
  gradingTitleFont: {
    id: 'view.newgym.grading.font',
    defaultMessage: 'Font'
  },
  gradingTooltipFont: {
    id: 'view.newgym.grading.font.tooltip',
    defaultMessage: '6A, 6B+..'
  },
  gradingTitleHueco: {
    id: 'view.newgym.grading.hueco',
    defaultMessage: 'Hueco'
  },
  gradingTooltipHueco: {
    id: 'view.newgym.grading.hueco.tooltip',
    defaultMessage: 'V5, V6..'
  }
});

class CreateGym extends Component {
  constructor() {
    super();

    this.state = {
      locationName: '',
      gradingSystem: 'French',
      gradingSystemBoulder: 'Font',
      location: {
        lat: 37.774929,
        lng: -122.419416
      }
    };
  }

  createGym = () => {
    const data = {
      locationName: this.state.locationName,
      locationType: 1, // Indoor
      gradingSystem: this.state.gradingSystem,
      gradingSystemBoulder: this.state.gradingSystemBoulder,
      latitude: this.state.location.lat,
      longitude: this.state.location.lng,
      areaName: 'Main area',
      wallName: 'Example Wall'
    };

    this.props.postLocation(data, (newLocation) => {
      this.props.router.push('/mygyms/' + newLocation.locationId);
    });
  }

  setLocationName = (event) => {
    this.setState({ locationName: event.target.value });
  }

  setGradingSystem = (value) => {
    this.setState({ gradingSystem: value });
  }

  setGradingSystemBoulder = (value) => {
    this.setState({ gradingSystemBoulder: value });
  }

  getRouteGradingSystems = () => {
    const { formatMessage } = this.props.intl;
    const defaultGradingSystems = [
      { value: 'French', label: formatMessage(messages.gradingTitleFrench), tooltip: formatMessage(messages.gradingTooltipFrench) },
      { value: 'YDS USA', label: formatMessage(messages.gradingTitleYDS), tooltip: formatMessage(messages.gradingTooltipYDS) },
      { value: 'British Adj', label: formatMessage(messages.gradingTitleBritish), tooltip: formatMessage(messages.gradingTooltipBritish) },
      { value: 'UIAA', label: formatMessage(messages.gradingTitleUIAA), tooltip: formatMessage(messages.gradingTooltipUIAA) }
    ];

    return defaultGradingSystems;
  }

  getBoulderGradingSystems = () => {
    const { formatMessage } = this.props.intl;
    const defaultGradingSystems = [
      { value: 'Font', label: formatMessage(messages.gradingTitleFont), tooltip: formatMessage(messages.gradingTooltipFont) },
      { value: 'Hueco (USA)', label: formatMessage(messages.gradingTitleHueco), tooltip: formatMessage(messages.gradingTooltipHueco) },
    ];

    return defaultGradingSystems;
  }

  renderHelp() {
    const { formatMessage } = this.props.intl;
    return (
      <div>
        <p>{ formatMessage(messages.newGymHelpP1) }</p>
        <p>{ formatMessage(messages.newGymHelpP2) }</p>
        <p>{ formatMessage(messages.newGymHelpP3) }</p>
        <p>{ formatMessage(messages.newGymHelpP4) }</p>
      </div>
    );
  }

  setLocation = (lat, lng) => {
    this.setState({ location: { lat: lat, lng: lng }});
  }

  isFormValid() {
    return !!this.state.locationName;
  }

  render() {
    const { formatMessage } = this.props.intl;
    const location = (Math.round(this.state.location.lat * 10000) / 10000) + ', ' + (Math.round(this.state.location.lng * 10000) / 10000);
    return (
      <div className="CreateGym">
        <ViewHeader title={formatMessage(messages.newGymTitle)} />
        <div className="Content-Area">
          <div className="Row Form-Container">
            <div className="Col S8 Form">
              <div className="row">
                <div className="input-field col s12">
                  <input id="location_name" type="text" placeholder={formatMessage(messages.newGymFormNamePlaceholder)}
                    value={this.state.locationName} onChange={this.setLocationName} />
                  <label className="active" htmlFor="location_name">{formatMessage(messages.newGymFormName)} *</label>
                </div>
              </div>

              <div className="row">
                <div className="input-field col s12">
                  <ChipsInput selected={this.state.gradingSystem}
                    options={this.getRouteGradingSystems()} onSelect={this.setGradingSystem} />
                  <label className="active">{formatMessage(messages.newGymFormRouteGradingSystem)}</label>
                </div>
              </div>

              <div className="row">
                <div className="input-field col s12">
                  <ChipsInput selected={this.state.gradingSystemBoulder}
                    options={this.getBoulderGradingSystems()} onSelect={this.setGradingSystemBoulder} />
                  <label className="active">{formatMessage(messages.newGymFormBoulderGradingSystem)}</label>
                </div>
              </div>

              <div className="row">
                <div className="input-field col s12">
                  <MapInput onChange={this.setLocation} initialCenter={this.state.location} />
                  <label className="active" htmlFor="location">{formatMessage(messages.newGymFormLocation)} <span className="value">({location})</span></label>
                </div>
              </div>

              <div className="row">
                <div className="col s12 Actions">
                  <Button label={formatMessage(messages.actionAddGym)} onClick={this.createGym} disabled={!this.isFormValid()}
                      loading={this.props.isLoading} />
                </div>
              </div>
            </div>
            <div className="Col S4 Help">
              {this.renderHelp()}
            </div>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state, props) => {
  return {
    isLoading: state.routesAdmin.isLoading
  };
}

const mapDispatchToProps = (dispatch) => {
  return {
    postLocation: (data, successCb) => {
      dispatch(postLocation(data, successCb));
    }
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(injectIntl(CreateGym), { useRef: true }));
