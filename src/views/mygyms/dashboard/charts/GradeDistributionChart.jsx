import React, { Component } from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';
import ReactHighcharts from 'react-highcharts';
import { defineMessages, injectIntl } from 'react-intl';
import { fetchGradeDistributionChart } from '../../../../actions/adminCharts';

const messages = defineMessages({
  title: {
    id: 'view.mygyms.dashboard.gradeChart.title',
    defaultMessage: 'Grade distribution of routes and logged sends'
  },
  warningTooFew: {
    id: 'view.mygyms.dashboard.gradeChart.warning.tooFew',
    defaultMessage: 'There aren\'t not many routes set so this chart might look awkward.'
  },
  yAxisRoutes: {
    id: 'view.mygyms.dashboard.gradeChart.yAxis.amountRoutes',
    defaultMessage: 'Amount of routes'
  },
  yAxisSends: {
    id: 'view.mygyms.dashboard.gradeChart.yAxis.amountSends',
    defaultMessage: 'Amount of sends'
  },
  categoryRoutes: {
    id: 'view.mygyms.dashboard.gradeChart.category.routes',
    defaultMessage: 'Routes'
  },
  categoryBoulders: {
    id: 'view.mygyms.dashboard.gradeChart.category.boulders',
    defaultMessage: 'Boulders'
  },
  categoryRouteSends: {
    id: 'view.mygyms.dashboard.gradeChart.category.routeSends',
    defaultMessage: 'Route sends'
  },
  categoryBoulderSends: {
    id: 'view.mygyms.dashboard.gradeChart.category.boulderSends',
    defaultMessage: 'Boulder sends'
  },
  tooltipHeader: {
    id: 'view.mygyms.dashboard.gradeChart.category.tooltip',
    defaultMessage: 'Routes and sends'
  }
});

class GradeDistributionChart extends Component {
  componentDidMount() {
    this.props.fetchGradeDistributionChart(this.props.locationId);
  }

  getChartConfig = (data, gradingSystem, gradingSystemBoulder) => {
    const { formatMessage } = this.props.intl;
    // If there is no grading system set for boulders, use the one for routes
    if (!gradingSystemBoulder) {
      gradingSystemBoulder = gradingSystem;
    }

    const tooltipHeader = formatMessage(messages.tooltipHeader);
    return {
      tooltip: {
        shared: true,
        headerFormat: '<span style="font-size: 10px">' + tooltipHeader + '</span><br/>'
      },
      chart: {
        height: '250',
        style: {
          fontFamily: '"Proxima Nova", "Roboto", "sans-serif"'
        }
      },
      credits: {
        enabled: false
      },
      legend: {
        align: 'right',
        verticalAlign: 'top',
      },
      plotOptions: {
        series: {
          groupPadding: 0.05,
          pointPadding: 0,
          borderWidth: 0,
          dataLabels: {
            enabled: true,
            formatter: function() {
              return (this.series.type !== 'line' ? this.y || '' : '');
            }
          },
          events: {
            legendItemClick: function(e) {
              const seriesIndex = this.index;
              const series = this.chart.series;
              const toShow = !series[seriesIndex].visible;
              if (seriesIndex % 2 === 0) {
                if (series[0].visible === !toShow) series[0].setVisible(toShow, false);
                if (series[2].visible === !toShow) series[2].setVisible(toShow, false);
              } else {
                if (series[1].visible === !toShow) series[1].setVisible(toShow, false);
                if (series[3].visible === !toShow) series[3].setVisible(toShow, false);
              }
              this.chart.redraw();
              return false;
            }
          }
        },
      },
      title: {
        text: formatMessage(messages.title),
        align: 'left',
        x: 45
      },
      subtitle: {
        text: (data && data.categories.length < 5) ? formatMessage(messages.warningTooFew) : '',
        verticalAlign: 'bottom',
        floating: true,
        align: 'center',
        y: 12
      },
      xAxis: {
        categories: data ? data.categories : [],
        labels: {
          formatter: function() {
            if (!gradingSystem && !gradingSystemBoulder) {
              return this.value;
            }

            const sportRoutesVisible = this.axis.series[0].visible;
            const routeGrade = gradingSystem.grades[gradingSystem.internalGrades.indexOf(this.value)];

            const boulderProblemsVisible = this.axis.series[1].visible;
            const boulderGrade = gradingSystemBoulder.grades[gradingSystemBoulder.internalGrades.indexOf(this.value)];

            if (boulderGrade === routeGrade) {
              return routeGrade;
            }

            let label = '';
            if (sportRoutesVisible) {
              label += routeGrade;
            }

            if (sportRoutesVisible && boulderProblemsVisible && boulderGrade !== undefined) {
              label += ' / ';
            }

            if (boulderProblemsVisible && boulderGrade !== undefined) {
              label += boulderGrade;
            }

            return label;
          }
        }
      },
      yAxis: [{
        title: {
          text: formatMessage(messages.yAxisRoutes)
        }
      }, {
        title: {
          text: formatMessage(messages.yAxisSends)
        },
        opposite: true
      }],
      series: [{
        name: formatMessage(messages.categoryRoutes),
        yAxis: 0,
        type: 'column',
        color: '#69d2e7',
        data: data ? data.sportsRouteAmounts : []
      }, {
        name: formatMessage(messages.categoryBoulders),
        yAxis: 0,
        type: 'column',
        color: '#282828',
        data: data ? data.boulderRouteAmounts : []
      }, {
        name: formatMessage(messages.categoryRouteSends),
        yAxis: 1,
        color: '#fb6a01',
        data: data ? data.sendAmounts : []
      }, {
        name: formatMessage(messages.categoryBoulderSends),
        yAxis: 1,
        color: '#C2C99E',
        dashStyle: 'ShortDash',
        data: data ? data.boulderSends : []
      }]
    };
  }

  render() {
    const { gradingSystemBoulder, gradingSystem, data, isLoading } = this.props;
    if (this.refs.chart && isLoading) {
      let chart = this.refs.chart.getChart();
      isLoading ?  chart.showLoading() : chart.hideLoading();
    }
    const chartableData = mapDataToChartConfig(data);
    const chartConfig = this.getChartConfig(chartableData, gradingSystem, gradingSystemBoulder);
    return (
      <ReactHighcharts config={chartConfig} ref="chart" />
    );
  }
}

const mapStateToProps = (state, props) => {
  const location = state.routesAdmin.locations[props.locationId];
  const gradingSystem = location ? state.routesAdmin.gradingSystems[location.gradingSystem] : {};
  const gradingSystemBoulder = location ? state.routesAdmin.gradingSystems[location.gradingSystemBoulder] : {};
  return {
    locationId: props.locationId,
    isLoading: state.adminCharts.isLoading,
    data: state.adminCharts.gradeDistributionChart[props.locationId],
    gradingSystem: gradingSystem,
    gradingSystemBoulder: gradingSystemBoulder
  };
}

const mapDispatchToProps = (dispatch) => {
  return {
    fetchGradeDistributionChart: (locationId) => {
      dispatch(fetchGradeDistributionChart(locationId));
    }
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(injectIntl(GradeDistributionChart), { useRef: true }));

function mapDataToChartConfig(data) {
  if (!data) {
    return undefined;
  }

  let categories = [];
  let sportsRouteAmounts = [];
  let boulderRouteAmounts = [];
  let sendAmounts = [];
  let boulderSends = [];
  for (let i = 0; i < data.length; i++) {
    let values = data[i];
    categories.push(values.internalGrade);
    sportsRouteAmounts.push(values.autobelayAmount + values.leadAmount + values.topRopeAmount + values.topRopeAndLeadAmount);
    boulderRouteAmounts.push(values.boulderAmount);
    sendAmounts.push(values.autobelaySendsAmount + values.leadSendsAmount + values.topRopeSendsAmount);
    boulderSends.push(values.boulderSendsAmount);
  }

  return {
    categories: categories,
    sendAmounts: sendAmounts,
    boulderSends: boulderSends,
    sportsRouteAmounts: sportsRouteAmounts,
    boulderRouteAmounts: boulderRouteAmounts
  };
}
