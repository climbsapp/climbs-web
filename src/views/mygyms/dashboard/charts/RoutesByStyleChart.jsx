import React, { Component } from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';
import ReactHighcharts from 'react-highcharts';
import { defineMessages, injectIntl } from 'react-intl';
import { fetchRoutesByStyleChart } from '../../../../actions/adminCharts';

const messages = defineMessages({
  title: {
    id: 'view.mygyms.dashboard.styleChart.title',
    defaultMessage: 'Routes by style'
  },
  yAxisTitle: {
    id: 'view.mygyms.dashboard.styleChart.yTitle',
    defaultMessage: 'Amount of routes'
  },
  rangeNotGradedTitle: {
    id: 'view.mygyms.dashboard.styleChart.rangeNotGradedTitle',
    defaultMessage: 'Not graded'
  },
  tagNone: {
    id: 'view.mygyms.dashboard.styleChart.tag.none',
    defaultMessage: 'Not tagged'
  },
  tagArete: {
    id: 'view.mygyms.dashboard.styleChart.tag.arete',
    defaultMessage: 'Arete'
  },
  tagCrimps: {
    id: 'view.mygyms.dashboard.styleChart.tag.crimps',
    defaultMessage: 'Crimps'
  },
  tagDyno: {
    id: 'view.mygyms.dashboard.styleChart.tag.dyno',
    defaultMessage: 'Dyno'
  },
  tagMantle: {
    id: 'view.mygyms.dashboard.styleChart.tag.mantle',
    defaultMessage: 'Mantle'
  },
  tagPockets: {
    id: 'view.mygyms.dashboard.styleChart.tag.pockets',
    defaultMessage: 'Pockets'
  },
  tagPinches: {
    id: 'view.mygyms.dashboard.styleChart.tag.pinches',
    defaultMessage: 'Pinches'
  },
  tagTraverse: {
    id: 'view.mygyms.dashboard.styleChart.tag.traverse',
    defaultMessage: 'Traverse'
  },
  tagUnderclings: {
    id: 'view.mygyms.dashboard.styleChart.tag.underclings',
    defaultMessage: 'Underclings'
  },
  tagEndurance: {
    id: 'view.mygyms.dashboard.styleChart.tag.endurance',
    defaultMessage: 'Endurance'
  },
  tagPower: {
    id: 'view.mygyms.dashboard.styleChart.tag.power',
    defaultMessage: 'Power'
  },
  tagBalance: {
    id: 'view.mygyms.dashboard.styleChart.tag.balance',
    defaultMessage: 'Technical'
  },
  tagCircuit: {
    id: 'view.mygyms.dashboard.styleChart.tag.circuit',
    defaultMessage: 'Circuit'
  },
  tagSlopers: {
    id: 'view.mygyms.dashboard.styleChart.tag.slopers',
    defaultMessage: 'Slopers'
  }
});

class RoutesByStyleChart extends Component {
  componentDidMount() {
    this.props.fetchRoutesByStyleChart(this.props.locationId);
  }

  getChartConfig(data) {
    const { formatMessage } = this.props.intl;
    const categories = Object.keys(data.categories);
    const series = Object.keys(data.knownRanges).map(range => {
      return {
        rangeName: range,
        name: data.knownRanges[range].name,
        yAxis: 0,
        type: 'bar',
        data: []
      };
    });

    categories.forEach(categoryName => {
      let cat = data.categories[categoryName];
      series.forEach(serie => {
        serie.data.push(cat.ranges[serie.rangeName].amount);
      });
    });

    return {
      tooltip: {
        shared: true
      },
      chart: {
        style: {
          fontFamily: '"Proxima Nova", "Roboto", "sans-serif"'
        }
      },
      credits: {
        enabled: false
      },
      legend: {
        align: 'center',
        verticalAlign: 'bottom',
        reversed: true
      },
      title: {
        text: formatMessage(messages.title),
        align: 'left',
        x: 45
      },
      xAxis: {
        categories: categories
      },
      yAxis: [{
        title: {
          text: formatMessage(messages.yAxisTitle)
        }
      }],
      plotOptions: {
        series: {
          stacking: 'normal'
        }
      },
      series: series.reverse()
    };
  }

  render() {
    const { formatMessage } = this.props.intl;
    return (
      <ReactHighcharts config={this.getChartConfig(mapDataToChartConfig(this.props.data, this.props.gradingSystem, formatMessage))} />
    );
  }
}

const mapStateToProps = (state, props) => {
  const location = state.routesAdmin.locations[props.locationId];
  const gradingSystem = location ? state.routesAdmin.gradingSystems[location.gradingSystem] : {};

  return {
    locationId: props.locationId,
    isLoading: state.adminCharts.isLoading,
    data: state.adminCharts.routesByStyleChart[props.locationId],
    gradingSystem: gradingSystem
  };
}

const mapDispatchToProps = (dispatch) => {
  return {
    fetchRoutesByStyleChart: (locationId) => {
      dispatch(fetchRoutesByStyleChart(locationId));
    }
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(injectIntl(RoutesByStyleChart), { useRef: true }));

function mapDataToChartConfig(data, gradingSystem, formatMessage) {
  let categories = {};
  let knownRanges = {};

  if (!data || !gradingSystem) {
    return { categories, knownRanges };
  }

  for (let i = 0; i < data.length; i++) {
    let item = data[i];
    let routeStyle = mapStyle(item.routeStyle, formatMessage);
    if (!categories[routeStyle]) {
      categories[routeStyle] = {
        name: routeStyle,
        ranges: {}
      };
    }

    if (!knownRanges[item.gradeRange]) {
      knownRanges[item.gradeRange] = { name: mapRangeName(item, gradingSystem, formatMessage) };
    }

    Object.keys(categories).forEach(style => {
      let knownRangeKeys = Object.keys(knownRanges);
      for (let y = 0; y < knownRangeKeys.length; y++) {
        let range = knownRangeKeys[y];
        if (!categories[style].ranges[range]) {
          categories[style].ranges[range] = { amount: 0, name: mapRangeName(item, gradingSystem, formatMessage) };
        }
      }
    });

    categories[routeStyle].ranges[item.gradeRange].amount += item.amount;
  }

  return {
    categories,
    knownRanges
  };
}

function mapRangeName(item, gradingSystem, formatMessage) {
  if (item.rangeMin === null && item.rangeMax === null) {
    return formatMessage(messages.rangeNotGradedTitle);
  }

  let min = getGrade(gradingSystem, item.rangeMin);
  let max = getGrade(gradingSystem, item.rangeMax);

  return min + ' - ' + max;
}

function getGrade(gradingSystem, internalGrade) {
  if (!gradingSystem || !gradingSystem.internalGrades) {
    return '';
  }

  do {
    let index = gradingSystem.internalGrades.indexOf(internalGrade);
    if (index >= 0) {
      return gradingSystem.grades[index];
    }
    internalGrade--;
  } while(internalGrade >= 0);
  return '';
}

function mapStyle(style, formatMessage) {
  switch (style) {
    case 'ROUTE_STYLE_ARETE':
      return formatMessage(messages.tagArete);
    case 'ROUTE_STYLE_CRIMPS':
      return formatMessage(messages.tagCrimps);
    case 'ROUTE_STYLE_DYNO':
      return formatMessage(messages.tagDyno);
    case 'ROUTE_STYLE_MANTLE':
      return formatMessage(messages.tagMantle);
    case 'ROUTE_STYLE_POCKETS':
      return formatMessage(messages.tagPockets);
    case 'ROUTE_STYLE_PINCHES':
      return formatMessage(messages.tagPinches);
    case 'ROUTE_STYLE_TRAVERSE':
      return formatMessage(messages.tagTraverse);
    case 'ROUTE_STYLE_UNDERCLINGS':
      return formatMessage(messages.tagUnderclings);
    case 'ROUTE_STYLE_ENDURANCE':
      return formatMessage(messages.tagEndurance);
    case 'ROUTE_STYLE_POWER':
      return formatMessage(messages.tagPower);
    case 'ROUTE_STYLE_BALANCE':
      return formatMessage(messages.tagBalance);
    case 'ROUTE_STYLE_CIRCUIT':
      return formatMessage(messages.tagCircuit);
    case 'ROUTE_STYLE_SLOPERS':
      return formatMessage(messages.tagSlopers);
    default:
      return formatMessage(messages.tagNone);
  }
}
