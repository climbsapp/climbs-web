import React, { Component } from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';
import ReactHighcharts from 'react-highcharts';
import { defineMessages, injectIntl } from 'react-intl';
import { fetchRouteSettingChart } from '../../../../actions/adminCharts';

const messages = defineMessages({
  title: {
    id: 'view.mygyms.dashboard.setterChart.title',
    defaultMessage: 'Routes by routesetter'
  },
  yAxisTitle: {
    id: 'view.mygyms.dashboard.setterChart.yTitle',
    defaultMessage: 'Amount of routes (set currently)'
  },
  seriesThisMonth: {
    id: 'view.mygyms.dashboard.setterChart.series.thisMonth',
    defaultMessage: 'This month'
  },
  seriesLastMonth: {
    id: 'view.mygyms.dashboard.setterChart.series.lastMonth',
    defaultMessage: 'Last month'
  },
  seriesBefore: {
    id: 'view.mygyms.dashboard.setterChart.series.before',
    defaultMessage: 'Before'
  }
});

class RouteSettingChart extends Component {
  componentDidMount() {
    this.props.fetchRouteSettingChart(this.props.locationId);
  }

  getChartConfig() {
    const { formatMessage } = this.props.intl;
    const data = mapDataToChartConfig(this.props.data);
    return {
      tooltip: {
        shared: true
      },
      chart: {
        style: {
          fontFamily: '"Proxima Nova", "Roboto", "sans-serif"'
        }
      },
      credits: {
        enabled: false
      },
      legend: {
        align: 'center',
        verticalAlign: 'bottom',
        reversed: true
      },
      title: {
        text: formatMessage(messages.title),
        align: 'left',
        x: 45
      },
      xAxis: {
        categories: data ? data.routeSetters : []
      },
      yAxis: [{
        title: {
          text: formatMessage(messages.yAxisTitle)
        }
      }],
      plotOptions: {
        series: {
          stacking: 'normal'
        }
      },
      series: [{
        name: formatMessage(messages.seriesThisMonth),
        yAxis: 0,
        type: 'bar',
        color: '#69d2e7',
        data: data ? data.thisMonthRouteAmounts : []
      }, {
        name: formatMessage(messages.seriesLastMonth),
        yAxis: 0,
        type: 'bar',
        color: '#282828',
        data: data ? data.prevMonthRouteAmounts : []
      }, {
        name: formatMessage(messages.seriesBefore),
        yAxis: 0,
        type: 'bar',
        color: '#fb6a01',
        data: data ? data.oldRouteAmounts : []
      }]
    };
  }

  render() {
    return (
      <ReactHighcharts config={this.getChartConfig()} />
    );
  }
}

const mapStateToProps = (state, props) => {
  return {
    locationId: props.locationId,
    data: state.adminCharts.routeSettersByMonthChart[props.locationId]
  };
}

const mapDispatchToProps = (dispatch) => {
  return {
    fetchRouteSettingChart: (locationId) => {
      dispatch(fetchRouteSettingChart(locationId));
    }
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(injectIntl(RouteSettingChart), { useRef: true }));

function mapDataToChartConfig(data) {
  if (!data) {
    return undefined;
  }

  const today = new Date();
  const prevMonth = new Date(today.getTime() - ((today.getDate() + 1) * 24 * 60 * 60 * 1000));
  const thisMonthStr = today.getFullYear() + ' ' + ('0' + (today.getMonth() + 1)).slice(-2);
  const prevMonthStr = prevMonth.getFullYear() + ' ' + ('0' + (prevMonth.getMonth() + 1)).slice(-2);

  let routeSetters = [];
  let oldRouteAmounts = [];
  let prevMonthRouteAmounts = [];
  let thisMonthRouteAmounts = [];

  for (let i = 0; i < data.length; i++) {
    let item = data[i];
    let thisMonthAmount = 0;
    let prevMonthAmount = 0;
    let oldAmount = 0;
    for (let y = 0; y < item.routeCountList.length; y++) {
      let count = item.routeCountList[y];
      if (count.routeCreationDate === thisMonthStr) {
        thisMonthAmount += count.count;
      } else if (count.routeCreationDate === prevMonthStr) {
        prevMonthAmount += count.count;
      } else {
        oldAmount += count.count;
      }
    }

    routeSetters.push(item.name);
    thisMonthRouteAmounts.push(thisMonthAmount);
    prevMonthRouteAmounts.push(prevMonthAmount);
    oldRouteAmounts.push(oldAmount);
  }

  return {
    routeSetters: routeSetters,
    thisMonthRouteAmounts: thisMonthRouteAmounts,
    prevMonthRouteAmounts: prevMonthRouteAmounts,
    oldRouteAmounts: oldRouteAmounts
  };
}
