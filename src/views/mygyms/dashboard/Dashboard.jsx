import React, { Component } from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';
import { ViewHeader, ViewHeaderAction } from '../../../components';
import GradeDistributionChart from './charts/GradeDistributionChart';
import RouteSettingChart from './charts/RouteSettingChart';
import RoutesByStyleChart from './charts/RoutesByStyleChart';

class Dashboard extends Component {
  showEdit = () => {
    const locationId = this.props.params.locationId;
    this.props.router.push('/mygyms/' + locationId + '/dashboard/edit');
  }

  showEditHeaderImage = () => {
    const locationId = this.props.params.locationId;
    this.props.router.push('/mygyms/' + locationId + '/dashboard/editheaderimage');
  }

  render() {
    const venue = this.props.venue || { name: '' };
    return (
      <div className="Dashboard">
        {this.props.children}
        <ViewHeader title={venue.name}>
          <ViewHeaderAction onClick={this.showEdit} type="Edit" />
          <ViewHeaderAction onClick={this.showEditHeaderImage} type="Image" />
        </ViewHeader>
        <div className="Content-Area">
          <GradeDistributionChart locationId={this.props.selectedLocationId} />
        </div>
        <div><p /></div>
        <div className="Content-Area">
          <div className="row">
            <div className="col s6">
              <RouteSettingChart locationId={this.props.selectedLocationId} />
            </div>
            <div className="col s6">
              <RoutesByStyleChart locationId={this.props.selectedLocationId} />
            </div>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state, props) => {
  return {
    selectedLocationId: props.params.locationId,
    venue: state.routesAdmin.locations[props.params.locationId]
  };
}

export default connect(mapStateToProps)(withRouter(Dashboard));
