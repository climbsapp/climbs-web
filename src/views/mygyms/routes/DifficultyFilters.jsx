import React, { Component } from 'react';
import { defineMessages, injectIntl } from 'react-intl';
import { SelectableFilter } from '../../../components/table/Filters';

/* Difficulty filter values */
export const F_DIFFICULTIES_ALL = 'DIFFICULTIES_ALL';
export const F_DIFFICULTIES_UNKNOWN = 'DIFFICULTIES_UNKNOWN';
export const F_DIFFICULTIES_EASY = 'DIFFICULTIES_EASY';
export const F_DIFFICULTIES_AVERAGE = 'DIFFICULTIES_AVERAGE';
export const F_DIFFICULTIES_HARD = 'DIFFICULTIES_HARD';
export const F_DIFFICULTIES_EXPERT = 'DIFFICULTIES_EXPERT';

const messages = defineMessages({
  all: {
    id: 'view.mygyms.routes.filters.difficulty.all',
    defaultMessage: 'All difficulties'
  },
  unknown: {
    id: 'view.mygyms.routes.filters.difficulty.unknown',
    defaultMessage: 'Not graded ({amount, number})'
  },
  easy: {
    id: 'view.mygyms.routes.filters.difficulty.easy',
    defaultMessage: 'Easy routes ({amount, number})'
  },
  medium: {
    id: 'view.mygyms.routes.filters.difficulty.medium',
    defaultMessage: 'Medium routes ({amount, number})'
  },
  hard: {
    id: 'view.mygyms.routes.filters.difficulty.hard',
    defaultMessage: 'Hard routes ({amount, number})'
  },
  expert: {
    id: 'view.mygyms.routes.filters.difficulty.expert',
    defaultMessage: 'Expert routes ({amount, number})'
  }
});

class DifficultyFiltersView extends Component {
  state = {
    amounts: {
      DIFFICULTIES_UNKNOWN: 0,
      DIFFICULTIES_EASY: 0,
      DIFFICULTIES_AVERAGE: 0,
      DIFFICULTIES_HARD: 0,
      DIFFICULTIES_EXPERT: 0
    }
  }

  componentWillReceiveProps(nextProps) {
    const amounts = { DIFFICULTIES_UNKNOWN: 0, DIFFICULTIES_EASY: 0, DIFFICULTIES_AVERAGE: 0, DIFFICULTIES_HARD: 0, DIFFICULTIES_EXPERT: 0 };
    for (let i = 0; i < nextProps.routes.length; i++) {
      const route = nextProps.routes[i];
      const grade = route.internalGrade;
      if (grade === null) {
        amounts[F_DIFFICULTIES_UNKNOWN]++;
      } else if (grade <= 17) {
        amounts[F_DIFFICULTIES_EASY]++;
      } else if (grade > 17 && grade <= 26) {
        amounts[F_DIFFICULTIES_AVERAGE]++;
      } else if (grade > 26 && grade <= 35) {
        amounts[F_DIFFICULTIES_HARD]++;
      } else {
        amounts[F_DIFFICULTIES_EXPERT]++;
      }
    }

    this.setState({ amounts: amounts });
  }

  render() {
    const { formatMessage } = this.props.intl;
    const { amounts } = this.state;
    let difficultyFilters = [];
    const { values, checkFilter } = this.props;
    if (values.indexOf(F_DIFFICULTIES_ALL) > -1) {
      difficultyFilters.push(<SelectableFilter checked={true} title={ formatMessage(messages.all) }
                          key="allDifficulties" onChange={checkFilter(F_DIFFICULTIES_ALL)} />);
    } else {
      difficultyFilters.push(<SelectableFilter checked={false} title={ formatMessage(messages.all) }
                          key="allDifficulties" onChange={checkFilter(F_DIFFICULTIES_ALL)} />);
      const difficulty0checked = values.indexOf(F_DIFFICULTIES_UNKNOWN) > -1;
      difficultyFilters.push(<SelectableFilter checked={difficulty0checked} 
                          title={ formatMessage(messages.unknown, { amount: amounts[F_DIFFICULTIES_UNKNOWN] || 0 }) }
                          key="difficulties0" onChange={checkFilter(F_DIFFICULTIES_UNKNOWN)} />);
      const difficulty2checked = values.indexOf(F_DIFFICULTIES_EASY) > -1;
      difficultyFilters.push(<SelectableFilter checked={difficulty2checked}
                          title={ formatMessage(messages.easy, { amount: amounts[F_DIFFICULTIES_EASY] }) }
                          key="difficulties2" onChange={checkFilter(F_DIFFICULTIES_EASY)} />);
      const difficulty3checked = values.indexOf(F_DIFFICULTIES_AVERAGE) > -1;
      difficultyFilters.push(<SelectableFilter checked={difficulty3checked}
                          title={ formatMessage(messages.medium, { amount: amounts[F_DIFFICULTIES_AVERAGE] }) }
                          key="difficulties3" onChange={checkFilter(F_DIFFICULTIES_AVERAGE)} />);
      const difficulty4checked = values.indexOf(F_DIFFICULTIES_HARD) > -1;
      difficultyFilters.push(<SelectableFilter checked={difficulty4checked}
                          title={ formatMessage(messages.hard, { amount: amounts[F_DIFFICULTIES_HARD] }) }
                          key="difficulties4" onChange={checkFilter(F_DIFFICULTIES_HARD)} />);
      const difficulty5checked = values.indexOf(F_DIFFICULTIES_EXPERT) > -1;
      difficultyFilters.push(<SelectableFilter checked={difficulty5checked}
                          title={ formatMessage(messages.expert, { amount: amounts[F_DIFFICULTIES_EXPERT] }) }
                          key="difficulties5" onChange={checkFilter(F_DIFFICULTIES_EXPERT)} />);
    }

    return (
      <div>
        { difficultyFilters }
      </div>
    );
  }
}

export const DifficultyFilters = injectIntl(DifficultyFiltersView);
