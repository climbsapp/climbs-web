import React, { Component } from 'react';
import { defineMessages, injectIntl } from 'react-intl';
import { SelectableFilter } from '../../../components/table/Filters';

/* Route style filter values */
export const F_STYLE_ALL = Symbol('style_all');
export const F_STYLE_POWER = 'ROUTE_STYLE_POWER';
export const F_STYLE_ENDURANCE = 'ROUTE_STYLE_ENDURANCE';
export const F_STYLE_BALANCE = 'ROUTE_STYLE_BALANCE';
export const F_STYLE_CIRCUIT = 'ROUTE_STYLE_CIRCUIT';
export const F_STYLE_CRIMPS = 'ROUTE_STYLE_CRIMPS';
export const F_STYLE_POCKETS = 'ROUTE_STYLE_POCKETS';
export const F_STYLE_PINCHES = 'ROUTE_STYLE_PINCHES';

const messages = defineMessages({
  styleAll: {
    id: 'view.mygyms.routes.filters.style.all',
    defaultMessage: 'All route styles'
  },
  stylePower: {
    id: 'view.mygyms.routes.filters.style.power',
    defaultMessage: 'Power'
  },
  styleEndurance: {
    id: 'view.mygyms.routes.filters.style.endurance',
    defaultMessage: 'Endurance'
  },
  styleBalance: {
    id: 'view.mygyms.routes.filters.style.balance',
    defaultMessage: 'Technical'
  },
  styleCircuit: {
    id: 'view.mygyms.routes.filters.style.circuit',
    defaultMessage: 'Circuit'
  },
  styleCrimps: {
    id: 'view.mygyms.routes.filters.style.crimps',
    defaultMessage: 'Fingery'
  },
  stylePockets: {
    id: 'view.mygyms.routes.filters.style.pockets',
    defaultMessage: 'Pockets'
  },
  stylePinches: {
    id: 'view.mygyms.routes.filters.style.pinches',
    defaultMessage: 'Pinches'
  }
});

class RouteStyleFiltersView extends Component {
  state = {
    filters: []
  }

  componentDidMount() {
    const { formatMessage } = this.props.intl;
    this.setState({
      filters: [
        { key: 'routeStylePower', value: F_STYLE_POWER, title: formatMessage(messages.stylePower) },
        { key: 'routeStyleEndurance', value: F_STYLE_ENDURANCE, title: formatMessage(messages.styleEndurance) },
        { key: 'routeStyleBalance', value: F_STYLE_BALANCE, title: formatMessage(messages.styleBalance) },
        { key: 'routeStyleCircuit', value: F_STYLE_CIRCUIT, title: formatMessage(messages.styleCircuit) },
        { key: 'routeStyleCrimps', value: F_STYLE_CRIMPS, title: formatMessage(messages.styleCrimps) },
        { key: 'routeStylePockets', value: F_STYLE_POCKETS, title: formatMessage(messages.stylePockets) },
        { key: 'routeStylePinches', value: F_STYLE_PINCHES, title: formatMessage(messages.stylePinches) },
      ]
    });
  }

  render() {
    const { formatMessage } = this.props.intl;
    let routeStyleFilters = [];
    const { values, checkFilter } = this.props;
    if (values.indexOf(F_STYLE_ALL) > -1) {
      routeStyleFilters.push(<SelectableFilter checked={true} title={ formatMessage(messages.styleAll) }
                        key="allRouteStyles" onChange={checkFilter(F_STYLE_ALL)} />);
    } else {
      routeStyleFilters.push(<SelectableFilter checked={false} title={ formatMessage(messages.styleAll) }
                        key="allRouteStyles" onChange={checkFilter(F_STYLE_ALL)} />);
      for (let i = 0; i < this.state.filters.length; i++) {
        const filter = this.state.filters[i];
        const checked = values.indexOf(filter.value) > -1;
        routeStyleFilters.push(<SelectableFilter checked={checked} title={filter.title}
          key={filter.key} onChange={checkFilter(filter.value)} />
        );
      }
    }
    return (<div>{ routeStyleFilters }</div>);
  }
}

export const RouteStyleFilters = injectIntl(RouteStyleFiltersView);
