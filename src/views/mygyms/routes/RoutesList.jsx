import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Link, withRouter } from 'react-router';
import { defineMessages, injectIntl } from 'react-intl';
import WallNotifications from './WallNotifications';
import { ViewHeader } from '../../../components/header/ViewHeader';
import { SelectableFilter } from '../../../components/table/Filters';
import { RouteStyleFilters, F_STYLE_ALL } from './RouteStyleFilters';
import { RouteTypeFilters,
         F_TYPE_ALL, F_TYPE_AUTOBELAY, F_TYPE_BOULDER,
         F_TYPE_LEAD, F_TYPE_TOPROPE } from './RouteTypeFilters';
import { DifficultyFilters,
         F_DIFFICULTIES_ALL, F_DIFFICULTIES_UNKNOWN,
         F_DIFFICULTIES_EASY, F_DIFFICULTIES_AVERAGE,
         F_DIFFICULTIES_HARD, F_DIFFICULTIES_EXPERT } from './DifficultyFilters.jsx';
import { ScoreFilters,
         F_SCORES_ALL } from './ScoreFilters';
import { getColorStyle } from '../../../helpers/style';
import './RoutesList.css';

const SORTING_CREATED = Symbol('sorting_created');
const SORTING_TO_CHANGE = Symbol('change');
const SORTING_ROUTESETTER = Symbol('sorting_routesetter');
const SORTING_ROUTE = Symbol('sorting_route');
const SORTING_SEND_COUNT = 'SORTING_SEND_COUNT';
const SORTING_ASC = 'ASC';
const SORTING_DESC = 'DESC';
const F_ROUTESETTERS_ALL = Symbol('all');

const messages = defineMessages({
  filters: {
    id: 'view.mygyms.routes.filters.title',
    defaultMessage: 'Filters'
  },
  routesettersAll: {
    id: 'view.mygyms.routes.filters.routesetters.all',
    defaultMessage: 'All routesetters'
  },
  titleRoute: {
    id: 'view.mygyms.routes.table.header.route',
    defaultMessage: 'Route'
  },
  titleRouteSetter: {
    id: 'view.mygyms.routes.table.header.routeSetter',
    defaultMessage: 'Routesetter'
  },
  titleWall: {
    id: 'view.mygyms.routes.table.header.wall',
    defaultMessage: 'Wall'
  },
  titleName: {
    id: 'view.mygyms.routes.table.header.name',
    defaultMessage: 'Name'
  },
  titleStyle: {
    id: 'view.mygyms.routes.table.header.style',
    defaultMessage: 'Style'
  },
  titleSendCount: {
    id: 'view.mygyms.routes.table.header.sendCount',
    defaultMessage: 'Sends'
  },
  titleCreated: {
    id: 'view.mygyms.routes.table.header.created',
    defaultMessage: 'Created'
  },
  titleDaysToChange: {
    id: 'view.mygyms.routes.table.header.daysToChange',
    defaultMessage: 'Days to change'
  },
  routeAgeTitle: {
    id: 'view.mygyms.routes.title.routeCreated',
    defaultMessage: 'Set {createDate, date}'
  },
  lineTitle: {
    id: 'view.mygyms.routes.title.line',
    defaultMessage: 'Line {lineNumber}'
  },
  lineShortTitle: {
    id: 'view.mygyms.routes.title.line.short',
    defaultMessage: 'L{lineNumber}'
  },
  noRouteSetterLabel: {
    id: 'view.mygyms.routes.value.routeSetter.unknown',
    defaultMessage: '<unknown>'
  },
  twoRouteSetterTitle: {
    id: 'view.mygyms.routes.title.routeSetter.two',
    defaultMessage: '{setterName} and {setter2Name}'
  },
  noRoutesText: {
    id: 'view.mygyms.routes.noRoutes.text',
    defaultMessage: 'Dang! No routes found with given filtering options.'
  },
  dueNotSet: {
    id: 'view.mygyms.routes.due.notSet',
    defaultMessage: 'Not set'
  },
  dueSet: {
    id: 'view.mygyms.routes.due.dueSet',
    defaultMessage: 'Due {dueRelative}'
  },
  tagsTitleSelect: {
    id: 'view.mygyms.routes.tags.title',
    defaultMessage: '{tag, select, crimps {crimps} slopers {slopers} power {power} pinches {pinches} pockets {pockets} endurance {encurance} balance {technical} arete {arete} mantle {mantle} dyno {dyno} circuit {circuit} traverse {traverse} underclings {underclings} }'
  },
  tagsTitleAbbr: {
    id: 'view.mygyms.routes.tags.abbreviation',
    defaultMessage: '{tag, select, crimps {Cr} slopers {Sp} power {Pw} pinches {Pn} pockets {Pt} endurance {En} balance {Tn} arete {Ar} mantle {Mn} dyno {Dn} circuit {Cr} traverse {Tv} underclings {Uc} }'
  }
});

class RoutesList extends Component {
  state = {
    sorting: SORTING_TO_CHANGE,
    sortOrder: SORTING_ASC,
    routeSetters: [F_ROUTESETTERS_ALL],
    scores: [F_SCORES_ALL],
    difficulties: [F_DIFFICULTIES_ALL],
    routeTypes: [F_TYPE_ALL],
    routeStyles: [F_STYLE_ALL],
    routesSetByRouteSetter: {}
  }

  componentWillReceiveProps(nextProps) {
    // Calculate the amount of routes set by each routeSetter
    let routesSetByRouteSetter = {};
    nextProps.routes.forEach(route => {
      if (!routesSetByRouteSetter[route.routeSetterId]) {
        routesSetByRouteSetter[route.routeSetterId] = 0;
      }
      routesSetByRouteSetter[route.routeSetterId]++;
    });
    this.setState({ routesSetByRouteSetter: routesSetByRouteSetter });
  }

  routeSetterComparator = (r1, r2) => {
    const { formatMessage } = this.props.intl;
    const { routeSetters } = this.props;
    const r1Setter = routeSetters[r1.routeSetterId] ? routeSetters[r1.routeSetterId].name : formatMessage(messages.noRouteSetterLabel);
    const r2Setter = routeSetters[r2.routeSetterId] ? routeSetters[r2.routeSetterId].name : formatMessage(messages.noRouteSetterLabel);
    if (r1Setter === r2Setter) {
      return 0;
    }
    return r1Setter > r2Setter ? 1 : -1;
  }

  routeComparator = (r1, r2) => {
    return r1.internalGrade - r2.internalGrade;
  }

  routeChangeComparator = (r1, r2) => {
    const { walls } = this.props;
    const now = Date.now();
    const r1Interval = walls[r1.wallId].newRouteInterval;
    const r2Interval = walls[r2.wallId].newRouteInterval;
    const r1ToChange = r1Interval === 0 ? Infinity :
      r1Interval - Math.floor((now - r1.routeCreated) / (24*60*60*1000));
    const r2ToChange = r2Interval === 0 ? Infinity :
      r2Interval - Math.floor((now - r2.routeCreated) / (24*60*60*1000));
    return r1ToChange - r2ToChange;
  }

  routeSendCountComparator = (r1, r2) => {
    return r1.sendCount - r2.sendCount;
  }

  routeCreatedComparator(r1, r2) {
      let r1Year = r1.routeCreated.getFullYear();
      let r2Year = r2.routeCreated.getFullYear();
      if (r1Year - r2Year !== 0) {
        return r1Year - r2Year;
      }

      let r1Month = r1.routeCreated.getMonth();
      let r2Month = r2.routeCreated.getMonth();
      if (r1Month - r2Month !== 0) {
        return r1Month - r2Month;
      }

      let r1Date = r1.routeCreated.getDate();
      let r2Date = r2.routeCreated.getDate();
      if (r1Date - r2Date !== 0) {
        return r1Date - r2Date;
      }

      let lineComparison = r1.line - r2.line;
      if (lineComparison !== 0) {
        return lineComparison;
      }

      return r1.internalGrade - r2.internalGrade;
  }

  routeFilter = (value) => {
    // See if all routesetters or none are selected
    if (this.state.routeSetters.indexOf(F_ROUTESETTERS_ALL) > -1
        || this.state.routeSetters.length === 0) {
      return true;
    }

    return (this.state.routeSetters.indexOf(value.routeSetterId.toString()) > -1);
  }

  scoreFilter = (value) => {
    // See if all routesetters or none are selected
    if (this.state.scores.indexOf(F_SCORES_ALL) > -1
        || this.state.scores.length === 0) {
      return true;
    }

    return (this.state.scores.indexOf(value.communityScore) > -1);
  }

  difficultyFilter = (value) => {
    if (this.state.difficulties.indexOf(F_DIFFICULTIES_ALL) > -1
        || this.state.difficulties.length === 0) {
      return true;
    }

    // Easy
    const grade = value.internalGrade;
    if (grade === null) {
      return this.state.difficulties.indexOf(F_DIFFICULTIES_UNKNOWN) > -1;
    } else if (grade <= 17) {
      return this.state.difficulties.indexOf(F_DIFFICULTIES_EASY) > -1;
    } else if (grade > 17 && grade <= 26) {
      return this.state.difficulties.indexOf(F_DIFFICULTIES_AVERAGE) > -1;
    } else if (grade > 26 && grade <= 35) {
      return this.state.difficulties.indexOf(F_DIFFICULTIES_HARD) > -1;
    } else {
      return this.state.difficulties.indexOf(F_DIFFICULTIES_EXPERT) > -1;
    }
  }

  styleFilter = (value) => {
    const { routeStyles } = this.state;
    if (routeStyles.indexOf(F_STYLE_ALL) > -1 || routeStyles.length === 0) {
      return true;
    }

    const tags = value.tags || [];
    for (let i = 0; i < routeStyles.length; i++) {
      const tagKey = routeStyles[i];
      if (tags.find(tag => tagKey === tag.tagKey)) {
        return true;
      }
    }
    return false;
  }

  routeTypeFilter = (value) => {
    const { routeTypes } = this.state;
    if (this.state.routeTypes.indexOf(F_TYPE_ALL) > -1
        || this.state.routeTypes.length === 0) {
      return true;
    }

    switch (value.type) {
      case 3:
        return routeTypes.indexOf(F_TYPE_AUTOBELAY) > -1;
      case 4:
        return routeTypes.indexOf(F_TYPE_BOULDER) > -1;
      case 1:
        return routeTypes.indexOf(F_TYPE_LEAD) > -1;
      case 2:
        return routeTypes.indexOf(F_TYPE_TOPROPE) > -1;
      case 7: // Lead & toprope
        return routeTypes.indexOf(F_TYPE_LEAD) > -1 || routeTypes.indexOf(F_TYPE_TOPROPE) > -1;
      default:
        return true;
    }
  }

  getRoutesSorted(routes) {
    let matchingRoutes;
    switch (this.state.sorting) {
      case SORTING_CREATED:
        // Works only for date
        matchingRoutes = [...routes].sort(this.routeCreatedComparator);
        break;
      case SORTING_TO_CHANGE:
        matchingRoutes = [...routes].sort(this.routeChangeComparator);
        break;
      case SORTING_ROUTESETTER:
        matchingRoutes = [...routes].sort(this.routeSetterComparator);
        break;
      case SORTING_ROUTE:
        matchingRoutes = [...routes].sort(this.routeComparator);
        break;
      case SORTING_SEND_COUNT:
        matchingRoutes = [...routes].sort(this.routeSendCountComparator);
        break;
      default:
        matchingRoutes = [];
    }

    if (this.state.sortOrder === SORTING_DESC) {
      matchingRoutes = matchingRoutes.reverse();
    }

    return matchingRoutes.filter(this.routeFilter)
                         .filter(this.scoreFilter)
                         .filter(this.difficultyFilter)
                         .filter(this.routeTypeFilter)
                         .filter(this.styleFilter);
  }

  setSorting = (newSorting) => {
    return (event) => {
      event.preventDefault();
      const { sorting, sortOrder } = this.state;
      if (sorting === newSorting) {
        const newOrder = sortOrder === SORTING_ASC ? SORTING_DESC : SORTING_ASC;
        this.setState({ sortOrder: newOrder });
      } else {
        this.setState({ sorting: newSorting, sortOrder: SORTING_ASC });
      }
    };
  }

  getRouteSetterLabel(route, routeSetters) {
    if (routeSetters[route.routeSetterId] && routeSetters[route.routeSetter2Id]) {
      return routeSetters[route.routeSetterId].name + ' +1';
    } else if (routeSetters[route.routeSetterId]) {
      return routeSetters[route.routeSetterId].name;
    } else {
      const { formatMessage } = this.props.intl;
      return formatMessage(messages.noRouteSetterLabel);
    }
  }

  getRouteSetterTitle(route, routeSetters) {
    const { formatMessage } = this.props.intl;
    if (routeSetters[route.routeSetterId] && routeSetters[route.routeSetter2Id]) {
      return formatMessage(messages.twoRouteSetterTitle, { setterName: routeSetters[route.routeSetterId].name, setter2Name: routeSetters[route.routeSetter2Id].name });
    } else if (routeSetters[route.routeSetterId]) {
      return routeSetters[route.routeSetterId].name;
    } else {
      return formatMessage(messages.noRouteSetterLabel);
    }
  }

  renderRoutes = () => {
    const { routes, colors, walls, routeSetters } = this.props;
    const { sorting, sortOrder } = this.state;
    const routesSorted = this.getRoutesSorted(routes);
    const routeElements = [<RouteListHeader key="header" setSorting={this.setSorting} sorting={sorting} sortOrder={sortOrder} />];
    const now = Date.now();
    routesSorted.forEach(route => {
      const wall = walls[route.wallId];
      const color = colors[route.colorId];
      const wallName = wall.name;
      const routeSetter = this.getRouteSetterLabel(route, routeSetters);
      const routeSetterTitle = this.getRouteSetterTitle(route, routeSetters);
      const ageDays = Math.floor((now - route.routeCreated) / (24*60*60*1000));
      const ageRelative = 0 !== wall.newRouteInterval ? wall.newRouteInterval - ageDays : undefined;
      routeElements.push(<RouteListElement key={route.routeId} route={route}
        color={color} wallName={wallName} routeSetter={routeSetter} routeSetterTitle={routeSetterTitle}
        createDate={route.routeCreated} ageDays={ageDays} ageRelative={ageRelative} />);
    });

    if (routeElements.length === 1) {
      routeElements.push(<NoRoutesListElement key="noRoutes" />);
    }

    return routeElements;
  }

  renderFilters = () => {
    const { routes } = this.props;
    return (
      <div className="Filters">
        { this.renderRouteSettersFilterValues() }
        <div className="FilterDivider" />
        <ScoreFilters values={this.state.scores} checkFilter={this.checkScoreFilter} routes={routes} />
        <div className="FilterDivider" />
        <DifficultyFilters values={this.state.difficulties} checkFilter={this.checkDifficultyFilter} routes={routes} />
        <div className="FilterDivider" />
        <RouteTypeFilters values={this.state.routeTypes} checkFilter={this.checkRouteTypeFilter} routes={routes} />
        <div className="FilterDivider" />
        <RouteStyleFilters values={this.state.routeStyles} checkFilter={this.checkRouteStyleFilter} />
      </div>
    );
  }

  checkRouteStyleFilter = (value) => {
    return (event) => {
      const { routeStyles } = this.state;
      const index = routeStyles.indexOf(value);
      if (index > -1) {
        this.setState({ routeStyles: routeStyles.slice(0,index).concat(routeStyles.slice(index+1)) });
      } else {
        if (value === F_STYLE_ALL) {
          this.setState({ routeStyles: [value] });
        } else {
          this.setState({ routeStyles: [...routeStyles, value] });
        }
      }
    };
  }

  checkDifficultyFilter = (value) => {
    return (event) => {
      const { difficulties } = this.state;
      const index = difficulties.indexOf(value);
      if (index > -1) {
        this.setState({ difficulties: difficulties.slice(0,index).concat(difficulties.slice(index+1)) });
      } else {
        if (value === F_DIFFICULTIES_ALL) {
          this.setState({ difficulties: [value] });
        } else {
          this.setState({ difficulties: [...difficulties, value] });
        }
      }
    };
  }

  checkRouteTypeFilter = (value) => {
    return (event) => {
      const { routeTypes } = this.state;
      const index = routeTypes.indexOf(value);
      if (index > -1) {
        this.setState({ routeTypes: routeTypes.slice(0,index).concat(routeTypes.slice(index+1)) });
      } else {
        if (value === F_TYPE_ALL) {
          this.setState({ routeTypes: [value] });
        } else {
          this.setState({ routeTypes: [...routeTypes, value] });
        }
      }
    };
  }

  checkRouteSetterFilter = (value) => {
    return (event) => {
      const { routeSetters } = this.state;
      const index = routeSetters.indexOf(value);
      if (index > -1) {
        this.setState({ routeSetters: routeSetters.slice(0,index).concat(routeSetters.slice(index+1)) });
      } else {
        if (value === F_ROUTESETTERS_ALL) {
          this.setState({ routeSetters: [value] });
        } else {
          this.setState({ routeSetters: [...routeSetters, value] });
        }
      }
    };
  }

  checkScoreFilter = (value) => {
    return (event) => {
      const { scores } = this.state;
      const index = scores.indexOf(value);
      if (index > -1) {
        this.setState({ scores: scores.slice(0,index).concat(scores.slice(index+1)) });
      } else {
        if (value === F_SCORES_ALL) {
          this.setState({ scores: [value] });
        } else {
          this.setState({ scores: [...scores, value] });
        }
      }
    };
  }

  renderRouteSettersFilterValues() {
    const { formatMessage } = this.props.intl;
    let routeSetterValues = [];
    const { routeSetters, routesSetByRouteSetter } = this.state;
    if (routeSetters.indexOf(F_ROUTESETTERS_ALL) > -1) {
      routeSetterValues.push(<SelectableFilter checked={true} title={ formatMessage(messages.routesettersAll) } key="all" onChange={this.checkRouteSetterFilter(F_ROUTESETTERS_ALL)} />);
    } else {
      routeSetterValues.push(<SelectableFilter checked={false} title={ formatMessage(messages.routesettersAll) } key="all" onChange={this.checkRouteSetterFilter(F_ROUTESETTERS_ALL)} />);
      const routeSettersAll = this.props.routeSetters;
      routeSetterValues.push(Object.keys(routeSettersAll)
          .sort((r1, r2) => (routesSetByRouteSetter[r2] || 0) - (routesSetByRouteSetter[r1] || 0))
          .map(id => {
            const routeSetter = routeSettersAll[id];
            const routesSet = routesSetByRouteSetter[id];
            if (!routesSet) {
              return undefined;
            }
            const title = routeSetter.name + ' (' + routesSet + ')';
            const checked = routeSetters.indexOf(id) > -1;
            return (
              <SelectableFilter checked={checked} title={title} key={id}
                onChange={this.checkRouteSetterFilter(id)} />
            );
          }));
    }

    return routeSetterValues;
  }

  render() {
    const { formatMessage } = this.props.intl;
    return (
      <div className="RoutesList">
        <WallNotifications />
        <ViewHeader title="Routes" />
        { this.props.children }
        <div className="Content-Area">
          <div className="row">
            <div className="col s3">
              <div className="ListHeading">
                { formatMessage(messages.filters) }
              </div>
              { this.renderFilters() }
            </div>
            <div className="col s9">
              <ul id="Routes">
                { this.renderRoutes() }
              </ul>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

class RouteListHeaderComponent extends Component {
  getHeader(title, name, sortingName) {
    const { setSorting, sortOrder, sorting } = this.props;
    const onClick = sorting ? setSorting(sortingName) : undefined;
    let className = name;
    if (sorting === sortingName ) {
        className += ' Sorted ' + sortOrder;
    }
    if (sortingName) {
      className += ' Sortable';
    }
    return (
      <div className={className} onClick={onClick}><span>{title}</span></div>
    );
  }

  render() {
    const { formatMessage } = this.props.intl;
    return (
      <li className="RouteListElement RouteListHeader">
        <div className="Wall">{ formatMessage(messages.titleWall) }</div>
        { this.getHeader(formatMessage(messages.titleRoute), 'Route', SORTING_ROUTE) }
        <div className="Name">{ formatMessage(messages.titleName) }</div>
        { this.getHeader(formatMessage(messages.titleSendCount), 'SendCount', SORTING_SEND_COUNT) }
        { this.getHeader(formatMessage(messages.titleRouteSetter), 'RouteSetter', SORTING_ROUTESETTER) }
        <div className="Style">{ formatMessage(messages.titleStyle) }</div>
        { this.getHeader(formatMessage(messages.titleCreated), 'RouteCreated', SORTING_CREATED) }
        { this.getHeader(formatMessage(messages.titleDaysToChange), 'RouteAge', SORTING_TO_CHANGE) }
      </li>
    );
  }
}

const RouteListHeader = injectIntl(RouteListHeaderComponent);

class RouteListElementComponent extends Component {
  getAgeClassName() {
    const { ageRelative } = this.props;
    if (ageRelative === undefined) {
      return 'chip NotSet';
    } else if (ageRelative <= 0) {
      return 'Late';
    } else if (ageRelative > 0 && ageRelative <= 3) {
      return 'SoonLate';
    } else {
      return 'NotLate';
    }
  }

  formatDueRelative = () => {
    const { formatMessage, formatRelative } = this.props.intl;
    const { ageRelative, route } = this.props;
    if (ageRelative === undefined) {
      return formatMessage(messages.dueNotSet);
    } else {
      return formatMessage(messages.dueSet, { dueRelative: formatRelative(route.routeCreated.getTime() + 1000 * 60 * 60 * 24, { units: 'day' })});
    }
  }

  render() {
    const { formatMessage, formatRelative, formatDate } = this.props.intl;
    const { route, color, routeSetter, routeSetterTitle, wallName } = this.props;
    const { communityScore, grade, line, name, sendCount } = route;
    const routeColorClassName = 'RouteColor' + (!color.hex ? ' Transparent' : '');
    const routeColorStyle = getColorStyle(color);
    const createDate = formatDate(route.routeCreated);
    const createdRelative = formatRelative(route.routeCreated);
    const locationId = this.props.params.locationId;

    return (
      <li className="RouteListElement">
        <div className="Wall" title={wallName}>{wallName}</div>
        <div className="Route">
          <Link to={`mygyms/${locationId}/routes/edit/${route.routeId}`}>
            <div className="RouteLine" title={ formatMessage(messages.lineTitle, { lineNumber: line }) }>
              { formatMessage(messages.lineShortTitle, { lineNumber: line }) }
            </div>
            <div className="RouteDivider">/</div>
            <div className={routeColorClassName} style={routeColorStyle}>
              <span className="RouteGrade">
                { grade }
              </span>
            </div>
            <div className="RouteDivider" style={{display: (communityScore >= 1 ? 'inline-block' : 'none')}}>/</div>
            <div className="RouteStar" style={ {display: (communityScore >= 1 ? 'inline-block' : 'none') }} />
            <div className="RouteStar" style={ {display: (communityScore >= 2 ? 'inline-block' : 'none') }} />
            <div className="RouteStar" style={ {display: (communityScore >= 3 ? 'inline-block' : 'none') }} />
          </Link>
        </div>
        <div className="Name">{ name }</div>
        <div className="SendCount">
          <Link to={`mygyms/${locationId}/routes/info/${route.routeId}`}>
            { sendCount }
          </Link>
        </div>
        <div className="RouteSetter" title={routeSetterTitle}>{ routeSetter }</div>
        <div className="Style">
          <RouteTags tags={route.tags} />
        </div>
        <div className="RouteCreated" title={createDate}>{createdRelative}</div>
        <div className="RouteAge" title={ formatMessage(messages.routeAgeTitle, { createDate: route.routeCreated }) }>
          <div className={this.getAgeClassName()}>
            {this.formatDueRelative()}
          </div>
        </div>
      </li>
    );
  }
}

const RouteListElement = withRouter(injectIntl(RouteListElementComponent));

class NoRoutesListElementComponent extends Component {
  render() {
    const { formatMessage } = this.props.intl;
    return (
      <li className="NoRoutesListElement">
        <div className="NoRoutes">
          { formatMessage(messages.noRoutesText) }
        </div>
      </li>
    );
  }
}

const NoRoutesListElement = injectIntl(NoRoutesListElementComponent);

class RouteTagsComponent extends Component {
  render() {
    const tagElements = this.renderTags();
    return (
      <div className="RouteTags">
        { tagElements }
      </div>
    );
  }

  renderTags() {
    const { formatMessage } = this.props.intl;
    const { tags } = this.props;
    let tagElements = [];
    for (let i = 0; i < tags.length; i++) {
      const tag = tags[i];
      const key = tag.targetName + '-' + tag.targetId + '-' + tag.tagKey;
      const className = 'RouteTag ' + tag.tagKey;
      const tagKey = tag.tagKey.split('_')[2].toLowerCase();
      const title = formatMessage(messages.tagsTitleSelect, { tag: tagKey });
      const titleAbbr = formatMessage(messages.tagsTitleAbbr, { tag: tagKey });
      tagElements.push(<div className={className} key={key} title={title}>{titleAbbr}</div>);
    }
    return tagElements;
  }
}

const mapStateToProps = (state, props) => {
  const { routesAdmin } = state;
  return {
    routes: Object.keys(routesAdmin.routes).map(routeId => routesAdmin.routes[routeId]),
    colors: routesAdmin.colors,
    walls: routesAdmin.walls,
    routeSetters: routesAdmin.routeSetters
  };
}

const RouteTags = injectIntl(RouteTagsComponent);

export default connect(mapStateToProps)(injectIntl(RoutesList));
