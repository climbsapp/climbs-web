import React, { Component } from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';
import { ViewHeader } from '../../../components/header/ViewHeader';
import { editWall } from '../../../actions/routesAdmin';
import './WallNotifications.css';

class WallNotifications extends Component {
  checkNotification = (data) => {
    return () => {
      const { locationId } = this.props.params;
      const wallData = Object.assign({}, data, { internalInfo: '' });
      this.props.checkNotification(wallData, locationId);
    };
  }

  renderWalls() {
    const { walls } = this.props;
    return walls.map(wall => {
      return (
        <div key={'notified-wall-' + wall.wallId} className="col s4">
          <div className="WallNotification">
            <div className="WallInfo">
              <div className="AreaName">{wall.areaName}</div>
              <div className="RouteDivider">/</div>
              <div className="WallName">{wall.wallName}</div>
              <div className="NotificationAction" title="Mark done" onClick={this.checkNotification(wall.data)} />
            </div>
            <div>{wall.internalInfo}</div>
          </div>
        </div>
      );
    });
  }

  render() {
    const wallElements = this.renderWalls();
    if (wallElements.length === 0) {
      return null;
    }

    return (
      <div className="WallNotifications">
        <ViewHeader title="Wall notifications" />
        <div className="Content-Area">
          <div className="row">
            { wallElements }
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state, props) => {
  const { walls, areas } = state.routesAdmin;
  let notifiedWalls = Object.keys(walls)
    .map(wallId => walls[wallId])
    .filter(wall => wall.internalInfo)
    .map(wall => {
      return {
        areaName: areas[wall.areaId].name,
        wallName: wall.name,
        wallId: wall.wallId,
        internalInfo: wall.internalInfo,
        data: {
          wallId: wall.wallId,
          name: wall.name,
          areaId: wall.areaId,
          gradingSystem: wall.gradingSystem,
          active: true
        }
      }
    });

  return {
    walls: notifiedWalls
  };
}

const mapDispatchToProps = (dispatch) => {
  return {
    checkNotification: (data, locationId) => {
      return dispatch(editWall(data, locationId));
    }
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(WallNotifications));
