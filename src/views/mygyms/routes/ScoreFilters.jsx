import React, { Component } from 'react';
import { defineMessages, injectIntl } from 'react-intl';
import { SelectableFilter } from '../../../components/table/Filters';

/* Difficulty filter values */
export const F_SCORES_ALL = 'SCORES_ALL';
export const SCORE_0 = 'SCORE_0';
export const SCORE_1 = 'SCORE_1';
export const SCORE_2 = 'SCORE_2';
export const SCORE_3 = 'SCORE_3';

const messages = defineMessages({
  all: {
    id: 'view.mygyms.routes.filters.scores.all',
    defaultMessage: 'All scores'
  },
  average: {
    id: 'view.mygyms.routes.filters.scores.average',
    defaultMessage: 'Average routes ({amount, number})'
  },
  good: {
    id: 'view.mygyms.routes.filters.scores.good',
    defaultMessage: 'Good routes ({amount, number})'
  },
  great: {
    id: 'view.mygyms.routes.filters.scores.great',
    defaultMessage: 'Great routes ({amount, number})'
  },
  brilliant: {
    id: 'view.mygyms.routes.filters.scores.brilliant',
    defaultMessage: 'Brilliant routes ({amount, number})'
  },
});

class ScoreFiltersView extends Component {
  state = {
    scores: {
      SCORE_0: 0,
      SCORE_1: 0,
      SCORE_2: 0,
      SCORE_3: 0
    }
  }

  componentWillReceiveProps(nextProps) {
    const scores = { SCORE_0: 0, SCORE_1: 0, SCORE_2: 0, SCORE_3: 0 };
    for (let i = 0; i < nextProps.routes.length; i++) {
      const route = nextProps.routes[i];
      if (route.communityScore === 0) {
        scores[SCORE_0]++;
      } else if (route.communityScore === 1) {
        scores[SCORE_1]++;
      } else if (route.communityScore === 2) {
        scores[SCORE_2]++;
      } else if (route.communityScore >= 3) {
        scores[SCORE_3]++;
      }
    }

    this.setState({ scores: scores });
  }

  render() {
    const { formatMessage } = this.props.intl;
    let scoreFilters = [];
    const { scores } = this.state;
    const { values, checkFilter } = this.props;
    if (values.indexOf(F_SCORES_ALL) > -1) {
      scoreFilters.push(<SelectableFilter checked={true} title={ formatMessage(messages.all) }
                          key="allScores" onChange={checkFilter(F_SCORES_ALL)} />);
    } else {
      scoreFilters.push(<SelectableFilter checked={false} title={ formatMessage(messages.all) }
                          key="allScores" onChange={checkFilter(F_SCORES_ALL)} />);
      const score0checked = values.indexOf(0) > -1;
      scoreFilters.push(<SelectableFilter checked={score0checked} title={ formatMessage(messages.average, { amount: scores[SCORE_0] }) }
                          key="allScores0" onChange={checkFilter(0)} />);
      const score1checked = values.indexOf(1) > -1;
      scoreFilters.push(<SelectableFilter checked={score1checked} title={ formatMessage(messages.good, { amount: scores[SCORE_1] }) }
                          key="allScores1" onChange={checkFilter(1)} />);
      const score2checked = values.indexOf(2) > -1;
      scoreFilters.push(<SelectableFilter checked={score2checked} title={ formatMessage(messages.great, { amount: scores[SCORE_2] }) }
                          key="allScores2" onChange={checkFilter(2)} />);
      const score3checked = values.indexOf(3) > -1;
      scoreFilters.push(<SelectableFilter checked={score3checked} title={ formatMessage(messages.brilliant, { amount: scores[SCORE_3] }) }
                          key="allScores3" onChange={checkFilter(3)} />);
    }

    return (
      <div>
        { scoreFilters }
      </div>
    );
  }
}

export const ScoreFilters = injectIntl(ScoreFiltersView);
