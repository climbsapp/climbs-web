import React, { Component } from 'react';
import { withRouter } from 'react-router';
import { connect } from 'react-redux';
import { defineMessages, injectIntl } from 'react-intl';
import ReactHighcharts from 'react-highcharts';
import moment from 'moment';
import { fetchRouteSends } from '../../../actions/adminCharts';
import OverlayContainer from '../../../components/modal/OverlayContainer';
import ChartIcon from '../../../images/icons/chart-line-ffffff.svg';
import './RouteInfoOverlay.css';

const messages = defineMessages({
  title: {
    id: 'view.mygyms.routes.route.info.title',
    defaultMessage: 'Route lifespan'
  },
  infoTotalSends: {
    id: 'view.mygyms.routes.route.info.totalSends',
    defaultMessage: 'total sends'
  },
  infoDaysLive: {
    id: 'view.mygyms.routes.route.info.daysLive',
    defaultMessage: 'days live'
  },
  infoSinceLastSend: {
    id: 'view.mygyms.routes.route.info.daysSinceLastSend',
    defaultMessage: 'days since last send'
  },
  yAxisTitle: {
    id: 'view.mygyms.routes.route.info.chart.yAxisTitle',
    defaultMessage: 'Send count'
  },
  xAxisTitle: {
    id: 'view.mygyms.routes.route.info.chart.xAxisTitle',
    defaultMessage: 'Route lifespan'
  },
  sendsSeriesTitle: {
    id: 'view.mygyms.routes.route.info.chart.sendsSeries',
    defaultMessage: 'Sends'
  },
  loadingText: {
    id: 'view.mygyms.routes.route.info.chart.loading',
    defaultMessage: 'Looking into the great ledger...'
  }
});

class RouteInfoOverlay extends Component {

  componentDidMount() {
    this.props.fetchRouteSends(this.props.params.routeId);
  }

  countTotalSends() {
    const { rawData } = this.props;
    let totalSends = 0;
    for (let i = 0; i < rawData.length; i++) {
      totalSends += rawData[i].sendCount;
    }
    return totalSends;
  }

  countDaysLive() {
    const { route } = this.props;
    return route ? Math.abs(moment(route.routeCreated).diff(moment(), 'days')) : 0;
  }

  countDaysSinceLastSend() {
    const { route, rawData } = this.props;
    const today = moment();

    if (rawData.length > 0) {
      const lastSendDate = moment(rawData[rawData.length - 1].sendDateString);
      return Math.abs(today.diff(lastSendDate, 'days'));
    } else {
      return route ? Math.abs(moment(route.routeCreated).diff(today, 'days')) : 0;
    }
  }

  render() {
    const { formatMessage } = this.props.intl;
    const totalSends = this.countTotalSends();
    const daysLive = this.countDaysLive();
    const daysSinceLastSend = this.countDaysSinceLastSend();
    return (
      <OverlayContainer title={ formatMessage(messages.title) } iconImg={ ChartIcon }>
        <div className="InfoContainer row">
          <div className="col s6">
            <span className="Number">{ totalSends }</span>
            <span>{ formatMessage(messages.infoTotalSends) }</span>
          </div>
          <div className="col s6">
            <span className="Number">{ daysLive }</span>
            <span>{ formatMessage(messages.infoDaysLive) }</span>
          </div>
          <div className="col s6">
            <span className="Number">{ daysSinceLastSend }</span>
            <span>{ formatMessage(messages.infoSinceLastSend) }</span>
          </div>
        </div>
        { this.renderChart() }
      </OverlayContainer>
    );
  }

  renderChart() {
    if (this.props.isLoading) {
      const { formatMessage } = this.props.intl;
      return (
        <div className="NoData">
          <h3>{ formatMessage(messages.loadingText) }</h3>
        </div>
      );
    }

    const chartConfig = this.getChartConfig();
    return <ReactHighcharts config={ chartConfig } />;
  }

  getChartConfig() {
    const { formatMessage } = this.props.intl;
    const { route, rawData } = this.props;

    let data;
    let pointStart;
    if (!route) {
      data = [];
    } else {
      const routeCreated = moment(route.routeCreated);
      const routeCreateDate = moment(routeCreated.format('YYYY-MM-DD'));
      pointStart = routeCreateDate.valueOf();
      data = mapDataToChartConfig(rawData, routeCreateDate);
    }

    return {
      title: {
        text: ''
      },
      chart: {
        type: 'spline',
        height: '250',
        style: {
          fontFamily: '"Proxima Nova", "Roboto", "sans-serif"'
        }
      },
      credits: {
        enabled: false
      },
      lang: {
        noData: 'No sends logged in yet to show history.'
      },
      xAxis: {
        type: 'datetime',
        title: {
          text: formatMessage(messages.xAxisTitle)
        }
      },
      yAxis: {
        title: {
          text: formatMessage(messages.yAxisTitle)
        }
      },
      series: [{
        name: formatMessage(messages.sendsSeriesTitle),
        pointStart: pointStart,
        data: data
      }]
    };
  }
}

const mapStateToProps = (state, props) => {
  const routeId = props.params.routeId;
  return {
    route: state.routesAdmin.routes[routeId],
    rawData: state.adminCharts.routeSendCounts[routeId] || [],
    isLoading: state.adminCharts.isLoading
  };
}

const mapDispatchToProps = (dispatch) => {
  return {
    fetchRouteSends: (routeId) => dispatch(fetchRouteSends(routeId))
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(injectIntl(RouteInfoOverlay)));

function mapDataToChartConfig(data, pointStart) {
  if (!data || data.length === 0) {
    return {};
  }

  const points = [];
  let previousValueDate = moment(pointStart);
  for (let i = 0; i < data.length; i++) {
    const { sendDateString, sendCount } = data[i];
    const sendDate = moment(sendDateString);

    const dateDiff = Math.abs(previousValueDate.diff(sendDate, 'days'));
    if (i === 0 && dateDiff > 0) {
      points.push([previousValueDate.clone().valueOf(), 0]);
    }

    if (dateDiff > 1) {
      points.push([previousValueDate.clone().add(1, 'days').valueOf(), 0]);
      points.push([sendDate.clone().subtract(1, 'days').valueOf(), 0]);
    }

    points.push([sendDate.valueOf(), sendCount]);
    previousValueDate = sendDate;

    if (i === data.length - 1) {
      const today = moment();
      const todayDiff = Math.abs(today.diff(sendDate));
      if (todayDiff > 0) {
        points.push([sendDate.clone().add(1, 'days').valueOf(), 0]);
      }

      if (todayDiff > 1) {
        points.push([today.valueOf(), 0]);
      }
    }
  }

  return points;
}
