import React, { Component } from 'react';
import { defineMessages, injectIntl } from 'react-intl';
import { SelectableFilter } from '../../../components/table/Filters';

export const F_TYPE_ALL = 'TYPE_ALL';
export const F_TYPE_AUTOBELAY = 'TYPE_AUTOBELAY';
export const F_TYPE_BOULDER = 'TYPE_BOULDER';
export const F_TYPE_LEAD = 'TYPE_LEAD';
export const F_TYPE_TOPROPE = 'TYPE_TOPROPE';

const messages = defineMessages({
  typeAll: {
    id: 'view.mygyms.routes.filters.type.all',
    defaultMessage: 'All route types'
  },
  typeAutobelay: {
    id: 'view.mygyms.routes.filters.type.autobelay',
    defaultMessage: 'Autobelay ({amount})'
  },
  typeBoulder: {
    id: 'view.mygyms.routes.filters.type.boulder',
    defaultMessage: 'Boulder ({amount})'
  },
  typeLead: {
    id: 'view.mygyms.routes.filters.type.lead',
    defaultMessage: 'Lead ({amount})'
  },
  typeToprope: {
    id: 'view.mygyms.routes.filters.type.toprope',
    defaultMessage: 'Toprope ({amount})'
  }
});

export class RouteTypeFiltersView extends Component {
  state = {
    amounts: {
      TYPE_AUTOBELAY: 0,
      TYPE_BOULDER: 0,
      TYPE_LEAD: 0,
      TYPE_TOPROPE: 0
    }
  }

  componentWillReceiveProps(nextProps) {
    const amounts = { TYPE_AUTOBELAY: 0, TYPE_BOULDER: 0, TYPE_LEAD: 0, TYPE_TOPROPE: 0 };
    for (let i = 0; i < nextProps.routes.length; i++) {
      const route = nextProps.routes[i];
      switch (route.type) {
        case 3:
          amounts[F_TYPE_AUTOBELAY]++;
          break;
        case 4:
          amounts[F_TYPE_BOULDER]++;
          break;
        case 1:
          amounts[F_TYPE_LEAD]++;
          break;
        case 2:
          amounts[F_TYPE_TOPROPE]++;
          break;
        case 7: // Lead & toprope
          amounts[F_TYPE_TOPROPE]++;
          amounts[F_TYPE_LEAD]++;
          break;
        default:
          // Unknown. Skip.
      }
    }

    this.setState({ amounts: amounts });
  }

  render() {
    const { formatMessage } = this.props.intl;
    const { amounts } = this.state;
    let routeTypeFilters = [];
    const { values, checkFilter } = this.props;
    if (values.indexOf(F_TYPE_ALL) > -1) {
      routeTypeFilters.push(<SelectableFilter checked={true} title={ formatMessage(messages.typeAll) }
                        key="allRouteTypes" onChange={checkFilter(F_TYPE_ALL)} />);
    } else {
      routeTypeFilters.push(<SelectableFilter checked={false} title={ formatMessage(messages.typeAll) }
                        key="allRouteTypes" onChange={checkFilter(F_TYPE_ALL)} />);
      const fAutobelayChecked = values.indexOf(F_TYPE_AUTOBELAY) > -1;
      routeTypeFilters.push(<SelectableFilter checked={fAutobelayChecked} 
                        title={ formatMessage(messages.typeAutobelay, { amount: amounts[F_TYPE_AUTOBELAY] }) }
                        key="routeTypesAutobelay" onChange={checkFilter(F_TYPE_AUTOBELAY)} />);
      const fBoulderChecked = values.indexOf(F_TYPE_BOULDER) > -1;
      routeTypeFilters.push(<SelectableFilter checked={fBoulderChecked} 
                        title={ formatMessage(messages.typeBoulder, { amount: amounts[F_TYPE_BOULDER] }) }
                        key="routeTypesBoulder" onChange={checkFilter(F_TYPE_BOULDER)} />);
      const fLeadChecked = values.indexOf(F_TYPE_LEAD) > -1;
      routeTypeFilters.push(<SelectableFilter checked={fLeadChecked} 
                        title={ formatMessage(messages.typeLead, { amount: amounts[F_TYPE_LEAD] }) }
                        key="routeTypesLead" onChange={checkFilter(F_TYPE_LEAD)} />);
      const fTopropeChecked = values.indexOf(F_TYPE_TOPROPE) > -1;
      routeTypeFilters.push(<SelectableFilter checked={fTopropeChecked} 
                        title={ formatMessage(messages.typeToprope, { amount: amounts[F_TYPE_TOPROPE] }) }
                        key="routeTypesToprope" onChange={checkFilter(F_TYPE_TOPROPE)} />);
    }
    return (
      <div>{routeTypeFilters}</div>
    );
  }
}

export const RouteTypeFilters = injectIntl(RouteTypeFiltersView);
