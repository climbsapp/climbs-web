import React, { Component } from 'react';
import { connect } from 'react-redux';
import { defineMessages, injectIntl } from 'react-intl';
import ReactHighcharts from 'react-highcharts';
import { ViewHeader } from '../../../components/header/ViewHeader';
import WallFilter from '../../../components/table/WallFilter';

const messages = defineMessages({
  title: {
    id: 'view.mygyms.routestats.title',
    defaultMessage: 'Wall stats'
  },
  chartTitle: {
    id: 'view.mygyms.routestats.chart.title',
    defaultMessage: 'Amount of routes on walls'
  },
  loadingText: {
    id: 'view.mygyms.routestats.chart.loading',
    defaultMessage: 'Loading'
  },
  xAxisTitle: {
    id: 'view.mygyms.routestats.chart.xAxisTitle',
    defaultMessage: 'Grades'
  },
  yAxisTitle: {
    id: 'view.mygyms.routestats.chart.yAxisTitle',
    defaultMessage: 'Routes'
  },
  routesSeriesTitle: {
    id: 'view.mygyms.routestats.chart.seriesTitle',
    defaultMessage: 'Amount of routes'
  }
});

class RouteStats extends Component {
  state = {
    wallIds: {}
  }

  getWallFilterOptions() {
    const { walls } = this.props;
    return walls.map(wall => { return { label: wall.name, value: wall.wallId }});
  }

  wallFilterChanged = (wallIds) => {
    this.setState(prevState => Object.assign({}, prevState, { wallIds }));
  }

  render() {
    const { areas, walls } = this.props;
    const { formatMessage } = this.props.intl;
    return (
      <div className="RouteStats">
        <ViewHeader title={ formatMessage(messages.title) } />
        <div className="Content-Area">
          <div className="row">
            <div className="col s3">
              <WallFilter areas={ areas } walls={ walls } onChange={ this.wallFilterChanged } />
            </div>
            <div className="col s9">
              { this.renderChart() }
            </div>
          </div>
        </div>
      </div>
    );
  }

  renderChart() {
    if (this.props.isLoading) {
      const { formatMessage } = this.props.intl;
      return (
        <div className="NoData">
          <h3>{ formatMessage(messages.loadingText) }</h3>
        </div>
      );
    }

    const chartConfig = this.getChartConfig();
    return <ReactHighcharts config={ chartConfig } />;
  }

  getChartConfig() {
    const { formatMessage } = this.props.intl;
    const { routes, gradingSystems } = this.props;

    const data = mapDataToChartConfig(routes, this.state.wallIds);
    const gradingSystem = data.gradingSystems.length >= 1 ? gradingSystems[data.gradingSystems[0]] : null;
    return {
      title: {
        text: formatMessage(messages.chartTitle),
        align: 'left',
        x: 45
      },
      chart: {
        height: '250',
        style: {
          fontFamily: '"Proxima Nova", "Roboto", "sans-serif"'
        }
      },
      credits: {
        enabled: false
      },
      lang: {
        noData: 'No sends logged in yet to show history.'
      },
      xAxis: {
        categories: data ? data.categories : [],
        labels: {
          formatter: function() {
            if (!gradingSystem) {
              return this.value;
            }
            const routeGradeIndex = gradingSystem.internalGrades.indexOf(parseInt(this.value, 10));
            const routeGrade = gradingSystem.grades[routeGradeIndex];
            return routeGrade;
          }
        }
      },
      yAxis: {
        min: 0,
        softMax: 10,
        startOnTick: true,
        minTickInterval: 1,
        title: {
          text: formatMessage(messages.yAxisTitle)
        }
      },
      series: [{
        name: formatMessage(messages.routesSeriesTitle),
        type: 'column',
        color: '#69d2e7',
        data: data.routeAmounts
      }]
    };
  }
}

const mapStateToProps = (state, props) => {
  const { routesAdmin } = state;
  return {
    routes: Object.keys(routesAdmin.routes).map(routeId => routesAdmin.routes[routeId]),
    walls: Object.keys(routesAdmin.walls).map(wallId => routesAdmin.walls[wallId]),
    areas: Object.keys(routesAdmin.areas).map(areaId => routesAdmin.areas[areaId]),
    gradingSystems: routesAdmin.gradingSystems,
    isLoading: routesAdmin.isLoading
  };
}

export default connect(mapStateToProps)(injectIntl(RouteStats));

function mapDataToChartConfig(routes, includeWalls) {
  if (!routes) {
    return undefined;
  }

  const categories = {};
  const routeAmounts = {};
  const boulderAmounts = {};
  const gradingSystems = {};

  for (let i = 0; i < routes.length; i++) {
    const route = routes[i];

    if (gradingSystems[route.gradingSystem] === undefined) {
      gradingSystems[route.gradingSystem] = true;
    }

    if (categories[route.internalGrade] === undefined) {
      boulderAmounts[route.internalGrade] = 0;
      routeAmounts[route.internalGrade] = 0;
    }

    categories[route.internalGrade] = null;

    if (includeWalls[route.wallId] === true) {
      if (route.type === 5) {
        boulderAmounts[route.internalGrade]++;
      } else {
        routeAmounts[route.internalGrade]++;
      }
    }
  }

  return {
    categories: Object.keys(categories),
    routeAmounts: Object.keys(routeAmounts).map(key => routeAmounts[key]),
    boulderAmounts: Object.keys(boulderAmounts).map(key => boulderAmounts[key]),
    gradingSystems: Object.keys(gradingSystems)
  };
}
