import React, { Component } from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';
import { defineMessages, injectIntl } from 'react-intl';
import { SubNav, SubNavItem, SubNavDivider, SubNavSelector, SubNavHeader } from '../../components/navigation/SubNav';
import { fetchLocation, fetchLocations } from '../../actions/routesAdmin';
import dashboardImg from '../../images/icons/view-dashboard.svg';
import routesImg from '../../images/icons/table-9e9e9e.svg';
import manageImg from '../../images/icons/table-edit-9e9e9e.svg';
import addAreaImg from '../../images/icons/plus-outline-808080.svg';
import routeSettersImg from '../../images/icons/worker-9e9e9e.svg';

const messages = defineMessages({
  subNavItemNewDashboard: {
    id: 'view.mygyms.subnav.dashboard',
    defaultMessage: 'Dashboard'
  },
  subNavItemRoutes: {
    id: 'view.mygyms.subnav.routes',
    defaultMessage: 'Routes'
  },
  subNavItemRouteStats: {
    id: 'view.mygyms.subnav.routestats',
    defaultMessage: 'Route stats'
  },
  subNavItemManage: {
    id: 'view.mygyms.subnav.manage',
    defaultMessage: 'Manage'
  },
  subNavItemRouteSetters: {
    id: 'view.mygyms.subnav.routesetters',
    defaultMessage: 'Routesetters'
  },
  subNavItemAddArea: {
    id: 'view.mygyms.subnav.addarea',
    defaultMessage: 'Add new area'
  },
  subNavItemOtherGyms: {
    id: 'view.mygyms.subnav.othergyms',
    defaultMessage: 'Your other gyms'
  }
});

class MyGymsRoot extends Component {
  constructor(props) {
    super(props);

    this.state = {
      locationId: null
    };
  }

  componentWillReceiveProps(nextProps) {
    if (null !== nextProps.locationId && this.state.locationId !== nextProps.locationId) {
      this.setState({ locationId: nextProps.locationId });
      this.props.fetchLocation(nextProps.locationId);
    }
  }

  selectLocation = (locationId) => {
    return () => {
      this.props.router.push('/mygyms/' + locationId);
    };
  }

  renderNavigation() {
    const { formatMessage } = this.props.intl;
    let items = [];
    if (this.props.selectedLocation) {
      const location = this.props.selectedLocation;
      const { locationId } = this.props;
      items.push(<SubNavSelector text={location.name} logo={location.logo} key="locationName" />);
      items.push(<SubNavDivider key="locationNameDivider" />);
      items.push(
        <SubNavItem text={formatMessage(messages.subNavItemNewDashboard)} img={dashboardImg} key="Dashboard"
          path={'mygyms/' + locationId + '/dashboard'} location={this.props.location} />
      );
      items.push(
        <SubNavItem text={formatMessage(messages.subNavItemRoutes)} img={routesImg} key="Routes"
          path={'mygyms/' + locationId + '/routes'} location={this.props.location} />
      );
      items.push(
        <SubNavItem text={formatMessage(messages.subNavItemRouteStats)} img={routesImg} key="RouteStats"
          path={'mygyms/' + locationId + '/routestats'} location={this.props.location} />
      );
      items.push(
        <SubNavItem text={formatMessage(messages.subNavItemManage)} img={manageImg} key="Manage"
            path={'mygyms/' + locationId + '/manage'} location={this.props.location}>
          <SubNavItem text={formatMessage(messages.subNavItemAddArea)} img={addAreaImg}
            path={'mygyms/' + locationId + '/manage/addarea'} location={this.props.location} />
        </SubNavItem>
      );
      items.push(<SubNavItem text={formatMessage(messages.subNavItemRouteSetters)} img={routeSettersImg} key="Routesetters"
                    path={'mygyms/' + locationId + '/routesetters'} location={this.props.location} />);
    }

    if (this.props.locations.length > 1) {
      items.push(<SubNavHeader title={formatMessage(messages.subNavItemOtherGyms)} key="otherGymsHeader" />);
      for (let i = 0; i < this.props.locations.length; i++) {
        const location = this.props.locations[i];
        if (location.locationId !== this.props.selectedLocation.locationId) {
          items.push(<SubNavSelector text={location.name} onClick={this.selectLocation(location.locationId)}
                        key={'locationName-' + i} logo={location.logo} />);
        }
      }
    }

    return items;
  }

  render() {
    return (
      <div className="MyGyms">
        <div className="row">
          <div className="col l2 m3 s4">
            <SubNav>
              { this.renderNavigation() }
            </SubNav>
          </div>
          <div className="col l10 m9 s8">
            { this.props.children }
          </div>
        </div>
      </div>
    )
  }
}

const mapStateToProps = (state, props) => {
  const locations = Object.keys(state.routesAdmin.locations).map(id => state.routesAdmin.locations[id]);
  const location = state.routesAdmin.locations[props.params.locationId];

  return {
    locations: locations,
    selectedLocation: location,
    locationId: props.params.locationId
  };
}

const mapDispatchToProps = (dispatch) => {
  return {
    fetchLocation: (locationId) => {
      dispatch(fetchLocation(locationId));
    },
    fetchLocations: () => {
      dispatch(fetchLocations());
    }
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(injectIntl(MyGymsRoot)));
