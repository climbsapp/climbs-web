import React, { Component } from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';
import { defineMessages, injectIntl } from 'react-intl';
import { ViewHeader } from '../../../components/header/ViewHeader';
import { Button } from '../../../components';
import { addRouteSetter, addRouteSetterByName, deleteRouteSetter } from '../../../actions/adminRouteSetters';
import { notify } from '../../../actions/notifications';
import './RouteSetters.css';

const messages = defineMessages({
  title: {
    id: 'view.mygyms.manage.routesetters.title',
    defaultMessage: 'Manage route setters'
  },
  helpGeneral: {
    id: 'view.mygyms.manage.routesetters.help.general',
    defaultMessage: 'Route setters are users who can manage your climbing venue by keeping the areas, walls and routes in check. Ideally they remove routes when they disassemble them and add routes after they built them.'
  },
  helpInvite: {
    id: 'view.mygyms.manage.routesetters.help.invite',
    defaultMessage: 'You can invite route setters by giving their email address. This must be the same email address they used to register into Climbs. If they have not yet registered, we will send them an invite email they can use to register and start putting up routes.'
  },
  routeSetterRemoved: {
    id: 'notifications.routesetters.removed',
    defaultMessage: 'Removed routesetter: "{name}"'
  },
  addRegisteredRouteSetter: {
    id: 'notifications.routesetters.added.registered',
    defaultMessage: 'Added registered routesetter: "{email}"'
  },
  addAnonymousRouteSetter: {
    id: 'notifications.routesetters.added.anonymous',
    defaultMessage: 'Added not registered routesetter: "{name}"'
  },
  setterInfoText: {
    id: 'view.mygyms.manage.routesetters.infoText',
    defaultMessage: '{isRegistered, select, true {Registered at {registeredDate, date, short}.} other {Not registered}}'
  },
  inviteSetter: {
    id: 'view.mygyms.manage.routesetters.add.registered',
    defaultMessage: 'Invite setter'
  },
  addSetter: {
    id: 'view.mygyms.manage.routesetters.add.unregistered',
    defaultMessage: 'Add setter'
  },
  labelAddSetter: {
    id: 'view.mygyms.manage.routesetters.title.add',
    defaultMessage: 'Route setter (use email for Climbs user, name for anonymous)'
  },
  listTitle: {
    id: 'view.mygyms.manage.routesetters.list.title',
    defaultMessage: 'Current route setters'
  }
});

class RouteSettersView extends Component {
  state = {
    value: ''
  }

  renderHelp() {
    const { formatMessage } = this.props.intl;
    return (
      <div>
        <p>{ formatMessage(messages.helpGeneral) }</p>
        <p>{ formatMessage(messages.helpInvite) }</p>
      </div>
    );
  }

  setterPictureUrlSmall(setter) {
    return setter.pictures.length > 0 ? setter.pictures[0].urlSmall : undefined;
  }

  deleteRouteSetter = (routeSetterId, name) => {
    const { formatMessage } = this.props.intl;
    return () => {
      this.props.deleteRouteSetter(this.props.params.locationId, routeSetterId)
        .then(() => this.props.notify(formatMessage(messages.routeSetterRemoved, { name: name })));
    };
  }

  addRouteSetter = () => {
    const { formatMessage } = this.props.intl;
    const { value } = this.state;
    const { notify } = this.props;
    if (!value) {
      return;
    }

    let routeSetterData = {
      active: true,
      locationId: parseInt(this.props.params.locationId, 10),
    };

    if (value.indexOf('@') > 0) {
      routeSetterData.email = value;
      this.props.addRouteSetter(routeSetterData)
        .then(() => this.setState({ value: '' }))
        .then(() => notify(formatMessage(messages.addRegisteredRouteSetter, { email: routeSetterData.email })))
        .catch(error => {
          this.setState({ value: '' });
        });
    } else {
      routeSetterData.name = value;
      this.props.addRouteSetterByName(routeSetterData)
        .then(() => this.setState({ value: '' }))
        .then(() => notify(formatMessage(messages.addAnonymousRouteSetter, { name: routeSetterData.name })));
    }
  }

  setValue = (event) => {
    this.setState({ value: event.target.value });
  }

  renderRouteSetters() {
    const { formatMessage } = this.props.intl;
    const { routeSetters } = this.props;
    let elements = [];
    for (let i = 0; i < routeSetters.length; i++) {
      const routeSetter = routeSetters[i];
      const routeSetterPictureUrl = this.setterPictureUrlSmall(routeSetter);
      const routeSetterPictureClassName = 'RouteSetterPicture' + (routeSetterPictureUrl ? '' : ' Empty');
      const routeSetterStyle = routeSetterPictureUrl ? { backgroundImage: 'url(' + routeSetterPictureUrl + ')' } : {};
      const registeredDate = new Date(routeSetter.createDate);
      const email = routeSetter.isRealUser ? routeSetter.email : null;
      const emailElement = email ? (<span>(<i>{email}</i>)</span>) : null;
      const regString = formatMessage(messages.setterInfoText, { isRegistered: routeSetter.isRealUser, registeredDate: registeredDate });
      elements.push(
        <li className="RouteSetter" key={routeSetter.id}>
          <div className={routeSetterPictureClassName} style={routeSetterStyle} />
          <div className="RouteSetterInfo">
            <div className="Name">{routeSetter.name} {emailElement}</div>
            <div className="Registered">{regString}</div>
          </div>
          <div className="RouteSetterActions">
            <div className="Action Delete" onClick={this.deleteRouteSetter(routeSetter.id, routeSetter.name)}/>
          </div>
        </li>
      );
    }
    return elements;
  }

  render() {
    const { formatMessage } = this.props.intl;
    const { value } = this.state;
    const buttonLabel = value && value.indexOf('@') > 0 ? formatMessage(messages.inviteSetter) : formatMessage(messages.addSetter);
    return (
      <div className="RouteSettersView">
        <ViewHeader title={formatMessage(messages.title)} />
        <div className="Content-Area">
          <div className="Row">
            <div className="Col S8">
              <div className="row">
                <div className="input-field col s9">
                  <input id="value" type="text" value={this.state.value} onChange={this.setValue} />
                  <label className="active" htmlFor="value">{ formatMessage(messages.labelAddSetter) }</label>
                </div>
                <div className="input-field col s3">
                  <Button label={buttonLabel} onClick={this.addRouteSetter} loading={this.props.isLoading} />
                </div>
              </div>

              <label className="RouteSettersListLabel">{ formatMessage(messages.listTitle)} </label>
              <ul className="RouteSettersList">
                {this.renderRouteSetters()}
              </ul>
            </div>
            <div className="Col S4 Help">
              {this.renderHelp()}
            </div>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state, props) => {
  const { routeSetters, isLoading } = state.routesAdmin;
  return {
    routeSetters: Object.keys(routeSetters).map(id => routeSetters[id]),
    isLoading: isLoading
  };
}

const mapDispatchToProps = (dispatch) => {
  return {
    addRouteSetter: (data) => dispatch(addRouteSetter(data)),
    addRouteSetterByName: (data) => dispatch(addRouteSetterByName(data)),
    deleteRouteSetter: (locationId, routeSetterId) =>
      dispatch(deleteRouteSetter(locationId, routeSetterId)),
    notify: (text) => dispatch(notify(text))
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(injectIntl(RouteSettersView)));
