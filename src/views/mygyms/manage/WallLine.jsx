import React, { Component } from 'react';
import { PropTypes } from 'prop-types';
import { defineMessages, injectIntl } from 'react-intl';
import { DropDown, DropDownItem } from '../../../components/dropdown/DropDown';
import ImageAction from '../../../components/action/ImageAction';
import cameraPicture from '../../../images/icons/camera.svg';

const messages = defineMessages({
  setPhotoTitle: {
    id: 'view.mygyms.manage.wall.title.setphoto',
    defaultMessage: 'Set wall photo'
  },
  editTitle: {
    id: 'view.mygyms.manage.wall.title.edit',
    defaultMessage: 'Edit wall'
  },
  removeTitle: {
    id: 'view.mygyms.manage.wall.title.remove',
    defaultMessage: 'Remove wall'
  },
  removeDisabledTooltip: {
    id: 'view.mygyms.manage.wall.tooltip.remove',
    defaultMessage: 'Remove all routes first.'
  },
  printTopoAction: {
    id: 'view.mygyms.manage.wall.title.print',
    defaultMessage: 'Print wall topo'
  },
  moveWallUpAction: {
    id: 'view.mygyms.manage.wall.title.moveUp',
    defaultMessage: 'Move up'
  },
  moveWallDownAction: {
    id: 'view.mygyms.manage.wall.title.moveDown',
    defaultMessage: 'Move down'
  },
  wallTypeLeadDefault: {
    id: 'view.mygyms.manage.wall.type.lead',
    defaultMessage: 'Lead wall'
  },
  wallTypeAutobelayDefault: {
    id: 'view.mygyms.manage.wall.type.autobelay',
    defaultMessage: 'Autobelay wall'
  },
  wallTypeBoulderDefault: {
    id: 'view.mygyms.manage.wall.type.boulder',
    defaultMessage: 'Boulder wall'
  },
  wallTypeTopropeDefault: {
    id: 'view.mygyms.manage.wall.type.toprope',
    defaultMessage: 'Toprope wall'
  },
  wallTypeLeadAndTopropeDefault: {
    id: 'view.mygyms.manage.wall.type.leadandtoprope',
    defaultMessage: 'Lead & toprope wall'
  }
});

class WallLine extends Component {
  getWallType() {
    const { formatMessage } = this.props.intl;
    const { typeName } = this.props;
    switch (typeName.toLowerCase()) {
      case 'lead':
        return formatMessage(messages.wallTypeLeadDefault)
      case 'boulder':
        return formatMessage(messages.wallTypeBoulderDefault)
      case 'toprope':
        return formatMessage(messages.wallTypeTopropeDefault)
      case 'lead & top rope':
        return formatMessage(messages.wallTypeLeadAndTopropeDefault)
      case 'autobelay':
        return formatMessage(messages.wallTypeAutobelayDefault)
      default:
        return typeName + ' wall';
    }
  }

  render() {
    const { formatMessage } = this.props.intl;
    let wallPicture, wallActions, onClick;
    if (this.props.selected) {
      const { pictureUrlSmall, onPictureClick, onEditClick, onDeleteClick, onPrintClick, deleteEnabled, onMoveUpClick, onMoveDownClick } = this.props;
      const wallPictureStyle = {
        backgroundImage: (pictureUrlSmall ? 'url(' + pictureUrlSmall + ')' : '')
      };
      const wallPictureClassName = 'WallPicture' + (pictureUrlSmall ? '' : ' NoPic');
      wallPicture = (
        <div className={wallPictureClassName} onClick={onPictureClick} style={wallPictureStyle} title={ formatMessage(messages.setPhotoTitle) }>
          <img src={cameraPicture} alt="" />
        </div>
      );

      wallActions = (
        <div className="WallActions">
          <ImageAction type="Edit" onClick={onEditClick} title={ formatMessage(messages.editTitle) } />
          <ImageAction type="Delete" onClick={onDeleteClick} title={ formatMessage(messages.removeTitle) }
            disabledTooltip={formatMessage(messages.removeDisabledTooltip)} disabled={!deleteEnabled} />
          <ImageAction type="Print" onClick={onPrintClick} title={ formatMessage(messages.printTopoAction) } />
          <DropDown>
            <DropDownItem type="MoveUp" title={formatMessage(messages.moveWallUpAction)} onClick={onMoveUpClick} />
            <DropDownItem type="MoveDown" title={formatMessage(messages.moveWallDownAction)} onClick={onMoveDownClick} />
          </DropDown>
        </div>
      );
    } else {
      wallActions = (
        <div className="WallActions">
          <div className="Action Goto" />
        </div>
      );
      onClick = this.props.onSelectWall;
    }

    let className = this.props.selected ? 'selected WallLine' : 'WallLine';
    if (this.props.selectedBefore) {
      className += ' selectedBefore';
    }

    let internalInfo = null;
    if (this.props.internalInfo) {
      internalInfo = <div className="InternalInfo" title={this.props.internalInfo} />;
    }

    const wallType = this.getWallType();

    return (
      <li className={className} onClick={onClick}>
        { wallPicture }
        <div className="WallInfo">
          <div className="WallName">
            {this.props.name}
            {internalInfo}
          </div>
          <div className="WallType">{ wallType }</div>
        </div>
        { wallActions }
      </li>
    );
  }

  static propTypes = {
    name: PropTypes.string.isRequired,
    internalInfo: PropTypes.string,
    typeName: PropTypes.string,
    pictureUrlSmall: PropTypes.string,
    selected: PropTypes.bool.isRequired,
    selectedBefore: PropTypes.bool.isRequired,
    onClick: PropTypes.func,
    onEditClick: PropTypes.func.isRequired,
    onDeleteClick: PropTypes.func.isRequired,
    onPrintClick: PropTypes.func.isRequired,
    onMoveUpClick: PropTypes.func.isRequired,
    onMoveDownClick: PropTypes.func.isRequired,
    deleteEnabled: PropTypes.bool
  }
}

export default injectIntl(WallLine);
