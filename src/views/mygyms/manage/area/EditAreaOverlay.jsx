import React, { Component } from 'react';
import { withRouter } from 'react-router';
import { connect } from 'react-redux';
import { defineMessages, injectIntl } from 'react-intl';
import { AreaOverlayForm } from '../../../../components';
import { editArea } from '../../../../actions/routesAdmin';
import { notify } from '../../../../actions/notifications';
import editAreaImg from '../../../../images/icons/pencil-box-outline-white.svg';

const messages = defineMessages({
  title: {
    id: 'view.mygyms.manage.area.edit.title',
    defaultMessage: 'Edit area'
  },
  subtitle: {
    id: 'view.mygyms.manage.area.edit.subtitle',
    defaultMessage: '(of location {locationName})'
  },
  submit: {
    id: 'view.mygyms.manage.area.edit.submit',
    defaultMessage: 'Save changes'
  },
  success: {
    id: 'notifications.mygyms.area.edit.success',
    defaultMessage: 'Area modifications saved.'
  }
});

class EditAreaOverlay extends Component {
  close = () => {
    this.props.router.goBack();
  }

  submit = (areaData) => {
    const { formatMessage } = this.props.intl;
    const { locationId, area } = this.props;
    const data = Object.assign({}, areaData, { active: true, locationId: locationId, areaId: area.areaId });
    this.props.editArea(data, locationId)
      .then(this.close)
      .then(() => this.props.notify(formatMessage(messages.success)));
  }

  render() {
    const { formatMessage } = this.props.intl;
    const title = formatMessage(messages.title);
    const subtitle = formatMessage(messages.subtitle, { locationName: this.props.locationName });
    const submitTitle = formatMessage(messages.submit);
    return (
      <AreaOverlayForm onSubmit={this.submit} onClose={this.close}
        title={title} subtitle={subtitle} iconImg={editAreaImg} submitTitle={submitTitle}
        data={this.props.area} loading={this.props.loading} />
    );
  }
}

const mapStateToProps = (state, props) => {
  const location = state.routesAdmin.locations[props.params.locationId];
  const area = state.routesAdmin.areas[props.params.areaId];
  if (location) {
    return {
      loading: state.routesAdmin.isLoading,
      locationName: location.name,
      locationId: location.locationId,
      area: area
    };
  } else {
    return {
      loading: state.routesAdmin.isLoading,
      locationName: '',
      locationId: props.params.locationId,
      area: area
    };
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    editArea: (areaData, locationId) => dispatch(editArea(areaData, locationId)),
    notify: message => dispatch(notify(message))
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(injectIntl(EditAreaOverlay), { withRef: true }));
