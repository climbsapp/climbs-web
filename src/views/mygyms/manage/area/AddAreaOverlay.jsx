import React, { Component } from 'react';
import { withRouter } from 'react-router';
import { connect } from 'react-redux';
import { defineMessages, injectIntl } from 'react-intl';
import { AreaOverlayForm } from '../../../../components';
import { addNewArea } from '../../../../actions/routesAdmin';
import { notify } from '../../../../actions/notifications';
import addAreaImg from '../../../../images/icons/plus-circle-white.svg';

const messages = defineMessages({
  title: {
    id: 'view.mygyms.manage.area.add.title',
    defaultMessage: 'Add area'
  },
  subtitle: {
    id: 'view.mygyms.manage.area.add.subtitle',
    defaultMessage: '(to location {locationName})'
  },
  submit: {
    id: 'view.mygyms.manage.area.add.submit',
    defaultMessage: 'Add area'
  },
  success: {
    id: 'notifications.mygyms.area.add.success',
    defaultMessage: 'Yea! Area "{areaName}" added successfully.'
  }
});

class AddAreaOverlay extends Component {
  close = () => {
    this.props.router.goBack();
  }

  submit = (areaData) => {
    const { formatMessage } = this.props.intl;
    const { locationId } = this.props;
    const data = Object.assign({}, areaData, { active: true, locationId: locationId });
    this.props.addNewArea(data, locationId)
      .then(this.close)
      .then(() => this.props.notify(formatMessage(messages.success, { areaName: data.name })));
  }

  render() {
    const { formatMessage } = this.props.intl;
    const title = formatMessage(messages.title);
    const subtitle = formatMessage(messages.subtitle, { locationName: this.props.locationName });
    const submitTitle = formatMessage(messages.submit);
    return (
      <AreaOverlayForm onSubmit={this.submit} onClose={this.close}
        title={title} subtitle={subtitle} submitTitle={submitTitle} iconImg={addAreaImg} />
    );
  }
}

const mapStateToProps = (state, props) => {
  const location = state.routesAdmin.locations[props.params.locationId];
  if (location) {
    return {
      locationName: location.name,
      locationId: location.locationId
    };
  } else {
    return {
      locationName: '',
      locationId: props.params.locationId
    };
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    addNewArea: (areaData, locationId) => dispatch(addNewArea(areaData, locationId)),
    notify: message => dispatch(notify(message))
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(injectIntl(AddAreaOverlay), { withRef: true}));
