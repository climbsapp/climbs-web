import React, { Component } from 'react';
import { withRouter } from 'react-router';
import { connect } from 'react-redux';
import { defineMessages, injectIntl } from 'react-intl';
import WallOverlayForm from '../../../../components/forms/WallOverlayForm';
import { editWall } from '../../../../actions/routesAdmin';
import { notify } from '../../../../actions/notifications';
import editWallImg from '../../../../images/icons/pencil-box-outline-white.svg';

const messages = defineMessages({
  title: {
    id: 'view.mygyms.manage.wall.edit.title',
    defaultMessage: 'Edit wall'
  },
  subtitle: {
    id: 'view.mygyms.manage.wall.edit.subtitle',
    defaultMessage: '(of area {areaName})'
  },
  submit: {
    id: 'view.mygyms.manage.wall.edit.submit',
    defaultMessage: 'Save changes'
  },
  success: {
    id: 'notifications.mygyms.wall.edit.success',
    defaultMessage: 'Wall modifications saved.'
  }
});

class EditWallOverlay extends Component {
  close = () => {
    this.props.router.goBack();
  }

  submit = (wallData) => {
    const { formatMessage } = this.props.intl;
    const { locationId } = this.props.params;
    const data = Object.assign({}, wallData, { active: true, wallId: this.props.wall.wallId, areaId: this.props.areaId });
    this.props.editWall(data, locationId)
      .then(this.close)
      .then(() => this.props.notify(formatMessage(messages.success)));
  }

  render() {
    const { formatMessage } = this.props.intl;
    const title = formatMessage(messages.title);
    const subtitle = formatMessage(messages.subtitle, { areaName: this.props.areaName });
    const submitTitle = formatMessage(messages.submit);
    return (
      <WallOverlayForm title={title} subtitle={subtitle} data={this.props.wall}
        areaId={this.props.areaId}
        locationGradingSystem={this.props.locationGradingSystem}
        locationGradingSystemBoulder={this.props.locationGradingSystemBoulder}
        submitTitle={submitTitle} iconImg={editWallImg} isLoading={this.props.isLoading}
        onClose={this.close} onSubmit={this.submit} />
    );
  }
}

const mapStateToProps = (state, props) => {
  const { walls, areas, locations, isLoading } = state.routesAdmin;
  const wallId = parseInt(props.params.wallId, 10);
  const wall = walls[wallId];
  const area = wall ? areas[wall.areaId] : {};
  const location = area ? locations[area.locationId] : {};

  const locationGradingSystem = location ? location.gradingSystem : '';
  const locationGradingSystemBoulder = location ? location.gradingSystemBoulder : '';
  const areaName = area ? area.name : '';
  return {
    isLoading: isLoading,
    locationGradingSystem: locationGradingSystem,
    locationGradingSystemBoulder: locationGradingSystemBoulder,
    areaName: areaName,
    areaId: wall ? wall.areaId : -1,
    wall: wall
  };
}

const mapDispatchToProps = (dispatch) => {
  return {
    editWall: (wallData, locationId) => dispatch(editWall(wallData, locationId)),
    notify: message => dispatch(notify(message))
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(injectIntl(EditWallOverlay), { withRef: true}));
