import React, { Component } from 'react';
import { withRouter } from 'react-router';
import { connect } from 'react-redux';
import { defineMessages, injectIntl } from 'react-intl';
import OverlayContainer from '../../../../components/modal/OverlayContainer';
import { loadWallTopoPdf } from '../../../../actions/routesAdmin';
import pdfImg from '../../../../images/icons/file-pdf-white.svg';
import './TopoPdfOverlay.css';

const messages = defineMessages({
  title: {
    id: 'view.mygyms.manage.wall.topo.title',
    defaultMessage: 'PDF topo'
  },
  subtitle: {
    id: 'view.mygyms.manage.wall.topo.subtitle',
    defaultMessage: '(of wall {wallName})'
  }
});

class TopoPdfOverlay extends Component {
  state = {
    data: null
  }

  componentDidMount() {
    this.props.loadWallTopoPdf(this.props.router.params.wallId)
      .then((data) => {
        const fileReader = new FileReader();
        fileReader.onload = (e) => {
          if (this.refs.container) {
            this.setState({ data: e.target.result });
          }
        };
        fileReader.readAsDataURL(data);
      });
  }

  renderPDF() {
    if (!this.state.data) {
      return undefined; // TODO: Loading indicator
    } else {
      return (
        <object type="application/pdf" data={ this.state.data }>
          <embed type="application/pdf" src={ this.state.date } />
        </object>
      );
    }
  }

  render() {
    const { formatMessage } = this.props.intl;
    const title = formatMessage(messages.title);
    const subtitle = formatMessage(messages.subtitle, { wallName: this.props.wall.name });
    return (
      <OverlayContainer title={title} subtitle={subtitle} ref="container" iconImg={ pdfImg }>
        { this.renderPDF() }
      </OverlayContainer>
    );
  }
}

const mapStateToProps = (state, props) => {
  return {
    wall: state.routesAdmin.walls[props.params.wallId] || { name: '' }
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    loadWallTopoPdf: wallId => dispatch(loadWallTopoPdf(wallId))
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(injectIntl(TopoPdfOverlay)));
