import React, { Component } from 'react';
import { withRouter } from 'react-router';
import { connect } from 'react-redux';
import { defineMessages, injectIntl } from 'react-intl';
import PictureOverlayForm from '../../../../components/forms/PictureOverlayForm';
import { Button } from '../../../../components';
import { uploadWallPicture } from '../../../../actions/routesAdmin';
import { notify } from '../../../../actions/notifications';
import cameraImg from '../../../../images/icons/camera-white.svg';
import './WallPictureOverlay.css';

const messages = defineMessages({
  title: {
    id: 'view.mygyms.manage.wall.picture.title',
    defaultMessage: 'Wall picture'
  },
  subtitle: {
    id: 'view.mygyms.manage.wall.picture.subtitle',
    defaultMessage: 'of {wallName} wall'
  },
  addNewTitle: {
    id: 'view.mygyms.manage.wall.picture.addNew',
    defaultMessage: 'Upload new picture'
  },
  addFirstTitle: {
    id: 'view.mygyms.manage.wall.picture.addFirst',
    defaultMessage: 'Upload wall picture'
  },
  helpText: {
    id: 'view.mygyms.manage.wall.picture.help',
    defaultMessage: 'Wall picture is something that helps climbers recognize the walls when using the Climbs application to track their progress.'
  },
  success: {
    id: 'notifications.mygyms.wall.picture.add.success',
    defaultMessage: 'Wall picture added successfully.'
  },
  failure: {
    id: 'notifications.mygyms.wall.picture.add.failure',
    defaultMessage: 'Ugh! Failed to add the picture for unknown reason.'
  }
});

class WallPictureOverlay extends Component {
  renderHelp() {
    const { formatMessage } = this.props.intl;
    return (
      <div>
        <p>{ formatMessage(messages.helpText) }</p>
      </div>
    );
  }

  uploadFileClick = () => {
    this.refs.picture.click();
  }

  uploadFileOnChange = (event) => {
    const { formatMessage } = this.props.intl;
    const { locationId } = this.props.params;
    let formData = new FormData();
    formData.append('id', this.props.wall.wallId);
    formData.append('context', 'WALL');
    formData.append('file', this.refs.picture.files[0]);
    this.props.uploadWallPicture(this.props.wall.wallId, formData, locationId)
      .then(() => this.props.router.goBack())
      .then(() => this.props.notify(formatMessage(messages.success)))
      .catch(error => this.props.notify(formatMessage(messages.failure)));
  }

  renderImgElement() {
    const { wall } = this.props;
    const url = (wall && wall.picture) ? wall.picture.url/*.replace('.test.', '.prod.')*/ : null;
    if (url) {
      return (<img className="OverlayPicture" src={url} alt="Wall" />);
    } else {
      return (<div className="NoPicture" />);
    }
  }

  render() {
    const { formatMessage } = this.props.intl;
    const { wall } = this.props;
    const title = formatMessage(messages.title);
    const subtitle = formatMessage(messages.subtitle, { wallName: (wall ? wall.name : '') });
    const helpElement = this.renderHelp();
    const buttonTitle = (wall && wall.picture) ? formatMessage(messages.addNewTitle) : formatMessage(messages.addFirstTitle);
    const style = { 'opacity': '0' };
    return (
      <PictureOverlayForm title={title} subtitle={subtitle} iconImg={cameraImg}
        helpElement={helpElement}>
        <div className="row">
          <div className="col s12">
            {this.renderImgElement()}
          </div>
        </div>
        <div className="Overlay-Footer">
          <Button onClick={this.uploadFileClick} loading={this.props.isLoading}
            label={buttonTitle} />
          <input name="picInput" type="file" ref="picture" onChange={this.uploadFileOnChange} style={style} />
        </div>
      </PictureOverlayForm>
    );
  }
}

const mapStateToProps = (state, props) => {
  return {
    isLoading: state.routesAdmin.isLoading,
    wall: state.routesAdmin.walls[props.params.wallId]
  };
}

const mapDispatchToProps = (dispatch) => {
  return {
    uploadWallPicture: (wallId, formData, locationId) => dispatch(uploadWallPicture(wallId, formData, locationId)),
    notify: message => dispatch(notify(message))
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(injectIntl(WallPictureOverlay)));
