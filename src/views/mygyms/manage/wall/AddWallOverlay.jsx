import React, { Component } from 'react';
import { withRouter } from 'react-router';
import { connect } from 'react-redux';
import { defineMessages, injectIntl } from 'react-intl';
import WallOverlayForm from '../../../../components/forms/WallOverlayForm';
import { addNewWall } from '../../../../actions/routesAdmin';
import { notify } from '../../../../actions/notifications';
import addWallImg from '../../../../images/icons/plus-box-white.svg';

const messages = defineMessages({
  title: {
    id: 'view.mygyms.manage.wall.add.title',
    defaultMessage: 'Add wall'
  },
  subtitle: {
    id: 'view.mygyms.manage.wall.add.subtitle',
    defaultMessage: '(to area {areaName})'
  },
  submit: {
    id: 'view.mygyms.manage.wall.add.submit',
    defaultMessage: 'Add wall'
  },
  success: {
    id: 'notifications.mygyms.wall.add.success',
    defaultMessage: 'Yea! Wall "{wallName}" added successfully.'
  }
});

class AddWallOverlay extends Component {
  close = () => {
    this.props.router.goBack();
  }

  submit = (wallData) => {
    const { formatMessage } = this.props.intl;
    const { locationId } = this.props.params;
    this.props.addNewWall(wallData, locationId)
      .then(this.close)
      .then(() => this.props.notify(formatMessage(messages.success, { wallName: wallData.name })));
  }

  render() {
    const { formatMessage } = this.props.intl;
    const { locationGradingSystem, locationGradingSystemBoulder, areaName, areaId, isLoading } = this.props;
    const title = formatMessage(messages.title);
    const subtitle = formatMessage(messages.subtitle, { areaName: areaName });
    const submitTitle = formatMessage(messages.submit);
    return (
      <WallOverlayForm title={title} subtitle={subtitle}
        areaId={areaId} locationGradingSystem={locationGradingSystem}
        locationGradingSystemBoulder={locationGradingSystemBoulder}
        submitTitle={submitTitle} iconImg={addWallImg} isLoading={isLoading}
        onClose={this.close} onSubmit={this.submit} />
    );
  }
}

const mapStateToProps = (state, props) => {
  const { areas, locations, isAddingNewWall } = state.routesAdmin;
  const locationId = props.params.locationId;
  const location = locations[locationId] ? locations[locationId] : null;
  const gradingSystem = location ? location.gradingSystem : '';
  const gradingSystemBoulder = location ? location.gradingSystemBoulder : '';

  let areaId = parseInt(props.params.areaId, 10);
  let area = areas[areaId];
  let areaName = area ? area.name : '';
  return {
    isLoading: isAddingNewWall,
    locationGradingSystem: gradingSystem,
    locationGradingSystemBoulder: gradingSystemBoulder,
    areaName: areaName,
    areaId: areaId
  };
}

const mapDispatchToProps = (dispatch) => {
  return {
    addNewWall: (wallData, locationId) => dispatch(addNewWall(wallData, locationId)),
    notify: message => dispatch(notify(message))
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(injectIntl(AddWallOverlay), { withRef: true}));
