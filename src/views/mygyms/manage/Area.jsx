import React, { Component } from 'react';
import { PropTypes } from 'prop-types';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';
import { defineMessages, injectIntl } from 'react-intl';
import OverlayDialog from '../../../components/modal/OverlayDialog';
import { ViewHeader, ViewHeaderAction } from '../../../components/header/ViewHeader';
import WallRoutes from './WallRoutes';
import { deleteArea, moveAreaDown, moveAreaUp } from '../../../actions/routesAdmin';
import { notify } from '../../../actions/notifications';

const messages = defineMessages({
  deleteConfirmationTitle: {
    id: 'view.mygyms.manage.area.deleteconfirmation.title',
    defaultMessage: 'You serious?'
  },
  deleteConfirmationMessage: {
    id: 'view.mygyms.manage.area.deleteconfirmation.message',
    defaultMessage: 'You are about to delete the area {areaName}. Please confirm as there is no coming back.'
  },
  editTitle: {
    id: 'view.mygyms.manage.area.edit.title.tooltip',
    defaultMessage: 'Edit area'
  },
  moveUpTitle: {
    id: 'view.mygyms.manage.area.edit.title.moveUp',
    defaultMessage: 'Move area up'
  },
  moveDownTitle: {
    id: 'view.mygyms.manage.area.edit.title.moveDown',
    defaultMessage: 'Move area down'
  },
  removeTitle: {
    id: 'view.mygyms.manage.area.remove.title.tooltip',
    defaultMessage: 'Remove area'
  },
  removeTooltip: {
    id: 'view.mygyms.manage.area.remove.disabled.tooltip',
    defaultMessage: 'Remove all walls first'
  },
  removeSuccess: {
    id: 'notifications.mygyms.area.remove.success',
    defaultMessage: 'Area removed successfully.'
  }
});

class Area extends Component {
  state = {
    showDeleteConfirmation: false
  }

  deleteArea = () => {
    const { formatMessage } = this.props.intl;
    this.setState({ showDeleteConfirmation: false });
    this.props.deleteArea(this.props.params.locationId, this.props.area.areaId)
      .then(() => this.props.notify(formatMessage(messages.removeSuccess)))
  }

  showEdit = () => {
    const locationId = this.props.params.locationId;
    const areaId = this.props.area.areaId;
    this.props.router.push('/mygyms/' + locationId + '/manage/editarea/' + areaId);
  }

  moveAreaUp = () => {
    const locationId = this.props.params.locationId;
    const areaId = this.props.area.areaId;
    this.props.moveAreaUp(areaId, locationId);
  }

  moveAreaDown = () => {
    const locationId = this.props.params.locationId;
    const areaId = this.props.area.areaId;
    this.props.moveAreaDown(areaId, locationId);
  }

  showDeleteConfirmation = () => {
    this.setState({ showDeleteConfirmation: true });
  }

  hideDeleteConfirmation = () => {
    this.setState({ showDeleteConfirmation: false });
  }

  renderDeleteConfirmationDialog() {
    if (this.state.showDeleteConfirmation) {
      const { formatMessage } = this.props.intl;
      const title = formatMessage(messages.deleteConfirmationTitle);
      const content = formatMessage(messages.deleteConfirmationMessage, { areaName: this.props.area.name });
      return (
        <OverlayDialog title={title} content={content}
          onAccept={this.deleteArea} onCancel={this.hideDeleteConfirmation} />
      );
    } else {
      return undefined;
    }
  }

  render() {
    const { formatMessage } = this.props.intl;
    let area = this.props.area;
    let walls = area.walls ? area.walls : [];

    const deleteDialog = this.renderDeleteConfirmationDialog();
    return (
      <div className="Area">
        <ViewHeader title={ area.name }>
          <ViewHeaderAction onClick={this.showEdit} type="Edit" title={formatMessage(messages.editTitle)} />
          <ViewHeaderAction onClick={this.showDeleteConfirmation} type="Delete"
            title={formatMessage(messages.removeTitle)} disabled={area.walls.length > 0} 
            disabledTooltip={formatMessage(messages.removeTooltip)} />
          <ViewHeaderAction onClick={this.moveAreaUp} type="MoveUp" title={formatMessage(messages.moveUpTitle)} />
          <ViewHeaderAction onClick={this.moveAreaDown} type="MoveDown" title={formatMessage(messages.moveDownTitle)} />
        </ViewHeader>
        <div className="Content-Area">
          <div className="row">
            <div className="col s12">
              <WallRoutes areaId={area.areaId} walls={walls} selectedWallIndex={area.selectedWallIndex} />
            </div>
          </div>
        </div>
        { deleteDialog }
      </div>
    );
  }

  static propTypes = {
    areaId: PropTypes.number.isRequired,
  }
}

const mapStateToProps = (state, props) => {
  return {
    area: state.routesAdmin.areas[props.areaId]
  };
}

const mapDispatchToProps = (dispatch) => {
  return {
    deleteArea: (locationId, areaId) => dispatch(deleteArea(locationId, areaId)),
    moveAreaUp: (areaId, locationId) => dispatch(moveAreaUp(areaId, locationId)),
    moveAreaDown: (areaId, locationId) => dispatch(moveAreaDown(areaId, locationId)),
    notify: message => dispatch(notify(message))
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(injectIntl(Area)));
