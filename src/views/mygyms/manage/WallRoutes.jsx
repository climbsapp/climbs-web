import React, { Component } from 'react';
import { PropTypes } from 'prop-types';
import { withRouter } from 'react-router';
import { connect } from 'react-redux';
import { defineMessages, injectIntl } from 'react-intl';
import RouteLines from './RouteLines';
import WallLine from './WallLine';
import { deleteWall, moveWallUp, moveWallDown } from '../../../actions/routesAdmin';
import { notify } from '../../../actions/notifications';

const messages = defineMessages({
  addWallTitle: {
    id: 'view.mygyms.manage.wall.title.add',
    defaultMessage: 'Add wall'
  },
  noWallsHelp: {
    id: 'view.mygyms.manage.wall.help.nowalls',
    defaultMessage: 'No walls yet. Go ahead and add one from above button! That\'s what you\'re here for anyway?'
  },
  removeWallSuccess: {
    id: 'notifications.mygyms.wall.remove.success',
    defaultMessage: 'Wall removed successfully.'
  }
});

class WallRoutes extends Component {
  state = {
    selectedWallIndex: 0
  }

  selectWall = (index) => {
    return () => {
      this.setState({ selectedWallIndex: index });
    };
  }

  selectWallByWallId = (wallId) => {
    const { walls } = this.props;
    for (let i = 0; i < walls.length; i++) {
      const wall = walls[i];
      if (wall.wallId === wallId) {
        this.selectWall(i)();
        return;
      }
    }
  }

  editWall = (wallId) => {
    return () => {
      this.props.router.push(this.props.router.location.pathname + '/editwall/' + wallId);
    };
  }

  printWall = (wallId) => {
    return () => {
      this.props.router.push(this.props.router.location.pathname + '/topo/' + wallId);
    };
  }

  moveWallUp = (wallId) => {
    const { locationId } = this.props;
    return () => {
      this.props.moveWallUp(wallId, locationId)
        .then(() => this.selectWallByWallId(wallId));
    };
  }

  moveWallDown = (wallId) => {
    const { locationId } = this.props;
    return () => {
      this.props.moveWallDown(wallId, locationId)
        .then(() => this.selectWallByWallId(wallId));
    };
  }

  deleteWall = (areaId, wallId) => {
    return () => {
      const { formatMessage } = this.props.intl;
      const { locationId } = this.props;
      this.props.deleteWall(locationId, areaId, wallId)
        .then(() => this.props.notify(formatMessage(messages.removeWallSuccess)));
    };
  }

  editPicture = (wallId) => {
    return () => {
      this.props.router.push(this.props.router.location.pathname + '/wallpicture/' + wallId);
    };
  }

  renderWalls() {
    let wallElements = [];
    const { walls } = this.props;
    if (walls && walls.length > 0) {
      for (let i = 0; i < walls.length; i++) {
        const wall = walls[i];

        wallElements.push(
          <WallLine name={wall.name} selected={i === this.state.selectedWallIndex}
            internalInfo={wall.internalInfo}
            typeName={wall.typeName} key={i} onSelectWall={this.selectWall(i)}
            deleteEnabled={wall.routes.length <= 0}
            onEditClick={this.editWall(wall.wallId)}
            onDeleteClick={this.deleteWall(wall.areaId, wall.wallId)}
            onPictureClick={this.editPicture(wall.wallId)}
            onPrintClick={this.printWall(wall.wallId)}
            onMoveUpClick={this.moveWallUp(wall.wallId)}
            onMoveDownClick={this.moveWallDown(wall.wallId)}
            pictureUrlSmall={wall.picture ? wall.picture.urlSmall : null}
            selectedBefore={i === this.state.selectedWallIndex - 1} />
        );
      }
    } else {
      const { formatMessage } = this.props.intl;
      wallElements.push(
        <li className="NoRoutes" key="line-none">
          { formatMessage(messages.noWallsHelp) }
        </li>
      );
    }
    return wallElements;
  }

  onAddWallClick = () => {
    this.props.router.push('/mygyms/' + this.props.params.locationId + '/manage/addwall/' + this.props.areaId);
  }

  render() {
    const { formatMessage } = this.props.intl;
    const wall = this.props.walls[this.state.selectedWallIndex];
    const wallId = wall ? wall.wallId : undefined;
    const routes = wall ? wall.routes : [];
    let wallElements = this.renderWalls();

    return (
      <div className="WallsRoutes">
        <div className="Row">
          <div className="Col Walls">
            <ul>
              <li>
                <div className="HeaderAction" onClick={this.onAddWallClick}>
                  <div className="Action AddWall" />
                  <div className="ActionDesc">{ formatMessage(messages.addWallTitle) }</div>
                </div>
              </li>
              {wallElements}
            </ul>
            <div className="WallsFooter"></div>
          </div>
          <div className="Col Routes">
            <RouteLines routes={routes} wallId={wallId} />
          </div>
        </div>

      </div>
    );
  }

  static propTypes = {
    walls: PropTypes.array,
  }
}

const mapStateToProps = (state, props) => {
  let walls = [].concat(props.walls.map(id => state.routesAdmin.walls[id]));

  walls.sort((a, b) => a.rowOrder - b.rowOrder);

  return {
    walls: walls
  };
}

const mapDispatchToProps = (dispatch) => {
  return {
    moveWallUp: (wallId, locationId) => dispatch(moveWallUp(wallId, locationId)),
    moveWallDown: (wallId, locationId) => dispatch(moveWallDown(wallId, locationId)),
    deleteWall: (locationId, areaId, wallId) => dispatch(deleteWall(locationId, areaId, wallId)),
    notify: message => dispatch(notify(message))
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(injectIntl(WallRoutes)));
