import React, { Component } from 'react';
import { PropTypes } from 'prop-types';
import { withRouter } from 'react-router';
import { connect } from 'react-redux';
import { defineMessages, injectIntl } from 'react-intl';
import { undoLastDelete } from '../../../actions/routesAdmin';
import { notify } from '../../../actions/notifications';
import RouteLine from './RouteLine';

const messages = defineMessages({
  lineTitle: {
    id: 'view.mygyms.manage.route.title.line',
    defaultMessage: 'Line #{lineNumber}'
  },
  addRoute: {
    id: 'view.mygyms.manage.route.title.add',
    defaultMessage: 'Add route'
  },
  undoLastDelete: {
    id: 'view.mygyms.manage.route.title.undoLastDelete',
    defaultMessage: 'Undo last delete'
  },
  helpNoWalls: {
    id: 'view.mygyms.manage.route.help.nowalls',
    defaultMessage: 'You need to first add wall before you can start adding routes! All routes are created under walls.'
  },
  helpNoRoutes: {
    id: 'view.mygyms.manage.route.help.noroutes',
    defaultMessage: 'Selected wall has no routes yet. Why don\'t you add one from above button?'
  },
  undoLastDeleteSuccess: {
    id: 'notifications.mygyms.wall.undoLastDelete.success',
    defaultMessage: 'Route returned successfully.'
  },
  undoLastDeleteFailure: {
    id: 'notifications.mygyms.wall.undoLastDelete.failure',
    defaultMessage: 'Obs! Something failed when returning last deleted route of the wall.'
  },
});

class RouteLines extends Component {
  onAddRouteClick = () => {
    if (this.props.wallId) {
      this.props.router.push('/mygyms/' + this.props.params.locationId + '/manage/addroute/' + this.props.wallId);
    }
  }

  onUndoLastDeleteClick = () => {
    if (this.props.wallId) {
      const { formatMessage } = this.props.intl;
      this.props.undoLastDelete(this.props.wallId)
        .then(() => this.props.notify(formatMessage(messages.undoLastDeleteSuccess)))
        .catch((error) => this.props.notify(formatMessage(messages.undoLastDeleteFailure)));
    }
  }

  render() {
    const { formatMessage } = this.props.intl;
    let lastLineNumber = 0;
    let routeElements = [];
    let routes = [].concat(this.props.routesArray).sort((r1, r2) => r1.line - r2.line);

    if (routes.length > 0) {
      for (let i = 0; i < routes.length; i++) {
        let route = routes[i];

        if (route.line > lastLineNumber) {
          routeElements.push(
            <li className="RouteLineNumberHeader" key={'line-' + route.line}>
              { formatMessage(messages.lineTitle, { lineNumber: route.line }) }
            </li>
          );
          lastLineNumber = route.line;
        }

        routeElements.push(<RouteLine wallId={this.props.wallId} routeId={route.routeId} key={'routeId-' + route.routeId} />);
      }
    } else if (this.props.wallId === undefined) {
      routeElements.push(
        <li className="NoRoutes" key="line-none">
          { formatMessage(messages.helpNoWalls) }
        </li>
      );
    } else {
      routeElements.push(
        <li className="NoRoutes" key="line-none">
          { formatMessage(messages.helpNoRoutes) }
        </li>
      );
    }

    const headerActionClassName = 'HeaderAction' + (!this.props.wallId ? ' disabled' : '');
    return (
      <ul>
        <li>
          <div className={headerActionClassName} onClick={this.onAddRouteClick}>
            <div className="Action AddRoute" />
            <div className="ActionDesc">{formatMessage(messages.addRoute)}</div>
          </div>
          <div className={headerActionClassName} onClick={this.onUndoLastDeleteClick}>
            <div className="Action UndoLastDelete" />
            <div className="ActionDesc">{formatMessage(messages.undoLastDelete)}</div>
          </div>
        </li>
        {routeElements}
      </ul>
    );
  }

  static propTypes = {
    routesArray: PropTypes.array.isRequired,
    wallId: PropTypes.number
  }
}

const mapStateToProps = (state, props) => {
  return {
    routesArray: props.routes.map(id => state.routesAdmin.routes[id])
  };
}

const mapDispatchToProps = (dispatch) => {
  return {
    undoLastDelete: (wallId) => dispatch(undoLastDelete(wallId)),
    notify: (message) => dispatch(notify(message))
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(injectIntl(RouteLines), { withRef: true }));
