import React, { Component } from 'react';
import { PropTypes } from 'prop-types';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';
import { defineMessages, injectIntl } from 'react-intl';
import { deleteRoute, undoDeleteRoute } from '../../../actions/routesAdmin';
import { notify, unNotify } from '../../../actions/notifications';
import { getColorStyle } from '../../../helpers/style';

const messages = defineMessages({
  editTitle: {
    id: 'view.mygyms.manage.route.title.edit',
    defaultMessage: 'Edit route'
  },
  removeTitle: {
    id: 'view.mygyms.manage.route.title.remove',
    defaultMessage: 'Remove route'
  },
  undoRemoveTitle: {
    id: 'view.mygyms.manage.route.undoRemove',
    defaultMessage: 'Undo'
  },
  unknownRouteNameTitle: {
    id: 'view.mygyms.manage.route.name.unknown',
    defaultMessage: '<no name>'
  },
  unknownRouteInfoTitle: {
    id: 'view.mygyms.manage.route.info.unknown',
    defaultMessage: '<no info>'
  },
  unknownRouteSetterTitle: {
    id: 'view.mygyms.manage.route.routesetter.unknown',
    defaultMessage: '<unknown>'
  },
  twoRouteSettersTitle: {
    id: 'view.mygyms.manage.route.routesetter.two.title',
    defaultMessage: '{setterName} and {setter2Name}'
  },
  deleteSuccess: {
    id: 'notifications.mygyms.route.delete.success',
    defaultMessage: 'Route removed successfully.'
  },
  deleteFailed: {
    id: 'notifications.mygyms.route.delete.failure',
    defaultMessage: 'Meh! Route removal failed. Like totally.'
  },
  undoDeleteSuccess: {
    id: 'notifications.mygyms.route.undoDelete.success',
    defaultMessage: 'Delete undone and route returned.'
  }
});

class RouteLine extends Component {
  editRoute = () => {
    this.props.router.push('/mygyms/' + this.props.params.locationId + '/manage/editroute/' + this.props.routeId);
  }

  deleteRoute = () => {
    const { formatMessage } = this.props.intl;
    const { locationId } = this.props.params;
    const { wallId, routeId, routeCreated, gradingSystem } = this.props;
    const routeDeleted = new Date();
    const active = false;
    this.props.deleteRoute({ wallId, routeId, routeCreated, gradingSystem, routeDeleted, active }, locationId)
      .then(() => {
        let notificationId;
        const undoAction = {
          title: formatMessage(messages.undoRemoveTitle),
          onClick: () => {
            this.props.undoDeleteRoute({ wallId, routeId, routeCreated, gradingSystem, routeDeleted: null, active: true }, locationId)
              .then(() => this.props.notify(formatMessage(messages.undoDeleteSuccess)));
            this.props.unNotify(notificationId);
          }
        };
        notificationId = this.props.notify(formatMessage(messages.deleteSuccess), { action: undoAction });
      })
      .catch(error => this.props.notify(formatMessage(messages.deleteFailed)))
  }

  /* Title showing when hovering on the label */
  getSetterNameTitle() {
    const { formatMessage } = this.props.intl;
    const { setterName, setter2Name } = this.props;
    if (setterName && setter2Name) {
      return formatMessage(messages.twoRouteSettersTitle, { setterName, setter2Name });
    } else if (setterName) {
      return setterName;  
    } else {
      return formatMessage(messages.unknownRouteSetterTitle);
    }
  }

  /* Label that shows on the list */
  getSetterNameLabel() {
    const { formatMessage } = this.props.intl;
    const { setterName, setter2Name } = this.props;
    if (setterName && setter2Name) {
      return setterName + ' +1'; // TODO: Maybe localize?
    } else if (setterName) {
      return setterName;  
    } else {
      return formatMessage(messages.unknownRouteSetterTitle);
    }
  }

  render() {
    const { formatMessage, formatDate } = this.props.intl;
    const { name, info, color, routeCreated } = this.props;
    const routeColorClass = 'RouteColor' + (!color.hex ? ' Transparent' : '');
    const routeColorStyle = getColorStyle(color);

    const routeNameClass = 'RouteName ' + (!name ? 'empty' : '');
    const routeDescClass = 'RouteDesc ' + (!info ? 'empty' : '');

    const routeName = name ? name : formatMessage(messages.unknownRouteNameTitle);
    const routeInfo = info ? info : formatMessage(messages.unknownRouteInfoTitle);
    const routeSetDate = formatDate(routeCreated, { year: 'numeric', month: 'numeric', day: 'numeric' });
    const setterNameLabel = this.getSetterNameLabel();
    const setterNameTitle = this.getSetterNameTitle();
    return (
      <li className="RouteLine">
        <div className="RouteLinePart">
          <div className="RouteLineStart">
            <div className={routeColorClass} style={routeColorStyle}>
              {this.props.grade}
            </div>
            <div className="RouteInfo">
              <div className={routeNameClass}>{routeName}</div>
              <div className={routeDescClass}>{routeInfo}</div>
            </div>
          </div>
        </div>
        <div className="RouteLinePart">
          <div className="RouteLineEnd">
            <div className="RouteActions">
              <div className="Action Edit" onClick={this.editRoute} title={formatMessage(messages.editTitle)} />
              <div className="Action Delete" onClick={this.deleteRoute} title={formatMessage(messages.removeTitle)} />
            </div>
            <div className="RouteInfo">
              <div className="RouteSetDate">{routeSetDate}</div>
              <div className="RouteSetterName" title={setterNameTitle}>{setterNameLabel}</div>
            </div>
          </div>
        </div>
      </li>
    );
  }

  static propTypes = {
    wallId: PropTypes.number.isRequired,
    routeId: PropTypes.number.isRequired
  }
}

const mapStateToProps = (state, props) => {
  const route = state.routesAdmin.routes[props.routeId];
  const wall = state.routesAdmin.walls[route.wallId];
  const color = state.routesAdmin.colors[route.colorId] || { hex: null };
  const routeSetter = state.routesAdmin.routeSetters[route.routeSetterId];
  const routeSetter2 = state.routesAdmin.routeSetters[route.routeSetter2Id] || null;

  const setterName = routeSetter ? routeSetter.name : undefined;
  const setter2Name = routeSetter2 ? routeSetter2.name : undefined;

  return {
    name: route.name,
    info: route.info,
    routeCreated: route.routeCreated,
    grade: route.grade,
    gradingSystem: wall.gradingSystem,
    color: color,
    setterName: setterName,
    setter2Name: setter2Name
  };
}

const mapDispatchToProps = (dispatch) => {
  return {
    deleteRoute: (route, locationId) => dispatch(deleteRoute(route, locationId)),
    undoDeleteRoute: (route, locationId) => dispatch(undoDeleteRoute(route, locationId)),
    notify: (message, options) => dispatch(notify(message, options)),
    unNotify: id => dispatch(unNotify(id))
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(injectIntl(RouteLine)));
