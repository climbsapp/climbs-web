import React, { Component } from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';
import { defineMessages, injectIntl } from 'react-intl';
import { ViewHeader } from '../../../components/header/ViewHeader';
import Area from './Area';
import './ManageRoutes.css';

const messages = defineMessages({
  title: {
    id: 'view.mygyms.manage.title',
    defaultMessage: 'Manage routes'
  },
  description: {
    id: 'view.mygyms.manage.description',
    defaultMessage: 'In Climbs gyms are arranged under areas which contain walls in which routes are set to. Routes are put under lines. A wall consists usually of 1-6 lines which represent the bolt-lines. Boulder walls usually contain just one line because problems rarely follow straight lines up.'
  }
});

class ManageRoutes extends Component {
  render() {
    const { formatMessage } = this.props.intl;
    let areas = this.props.areas;
    let areaElements = [];
    for (let i = 0; i < areas.length; i++) {
      let area = areas[i];
      areaElements.push(<Area areaId={area.areaId} key={area.areaId} />);
    }

    return (
      <div className="RoutesAdmin">
        {this.props.children}

        <ViewHeader title={formatMessage(messages.title)} />
        <div id="RouteManagementInfo" className="Content-Area">
          { formatMessage(messages.description) }
        </div>

        {areaElements}
      </div>
    );
  }
}


const mapStateToProps = (state, props) => {
  const admin = state.routesAdmin;
  const location = admin.locations[props.params.locationId];
  const isNotUndefined = (value) => value !== undefined;
  const areas = [].concat(location ? location.areas.map(id => admin.areas[id]).filter(isNotUndefined) : []);
  areas.sort((a, b) => a.rowOrder - b.rowOrder);
  return {
    // Areas need to be filtered because if selected location changed, we cant be sure all areas are available
    areas: areas
  };
}

export default connect(mapStateToProps)(withRouter(injectIntl(ManageRoutes)));
