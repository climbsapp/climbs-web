import React, { Component } from 'react';
import { withRouter } from 'react-router';
import { connect } from 'react-redux';
import { defineMessages, injectIntl } from 'react-intl';
import RouteOverlayForm from '../../../../components/forms/RouteOverlayForm';
import { getRouteTags, editRoute } from '../../../../actions/routesAdmin';
import { notify } from '../../../../actions/notifications';
import editRouteImg from '../../../../images/icons/plus-circle-white.svg';

const messages = defineMessages({
  title: {
    id: 'view.mygyms.manage.route.edit.title',
    defaultMessage: 'Edit route'
  },
  subtitle: {
    id: 'view.mygyms.manage.route.edit.subtitle',
    defaultMessage: '(of wall {wallName})'
  },
  submit: {
    id: 'view.mygyms.manage.route.edit.submit',
    defaultMessage: 'Save changes'
  },
  editSuccess: {
    id: 'notifications.mygyms.route.edit.success',
    defaultMessage: 'Yea! Route modified successfully.'
  },
  editFailure: {
    id: 'notifications.mygyms.route.edit.failure',
    defaultMessage: 'Poop! Route modification failed for some reason.'
  }
});

class EditRouteOverlay extends Component {
  close = () => {
    this.props.router.goBack();
  }

  componentDidMount() {
    this.props.getRouteTags(this.props.params.routeId);
  }

  submit = (routeData, tagsData) => {
    const { formatMessage } = this.props.intl;
    const { locationId } = this.props.params;
    const tags = tagsData.map(tag => Object.assign({}, tag, { targetId: this.props.route.routeId }))
    const data = Object.assign({}, routeData, { routeId: this.props.route.routeId, tags: tags });
    this.props.editRoute(data, locationId)
      .then(this.close)
      .then(() => this.props.notify(formatMessage(messages.editSuccess)))
      .catch(error => this.props.notify(formatMessage(messages.editFailure)));
  }

  enhanceRoute(route, grades) {
    if (!route) {
      return null;
    }

    return Object.assign({}, route, {
      gradeIndex: grades.indexOf(route.grade)
    });
  }

  render() {
    const { formatMessage } = this.props.intl;
    const { colors, routeSetters, grades, wall, isLoading, route } = this.props;
    const title = formatMessage(messages.title);
    const subtitle = formatMessage(messages.subtitle, { wallName: (wall ? wall.name : '') });
    const submitTitle = formatMessage(messages.submit);

    return (
      <RouteOverlayForm title={title} subtitle={subtitle} submitTitle={submitTitle}
        isLoading={isLoading} onSubmit={this.submit}
        onClose={this.close} iconImg={editRouteImg} wall={wall}
        grades={grades} colors={colors} routeSetters={routeSetters} data={this.enhanceRoute(route, grades)}  />
    );
  }
}

const mapStateToProps = (state, props) => {
  const data = state.routesAdmin;
  const route = data.routes[props.params.routeId];
  const wall = route ? data.walls[route.wallId] : undefined;
  const grades = wall ? data.gradingSystems[wall.gradingSystem].grades : [];

  return {
    route: route,
    isLoading: data.isLoading,
    wall: wall,
    colors: data.colors,
    routeSetters: data.routeSetters,
    grades: grades
  };
}

const mapDispatchToProps = (dispatch) => {
  return {
    getRouteTags: (routeId) => dispatch(getRouteTags(routeId)),
    editRoute: (routeData, locationId) => {
      return dispatch(editRoute(routeData, locationId));
    },
    notify: message => dispatch(notify(message))
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(injectIntl(EditRouteOverlay), { withRef: true}));
