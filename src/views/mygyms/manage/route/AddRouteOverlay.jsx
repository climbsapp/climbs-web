import React, { Component } from 'react';
import { withRouter } from 'react-router';
import { connect } from 'react-redux';
import { defineMessages, injectIntl } from 'react-intl';
import RouteOverlayForm from '../../../../components/forms/RouteOverlayForm';
import { addNewRoute } from '../../../../actions/routesAdmin';
import { notify } from '../../../../actions/notifications';
import addRouteImg from '../../../../images/icons/plus-circle-white.svg';

const messages = defineMessages({
  title: {
    id: 'view.mygyms.manage.route.add.title',
    defaultMessage: 'Add route'
  },
  subtitle: {
    id: 'view.mygyms.manage.route.add.subtitle',
    defaultMessage: '(to wall {wallName})'
  },
  submit: {
    id: 'view.mygyms.manage.route.add.submit',
    defaultMessage: 'Add route'
  },
  success: {
    id: 'notifications.mygyms.route.add.success',
    defaultMessage: 'Yea! Route added successfully.'
  },
  failure: {
    id: 'notifications.mygyms.route.add.failure',
    defaultMessage: 'Meh! Adding the route failed!'
  }
});

class AddRouteOverlay extends Component {
  close = () => {
    this.props.router.goBack();
  }

  submit = (routeData, tagsData) => {
    const { formatMessage } = this.props.intl;
    const { locationId } = this.props.params;
    this.props.addNewRoute(routeData, tagsData, locationId)
      .then(this.close)
      .then(() => this.props.notify(formatMessage(messages.success)))
      .catch(error => this.props.notify(formatMessage(messages.failure)));
  }

  render() {
    const { formatMessage } = this.props.intl;
    const { colors, routeSetters, grades, wall, isLoading } = this.props;
    const title = formatMessage(messages.title);
    const subtitle = formatMessage(messages.subtitle, { wallName: (wall ? wall.name : '') });
    const submitTitle = formatMessage(messages.submit);
    const gradeIndex = Object.keys(grades).length > 0 ? Math.floor(grades.length / 3) : 0;
    const data = {
      routeSetterId: Object.keys(routeSetters).length > 0 ? parseInt(Object.keys(routeSetters)[0], 10) : -1,
      colorId: Object.keys(colors).length > 0 ? parseInt(Object.keys(colors)[0], 10) : -1,
      grade: Object.keys(grades).length > 0 ? grades[gradeIndex] : '',
      gradeIndex: gradeIndex
    };

    return (
      <RouteOverlayForm title={title} subtitle={subtitle} submitTitle={submitTitle}
        isLoading={isLoading} onSubmit={this.submit}
        onClose={this.close} iconImg={addRouteImg} wall={wall}
        grades={grades} colors={colors} routeSetters={routeSetters} data={data}  />
    );
  }
}

const mapStateToProps = (state, props) => {
  let wallId = parseInt(props.params.wallId, 10);
  let wall = state.routesAdmin.walls[wallId];
  let grades = wall ? state.routesAdmin.gradingSystems[wall.gradingSystem].grades : [];

  return {
    isLoading: state.routesAdmin.isAddingNewRoute,
    wall: wall,
    colors: state.routesAdmin.colors,
    routeSetters: state.routesAdmin.routeSetters,
    grades: grades
  };
}

const mapDispatchToProps = (dispatch) => {
  return {
    addNewRoute: (routeData, tagsData, locationId) => 
      dispatch(addNewRoute(routeData, tagsData, locationId)),
    notify: message => dispatch(notify(message))
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(injectIntl(AddRouteOverlay), { withRef: true}));
