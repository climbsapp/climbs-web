import React, { Component } from 'react';
import { withRouter } from 'react-router';
import { connect } from 'react-redux';
import { defineMessages, injectIntl } from 'react-intl';
import PictureOverlayForm from '../../../../components/forms/PictureOverlayForm';
import { Button } from '../../../../components';
import { uploadLocationPicture } from '../../../../actions/routesAdmin';
import { notify } from '../../../../actions/notifications';
import cameraImg from '../../../../images/icons/camera-white.svg';
import './LocationHeaderPictureOverlay.css';

const messages = defineMessages({
  title: {
    id: 'view.mygyms.manage.location.picture.title',
    defaultMessage: 'Gym header picture'
  },
  subtitle: {
    id: 'view.mygyms.manage.location.picture.subtitle',
    defaultMessage: 'of {locationName}'
  },
  addNewTitle: {
    id: 'view.mygyms.manage.location.picture.addNew',
    defaultMessage: 'Upload new header picture'
  },
  addFirstTitle: {
    id: 'view.mygyms.manage.location.picture.addFirst',
    defaultMessage: 'Upload gym header picture'
  },
  helpText: {
    id: 'view.mygyms.manage.location.picture.help',
    defaultMessage: 'Gym picture is shown in the climber apps as the header of your gym.'
  },
  success: {
    id: 'notifications.mygyms.location.picture.add.success',
    defaultMessage: 'Gym header picture added successfully.'
  },
  failure: {
    id: 'notifications.mygyms.locationwall.picture.add.failure',
    defaultMessage: 'Ugh! Failed to add the picture for unknown reason.'
  }
});

class LocationHeaderPictureOverlay extends Component {
  renderHelp() {
    const { formatMessage } = this.props.intl;
    return (
      <div>
        <p>{ formatMessage(messages.helpText) }</p>
      </div>
    );
  }

  uploadFileClick = () => {
    this.refs.picture.click();
  }

  uploadFileOnChange = (event) => {
    const { formatMessage } = this.props.intl;
    let formData = new FormData();
    formData.append('id', this.props.venue.locationId);
    formData.append('context', 'LOCATION');
    formData.append('file', this.refs.picture.files[0]);
    this.props.uploadLocationPicture(this.props.venue.locationId, formData)
      .then(() => this.props.router.goBack())
      .then(() => this.props.notify(formatMessage(messages.success)))
      .catch(error => this.props.notify(formatMessage(messages.failure)));
  }

  renderImgElement() {
    const { venue } = this.props;
    const url = (venue && venue.picture) ? venue.picture.url/*.replace('.test.', '.prod.')*/ : null;
    if (url) {
      return (<img className="OverlayPicture" src={url} alt="Location header" />);
    } else {
      return (<div className="NoPicture" />);
    }
  }

  render() {
    const { formatMessage } = this.props.intl;
    const { venue } = this.props;
    const title = formatMessage(messages.title);
    const subtitle = formatMessage(messages.subtitle, { locationName: (venue ? venue.name : '') });
    const helpElement = this.renderHelp();
    const buttonTitle = (venue && venue.picture) ? formatMessage(messages.addNewTitle) : formatMessage(messages.addFirstTitle);
    const style = { 'opacity': '0' };
    return (
      <PictureOverlayForm title={title} subtitle={subtitle} iconImg={cameraImg}
        helpElement={helpElement}>
        <div className="row">
          <div className="col s12">
            {this.renderImgElement()}
          </div>
        </div>
        <div className="Overlay-Footer">
          <Button onClick={this.uploadFileClick} loading={this.props.isLoading}
            label={buttonTitle} />
          <input name="picInput" type="file" ref="picture" onChange={this.uploadFileOnChange} style={style} />
        </div>
      </PictureOverlayForm>
    );
  }
}

const mapStateToProps = (state, props) => {
  return {
    isLoading: state.routesAdmin.isLoading,
    venue: state.routesAdmin.locations[props.params.locationId]
  };
}

const mapDispatchToProps = (dispatch) => {
  return {
    uploadLocationPicture: (locationId, formData) => dispatch(uploadLocationPicture(locationId, formData)),
    notify: message => dispatch(notify(message))
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(injectIntl(LocationHeaderPictureOverlay)));
