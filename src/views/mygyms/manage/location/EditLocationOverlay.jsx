import React, { Component } from 'react';
import { withRouter } from 'react-router';
import { connect } from 'react-redux';
import { defineMessages, injectIntl } from 'react-intl';
import { LocationOverlayForm } from '../../../../components';
import { editLocation } from '../../../../actions/routesAdmin';
import { notify } from '../../../../actions/notifications';
import editLocationImg from '../../../../images/icons/pencil-box-outline-white.svg';

const messages = defineMessages({
  title: {
    id: 'view.mygyms.manage.location.edit.title',
    defaultMessage: 'Edit location'
  },
  subtitle: {
    id: 'view.mygyms.manage.location.edit.subtitle',
    defaultMessage: '{locationName}'
  },
  submit: {
    id: 'view.mygyms.manage.location.edit.submit',
    defaultMessage: 'Save changes'
  },
  success: {
    id: 'notifications.mygyms.location.edit.success',
    defaultMessage: 'Yay! Location modified successfully.'
  }
});

class EditLocationOverlay extends Component {
  close = () => {
    this.props.router.goBack();
  }

  submit = (locationData) => {
    const { formatMessage } = this.props.intl;
    const { locationId, locationType } = this.props.locationData;
    const data = Object.assign({}, locationData, { locationId, locationType });
    this.props.editLocation(data)
      .then(this.close)
      .then(() => this.props.notify(formatMessage(messages.success)));
  }

  render() {
    const { formatMessage } = this.props.intl;
    const title = formatMessage(messages.title);
    const subtitle = formatMessage(messages.subtitle, { locationName: this.props.locationData.name });
    const submitTitle = formatMessage(messages.submit);
    return (
      <LocationOverlayForm onSubmit={this.submit} onClose={this.close}
        title={title} subtitle={subtitle} iconImg={editLocationImg} submitTitle={submitTitle}
        data={this.props.locationData} loading={this.props.loading} />
    );
  }
}

const mapStateToProps = (state, props) => {
  const location = state.routesAdmin.locations[props.params.locationId] || {};
  return {
    loading: state.routesAdmin.isLoading,
    locationData: location,
    locationId: props.params.locationId
  };
}

const mapDispatchToProps = (dispatch) => {
  return {
    editLocation: (locationData) => dispatch(editLocation(locationData)),
    notify: message => dispatch(notify(message))
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(injectIntl(EditLocationOverlay), { withRef: true }));
