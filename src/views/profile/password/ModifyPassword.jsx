import React, { Component } from 'react';
import { connect } from 'react-redux';
import { defineMessages, injectIntl } from 'react-intl';
import { Button } from '../../../components';
import { ViewHeader } from '../../../components/header/ViewHeader';
import { FormInput } from '../../../components/forms/Inputs';
import { changePassword } from '../../../actions';
import { notify } from '../../../actions/notifications';

const messages = defineMessages({
  passwordTitle: {
    id: 'view.profile.password.title',
    defaultMessage: 'Change password'
  },
  currentPasswordLabel: {
    id: 'view.profile.password.label.current',
    defaultMessage: 'Current password'
  },
  newPasswordLabel: {
    id: 'view.profile.password.label.new',
    defaultMessage: 'New password'
  },
  newPasswordError: {
    id: 'view.profile.password.error.new',
    defaultMessage: 'At least 5 characters'
  },
  repeatNewPasswordLabel: {
    id: 'view.profile.password.label.new.repeat',
    defaultMessage: 'Repeat new password'
  },
  repeatNewPasswordError: {
    id: 'view.profile.password.error.new.repeat',
    defaultMessage: 'Must equal the new password'
  },
  actionChangePassword: {
    id: 'action.title.profile.password.save',
    defaultMessage: 'Change'
  },
  warningFacebookUser: {
    id: 'view.profile.password.warning.facebookUser',
    defaultMessage: 'Hey! You are logged in with your Facebook account. You can\'t change your password in Climbs.'
  },
  successChangingPassword: {
    id: 'notifications.profile.passwordChange.success',
    defaultMessage: 'Password changed'
  },
  errorChangingPassword: {
    id: 'notifications.profile.passwordChange.error',
    defaultMessage: 'Failed to change password: {reason}'
  }
});

class ModifyPassword extends Component {
  state = {
    password: '',
    newPassword: '',
    newPassword2: ''
  }

  onChange = (fieldName) => {
    return (event) => {
      let change = {};
      change[fieldName] = event.target.value;
      this.setState(change);
    };
  }

  submit = () => {
    const { formatMessage } = this.props.intl;
    const { password, newPassword } = this.state;
    const { notify } = this.props;
    this.props.changePassword(password, newPassword)
    .then(() => {
      this.setState({ password: '', newPassword: '', newPassword2: '' });
      notify(formatMessage(messages.successChangingPassword));
    })
    .catch(error => {
      notify(formatMessage(messages.errorChangingPassword, { reason: error.message }));
    });
  }

  isFormValid() {
    const { password, newPassword, newPassword2 } = this.state;
    return !!password && !!newPassword && !!newPassword2 
      && newPassword === newPassword2
      && newPassword.length >= 5;
  }

  renderForm() {
    const { formatMessage } = this.props.intl;
    if (!this.props.isFacebookUser) {
      const { password, newPassword, newPassword2 } = this.state;
      const { isLoading } = this.props;
      return (
        <div className="row">
          <div className="col s6">
            <div className="row">
              <FormInput name="Password" value={password} onChange={this.onChange('password')}
                label={formatMessage(messages.currentPasswordLabel)} type="password" />
              <FormInput name="NewPassword" value={newPassword} onChange={this.onChange('newPassword')}
                valid={newPassword === '' || newPassword.length >= 5} errorMessage={formatMessage(messages.newPasswordError)}
                label={formatMessage(messages.newPasswordLabel)} type="password" />
              <FormInput name="NewPassword2" value={newPassword2} onChange={this.onChange('newPassword2')}
                valid={newPassword === newPassword2} errorMessage={formatMessage(messages.repeatNewPasswordError)}
                label={formatMessage(messages.repeatNewPasswordLabel)} type="password" />
              <Button label={formatMessage(messages.actionChangePassword)} isLoading={isLoading} disabled={!this.isFormValid()}
                onClick={this.submit} />
            </div>
          </div>
        </div>
      );
    } else {
      return (
        <div className="NoDataContainer">
          { formatMessage(messages.warningFacebookUser) }
        </div>
      )
    }
  }

  render() {
    const { formatMessage } = this.props.intl;
    return (
      <div className="ModifyPassword">
        <ViewHeader title={formatMessage(messages.passwordTitle)} />
        <div className="Content-Area">
          {this.renderForm()}
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state, props) => {
  const profile = state.profile.data;
  return {
    isLoading: state.profile.isLoading,
    isFacebookUser: !!profile.facebookId
  };
}

const mapDispatchToProps = (dispatch, state) => {
  return {
    changePassword: (oldPassword, newPassword) => 
      dispatch(changePassword(oldPassword, newPassword)),
    notify: (text) => dispatch(notify(text))
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(injectIntl(ModifyPassword));
