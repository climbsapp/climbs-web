import React, { Component } from 'react';
import { connect } from 'react-redux';
import { defineMessages, injectIntl } from 'react-intl';
import { logout } from '../../actions/login';
import { SubNav, SubNavItem, SubNavAction } from '../../components/navigation/SubNav';
import showProfileImg from '../../images/icons/account-circle.svg';
import editProfileImg from '../../images/icons/account-settings-variant.svg';
import changePwdImg from '../../images/icons/key-variant.svg';
import logoutImg from '../../images/icons/exit-to-app-616161.svg';

const messages = defineMessages({
  subNavItemInfo: {
    id: 'view.profile.subnav.info',
    defaultMessage: 'Information'
  },
  subNavItemModify: {
    id: 'view.profile.subnav.modify',
    defaultMessage: 'Modify'
  },
  subNavItemPassword: {
    id: 'view.profile.subnav.password',
    defaultMessage: 'Password'
  },
  actionLogout: {
    id: 'action.title.logout',
    defaultMessage: 'Logout'
  }
});

class ProfileRoot extends Component {

  render() {
    const { formatMessage } = this.props.intl;
    return (
      <div className="Profile">
        <div className="row">
          <div className="col l2 m3 s4">
            <SubNav>
              <SubNavItem text={formatMessage(messages.subNavItemInfo)} img={showProfileImg}
                path="profile/info" location={this.props.location} />
              <SubNavItem text={formatMessage(messages.subNavItemModify)} img={editProfileImg}
                path="profile/modify" location={this.props.location} />
              <SubNavItem text={formatMessage(messages.subNavItemPassword)} img={changePwdImg}
                path="profile/password" location={this.props.location} />
              <SubNavAction text={formatMessage(messages.actionLogout)} img={logoutImg}
                onClick={this.props.logout} />
            </SubNav>
          </div>
          <div className="col l9 m9 s8">
            { this.props.children }
          </div>
        </div>
      </div>
    )
  }
}

const mapStateToProps = (state, props) => {
  return {};
}

const mapDispatchToProps = (dispatch) => {
  return {
    logout: () => dispatch(logout())
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(injectIntl(ProfileRoot));
