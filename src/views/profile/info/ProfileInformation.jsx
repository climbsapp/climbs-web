import React, { Component } from 'react';
import { PropTypes } from 'prop-types';
import { defineMessages, injectIntl } from 'react-intl';
import { ViewHeader } from '../../../components/header/ViewHeader';
import { FormTextOutput, FormChipOutput } from '../../../components/forms/Inputs';
import { fetchProfile } from '../../../actions/profile';
import './ProfileInformation.css';
import DefaultProfilePic from '../../../images/icons/account_9e9e9e.svg';

const messages = defineMessages({
  infoTitle: {
    id: 'view.profile.info.title',
    defaultMessage: 'Profile'
  },
  labelName: {
    id: 'view.profile.info.label.name',
    defaultMessage: 'Name'
  },
  labelEmail: {
    id: 'view.profile.info.label.email',
    defaultMessage: 'Email'
  },
  labelLoginType: {
    id: 'view.profile.info.label.logintype',
    defaultMessage: 'Login type'
  },
  labelYearOfBirth: {
    id: 'view.profile.info.label.yearofbirth',
    defaultMessage: 'Year of birth'
  },
  labelAnonymity: {
    id: 'view.profile.info.label.anonymity',
    defaultMessage: 'Anonymity'
  },
  labelGender: {
    id: 'view.profile.info.label.gender',
    defaultMessage: 'Gender'
  },
  labelProfilePic: {
    id: 'view.profile.info.label.profilepic',
    defaultMessage: 'Profile picture'
  },
  valueLoginTypeUserPass: {
    id: 'view.profile.info.value.logintype.userpass',
    defaultMessage: 'USERNAME & PASSWORD'
  },
  valueLoginTypeFacebook: {
    id: 'view.profile.info.value.logintype.facebook',
    defaultMessage: 'FACEBOOK'
  },
  valueAnonymityTrue: {
    id: 'view.profile.info.value.anonymity.true',
    defaultMessage: 'Your name is hidden from others'
  },
  valueAnonymityFalse: {
    id: 'view.profile.info.value.anonymity.false',
    defaultMessage: 'Your name is shown in the leaderboards'
  },
  valueGender: {
    id: 'view.profile.info.value.gender',
    defaultMessage: '{gender, select, MALE {male} FEMALE {female} OTHER {other} other {unknown}}'
  }
});

class ProfileInformation extends Component {
  state = {
    name: '',
    yearOfBirth: '',
    pictureUrl: '',
    gender: '',
    email: '',
    loginTypeString: '',
    anonymity: ''
  }

  componentDidMount() {
    const { dispatch } = this.props;
    dispatch(fetchProfile());
  }

  componentWillReceiveProps(props) {
    if (!props.profile.isLoading) {
      const { formatMessage } = this.props.intl;
      let profile = props.profile.data;
      // HACK: Replace test from url to make link work
      let pictureUrl = profile.pictures.length > 0 ? profile.pictures[0].url.replace('dev', 'prod') : '';
      this.setState({
        name: profile.name,
        yearOfBirth: profile.yearOfBirth,
        pictureUrl: pictureUrl,
        gender: formatMessage(messages.valueGender, { gender: profile.gender ? profile.gender.toUpperCase() : 'OTHER' }),
        email: profile.email,
        loginTypeString: profile.facebookId == null ? formatMessage(messages.valueLoginTypeUserPass) : formatMessage(messages.valueLoginTypeFacebook),
        anonymity: profile.anonymous ? formatMessage(messages.valueAnonymityTrue) : formatMessage(messages.valueAnonymityFalse)
      });
    }
  }

  render() {
    const { formatMessage } = this.props.intl;
    const profilePic = this.state.pictureUrl ? this.state.pictureUrl : DefaultProfilePic;
    const { name, email, yearOfBirth, gender, anonymity, loginTypeString } = this.state;
    return (
      <div className="ProfileInfo">
        <ViewHeader title={formatMessage(messages.infoTitle)} />
        <div className="row Content-Area">
          <div className="col s6">
            <div className="row">
              <FormTextOutput name="name" value={name} label={formatMessage(messages.labelName)} />
              <FormTextOutput name="email" value={email} label={formatMessage(messages.labelEmail)} />
              <FormChipOutput name="loginType" value={loginTypeString} label={formatMessage(messages.labelLoginType)} />
              <FormTextOutput name="yearOfBirth" value={yearOfBirth} label={formatMessage(messages.labelYearOfBirth)} />
              <FormTextOutput name="gender" value={gender} label={formatMessage(messages.labelGender)} />
              <FormTextOutput name="anonymity" value={anonymity} label={formatMessage(messages.labelAnonymity)} />
            </div>
          </div>
          <div className="col s6">
            <label className="active">{formatMessage(messages.labelProfilePic)}</label>
            <div className="ProfilePicContainer z-depth-1">
              <img src={profilePic} alt="Profile" />
            </div>
          </div>
        </div>
      </div>
    )
  }

  static propTypes = {
    dispatch: PropTypes.func.isRequired
  }
}

export default injectIntl(ProfileInformation);
