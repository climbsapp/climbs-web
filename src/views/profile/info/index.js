import { connect } from 'react-redux';
import ProfileInformation from './ProfileInformation';

const mapStateToProps = (state) => {
  return {
    profile: state.profile
  };
}

const ProfileInfoView = connect(mapStateToProps)(ProfileInformation);

export default ProfileInfoView;
