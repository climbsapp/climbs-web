import ProfileInfoView from './info';
import ModifyProfile from './modify';
import ProfileRoot from './ProfileRoot';
import ModifyPassword from './password/ModifyPassword';

export {
  ProfileInfoView,
  ModifyProfile,
  ProfileRoot,
  ModifyPassword
};
