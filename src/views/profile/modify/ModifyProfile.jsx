import React, { Component } from 'react';
import { PropTypes } from 'prop-types';
import { defineMessages, injectIntl } from 'react-intl';
import { fetchProfile } from '../../../actions';
import { ViewHeader } from '../../../components/header/ViewHeader';
import { FormInput, FormChipsInput } from '../../../components/forms/Inputs';
import { getGenderOptions } from '../../../helpers/commonOptions';
import './ModifyProfile.css';
import defaultProfilePic from '../../../images/icons/account_9e9e9e.svg';

const messages = defineMessages({
  modifyTitle: {
    id: 'view.profile.modify.title',
    defaultMessage: 'Modify profile'
  },
  labelName: {
    id: 'view.profile.modify.label.name',
    defaultMessage: 'Name'
  },
  errorNameRequired: {
    id: 'view.profile.modify.error.name',
    defaultMessage: 'Name is required'
  },
  labelEmail: {
    id: 'view.profile.modify.label.email',
    defaultMessage: 'Email'
  },
  errorEmailRequired: {
    id: 'view.profile.modify.error.email',
    defaultMessage: 'Email is required'
  },
  labelLoginType: {
    id: 'view.profile.modify.label.logintype',
    defaultMessage: 'Login type'
  },
  labelYearOfBirth: {
    id: 'view.profile.modify.label.yearofbirth',
    defaultMessage: 'Year of birth'
  },
  errorYearOfBirthRequired: {
    id: 'view.profile.modify.error.yearofbirth',
    defaultMessage: 'Year of birth is required'
  },
  labelAnonymity: {
    id: 'view.profile.modify.label.anonymity',
    defaultMessage: 'Profile type'
  },
  labelLanguage: {
    id: 'view.profile.modify.label.language',
    defaultMessage: 'Language'
  },
  labelGender: {
    id: 'view.profile.modify.label.gender',
    defaultMessage: 'Gender'
  },
  labelProfilePic: {
    id: 'view.profile.modify.label.profilepic',
    defaultMessage: 'Profile picture'
  },
  valueAnonymityFalse: {
    id: 'view.profile.modify.value.anonymity.false',
    defaultMessage: 'Public'
  },
  valueAnonymityTrue: {
    id: 'view.profile.modify.value.anonymity.true',
    defaultMessage: 'Private'
  },
  tooltipAnonymityFalse: {
    id: 'view.profile.modify.tooltip.anonymity.false',
    defaultMessage: 'Name shown at leaderboards'
  },
  tooltipAnonymityTrue: {
    id: 'view.profile.modify.tooltip.anonymity.true',
    defaultMessage: 'Name not shown at leaderboards'
  },
  valueLanguageEnglish: {
    id: 'view.profile.modify.value.language.english',
    defaultMessage: 'English'
  },
  valueLanguageFinnish: {
    id: 'view.profile.modify.value.language.finnish',
    defaultMessage: 'Finnish'
  },
  valueLanguageCzech: {
    id: 'view.profile.modify.value.language.czech',
    defaultMessage: 'Czech'
  },
  valueLanguageItalian: {
    id: 'view.profile.modify.value.language.italian',
    defaultMessage: 'Italian'
  },
  actionUploadNewPicture: {
    id: 'action.title.upload.new.picture',
    defaultMessage: 'Click to upload new'
  },
  actionSaveChanges: {
    id: 'action.title.profile.modify.save',
    defaultMessage: 'Save changes'
  }
});

class ModifyProfile extends Component {
  state = {
    userId: -1,
    name: { valid: true, value: '' },
    yearOfBirth: { valid: true, value: '' },
    pictureUrl: '',
    gender: '',
    email: { valid: true, value: '' },
    anonymous: '',
    localeLang: ''
  }

  componentDidMount() {
    const { dispatch } = this.props;
    dispatch(fetchProfile());
  }

  componentWillReceiveProps(props) {
    if (!props.profile.isLoading) {
      const profile = props.profile.data;
      // HACK: Replace test from url to make link work
      let pictureUrl = profile.pictures.length > 0 ? profile.pictures[0].url.replace('dev', 'prod') : '';
      this.setState({
        userId: profile.userId,
        name: { value: profile.name, valid: true },
        yearOfBirth: { value: profile.yearOfBirth, valid: true },
        pictureUrl: pictureUrl,
        gender: profile.gender,
        email: { value: profile.email, valid: true },
        anonymous: profile.anonymous.toString(),
        localeLang: profile.localeLang
      });
    }
  }

  getProfileData = () => {
    return {
      userId: this.state.userId,
      name: this.state.name.value,
      yearOfBirth: this.state.yearOfBirth.value,
      gender: this.state.gender,
      email: this.state.email.value,
      anonymous: this.state.anonymous.toString(),
      localeLang: this.state.localeLang
    };
  }

  setName = (event) => {
    let value = event.target.value;
    this.setState({ name: { value: value, valid: !!value }});
  }

  setEmail = (event) => {
    let value = event.target.value;
    this.setState({ email: { value: value, valid: !!value }});
  }

  setYearOfBirth = (event) => {
    let isValid = this.isValidYearOfBirth(event.target.value);
    this.setState({ yearOfBirth: { value: event.target.value, valid: isValid } });
  }

  isValidYearOfBirth(value) {
    return (!value || (value > 1899 && value < 2017));
  }

  setGender = (value) => {
    this.setState({ gender: value });
  }

  setAnonymity = (value) => {
    this.setState({ anonymous: value });
  }

  setLocaleLang = (value) => {
    this.setState({ localeLang: value });
  }

  onModify = () => {
    this.props.updateProfile(this.getProfileData());
  }

  isFormValid = () => {
    return !(!this.props.profile.isUpdating && this.state.name.valid && this.state.email.valid && this.state.yearOfBirth.valid);
  }

  getGenderOptions() {
    const { formatMessage } = this.props.intl;
    return getGenderOptions(formatMessage);
  }

  getAnonymityOptions() {
    const { formatMessage } = this.props.intl;
    return [
      { value: 'false', label: formatMessage(messages.valueAnonymityFalse), tooltip: formatMessage(messages.tooltipAnonymityFalse) },
      { value: 'true', label: formatMessage(messages.valueAnonymityTrue), tooltip: formatMessage(messages.tooltipAnonymityTrue) }
    ];
  }

  getLanguageOptions() {
    const { formatMessage } = this.props.intl;
    return [
      { value: 'en', label: formatMessage(messages.valueLanguageEnglish) },
      { value: 'fi', label: formatMessage(messages.valueLanguageFinnish) },
      { value: 'cs', label: formatMessage(messages.valueLanguageCzech) },
      { value: 'it', label: formatMessage(messages.valueLanguageItalian) }
    ];
  }

  render() {
    const { formatMessage } = this.props.intl;
    const profilePic = this.state.pictureUrl ? this.state.pictureUrl : defaultProfilePic;
    return (
      <div className="ModifyProfile">
        <ViewHeader title={formatMessage(messages.modifyTitle)} />
        <div className="row Content-Area">
          <div className="col s6">
            <div className="row">
              <FormInput name="name" value={this.state.name.value} onChange={this.setName}
                valid={this.state.name.valid} errorMessage={formatMessage(messages.errorNameRequired)}
                label={formatMessage(messages.labelName)} />
              <FormInput name="email" value={this.state.email.value} onChange={this.setEmail}
                valid={this.state.email.valid} errorMessage={formatMessage(messages.errorEmailRequired)}
                label={formatMessage(messages.labelEmail)} type="email" />
              <FormInput name="yearOfBirth" value={this.state.yearOfBirth.value} onChange={this.setYearOfBirth}
                valid={this.state.yearOfBirth.valid} errorMessage={formatMessage(messages.errorYearOfBirthRequired)}
                label={formatMessage(messages.labelYearOfBirth)} type="number" />
              <FormChipsInput name="gender" label={formatMessage(messages.labelGender)} onSelect={this.setGender}
                selected={this.state.gender} options={this.getGenderOptions()} />
              <FormChipsInput name="anonymity" label={formatMessage(messages.labelAnonymity)} onSelect={this.setAnonymity}
                selected={this.state.anonymous} options={this.getAnonymityOptions()} />
              <FormChipsInput name="language" label={formatMessage(messages.labelLanguage)} onSelect={this.setLocaleLang}
                selected={this.state.localeLang} options={this.getLanguageOptions()} />
              <a className={'waves-effect waves-light btn ' + (this.isFormValid() ? 'disabled' : '')}
                onClick={this.onModify}>{formatMessage(messages.actionSaveChanges)}</a>
            </div>
          </div>
          <div className="col s6">
            <label className="active">{formatMessage(messages.labelProfilePic)}</label>
            <div className="ProfilePicContainer z-depth-1">
              <img src={profilePic} alt="Profile" />
              <div className="ProfilePicText">
                {formatMessage(messages.actionUploadNewPicture)}
              </div>
            </div>
          </div>
        </div>
      </div>
    )
  }
}

ModifyProfile.propTypes = {
  dispatch: PropTypes.func.isRequired
};

export default injectIntl(ModifyProfile);
