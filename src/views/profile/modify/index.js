import { connect } from 'react-redux';
import { updateProfile } from '../../../actions';
import ModifyProfile from './ModifyProfile';

const mapStateToProps = (state) => {
  return {
    profile: state.profile,
    isUpdating: state.profile.isUpdating
  };
}

const mapDispatchToProps = (dispatch) => {
  return {
    dispatch,
    updateProfile: (profileData) => {
      dispatch(updateProfile(profileData));
    }
  }
}

const ModifyProfileView = connect(
  mapStateToProps,
  mapDispatchToProps
)(ModifyProfile);

export default ModifyProfileView;
