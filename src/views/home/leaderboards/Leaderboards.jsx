import React, { Component } from 'react';
import { fetchLeaderboardSendsInPastYear,
         fetchLeaderboardSendsInPastMonth,
         fetchLeaderboardSendsInPastWeek } from '../../../actions/leaderboards';

class Leaderboards extends Component {
  componentDidMount() {
    const { dispatch } = this.props;
    dispatch(fetchLeaderboardSendsInPastYear());
    dispatch(fetchLeaderboardSendsInPastMonth());
    dispatch(fetchLeaderboardSendsInPastWeek());
  }

  render() {
    console.log('Render the leaderboards', this.props.leaderboards);
    return (
      <div className="Leaderboards">
        <h3>Leaderboards</h3>
      </div>
    )
  }
}

export default Leaderboards;
