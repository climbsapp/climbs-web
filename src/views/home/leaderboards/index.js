import Leaderboards from './Leaderboards';
import { connect } from 'react-redux';

const mapStateToProps = (state) => {
  return {
    leaderboards: state.leaderboards
  };
}

const LeaderboardsView = connect(mapStateToProps)(Leaderboards);

export default LeaderboardsView;
