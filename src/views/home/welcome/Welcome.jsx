import React, { Component } from 'react';
import { defineMessages, injectIntl } from 'react-intl';
import { ViewHeader } from '../../../components/header/ViewHeader';

const messages = defineMessages({
  welcomeViewHeader: {
    id: 'view.home.welcome.title',
    defaultMessage: 'Welcoming words'
  },
  welcomeViewText: {
    id: 'view.home.welcome.text',
    defaultMessage: 'Dear Climber, welcome to Climbs - the laid back climbing app!'
  },
  climbsForGymsViewHeader: {
    id: 'view.home.welcome.gyms.title',
    defaultMessage: 'Climbs for gyms'
  },
  climbsForGymsViewParagraphOne: {
    id: 'view.home.welcome.gyms.text.p1',
    defaultMessage: 'For gyms and gym owners, Climbs is the tool to success. In its heart there is an easy to use route management tool with mobile app support for route setters. On top of the route management we build all the metrics you need to measure how well you are performing.'
  },
  climbsForGymsViewParagraphTwo: {
    id: 'view.home.welcome.gyms.text.p2',
    defaultMessage: 'You always have up to date and easy to use information on routes you have built, their grade distribution, their age - no more you need to go with gut feeling (or tedious Excel-sheet) on what wall I should change next and what kind of route to replace old ones with. On top of the up to date route information, there is the apps for climbers to track their progress and through this, Climbs works as medium between gym and climbers. Youll see how well your routes cater your crowd. The direct feedback on grades and the route quality help route setters and you to evolve and succeed.'
  },
  climbsForGymsViewParagraphThree: {
    id: 'view.home.welcome.gyms.text.p3',
    defaultMessage: 'To make this very easy decision, all this is free. There is no hidden costs as there is no costs whatsoever. This might change some day, but if we decide to change something, we guarantee you will still go for free at least one year.'
  },
  climbsForClimbersViewHeader: {
    id: 'view.home.welcome.climbers.title',
    defaultMessage: 'Climbs for climbers'
  },
  climbsForClimbersViewParagraphOne: {
    id: 'view.home.welcome.climbers.text.p1',
    defaultMessage: 'For climbers, Climbs is the app to track your performance at all Climbs gyms. Along with data on your training, you\'ll get the opportunity to see how you perform in comparison to your friends.'
  },
  climbsForClimbersViewParagraphTwo: {
    id: 'view.home.welcome.climbers.text.p2',
    defaultMessage: 'With the knowledge we build up on your progress, we identify your strengths and weaknesses and can tailor you professional programs to work on. This is something we are very excited to share with you very soon.'
  },
  climbsForClimbersViewParagraphThree: {
    id: 'view.home.welcome.climbers.text.p3',
    defaultMessage: 'Along this Climbs works as medium between you and your favourite gyms. You get notified when new routes are built and can give back feedback to te route setters in terms of grade and how well they are doing.'
  },
  
});

class WelcomeView extends Component {

  render() {
    const { formatMessage } = this.props.intl;
    return (
      <div className="WelcomeView">
        <ViewHeader title={formatMessage(messages.welcomeViewHeader)} />
        <div className="Content-Area">
          <div className="row">
            <div className="col s12">
              { formatMessage(messages.welcomeViewText) }
            </div>
          </div>
        </div>

        <ViewHeader title={formatMessage(messages.climbsForGymsViewHeader)} />
        <div className="Content-Area">
          <div className="row">
            <div className="col s12">
              <p>{ formatMessage(messages.climbsForGymsViewParagraphOne) }</p>
              <p>{ formatMessage(messages.climbsForGymsViewParagraphTwo) }</p>
              <p>{ formatMessage(messages.climbsForGymsViewParagraphThree) }</p>
            </div>
          </div>
        </div>

        <ViewHeader title={formatMessage(messages.climbsForClimbersViewHeader)} />
        <div className="Content-Area">
          <div className="row">
            <div className="col s12">
              <p>{ formatMessage(messages.climbsForClimbersViewParagraphOne) }</p>
              <p>{ formatMessage(messages.climbsForClimbersViewParagraphTwo) }</p>
              <p>{ formatMessage(messages.climbsForClimbersViewParagraphThree) }</p>
            </div>
          </div>
        </div>
      </div>
    )
  }
}

export default injectIntl(WelcomeView);
