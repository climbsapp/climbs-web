import React, { Component } from 'react';
import { defineMessages, injectIntl } from 'react-intl';
import { SubNav, SubNavItem } from '../../components/navigation/SubNav';
import infoImg from '../../images/icons/information-9e9e9e.svg';

const messages = defineMessages({
  subNavItemWelcomeTitle: {
    id: 'view.home.subnav.welcome',
    defaultMessage: 'Welcome'
  }
});

class HomeRoot extends Component {

  render() {
    const { formatMessage } = this.props.intl;
    return (
      <div className="Dashboard">
        <div className="row">
          <div className="col l2 m3 s4">
            <SubNav>
              <SubNavItem text={ formatMessage(messages.subNavItemWelcomeTitle) } img={infoImg}
                path="home/welcome" location={this.props.location} />
            </SubNav>
          </div>
          <div className="col l9 m9 s8">
            { this.props.children }
          </div>
        </div>
      </div>
    )
  }
}

export default injectIntl(HomeRoot);
