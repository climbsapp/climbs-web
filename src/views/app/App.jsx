import React, { Component } from 'react';
import { connect } from 'react-redux';
import { IntlProvider, addLocaleData } from 'react-intl';
import en from 'react-intl/locale-data/en';
import fi from 'react-intl/locale-data/fi';
import cs from 'react-intl/locale-data/cs';
import it from 'react-intl/locale-data/it';
import { loadLocalization } from '../../actions/i18n';
import { defaultLanguage } from '../../helpers/translate';
import moment from 'moment';

addLocaleData([...en, ...fi, ...cs, ...it]);

class App extends Component {
  componentWillMount() {
    this.props.loadLocalization();
  }

  componentWillReceiveProps(nextProps) {
    moment.locale(nextProps.locale);
  }

  render() {
    const loadedLanguage = this.props.locale;
    return (
      <IntlProvider locale={defaultLanguage()} key={loadedLanguage} messages={this.props.messages}>
        { this.props.children }
      </IntlProvider>
    );
  }
}

const mapStateToProps = (state, props) => {
  return {
    messages: state.i18n.catalog,
    locale: state.i18n.locale
  };
}

const mapDispatchToProps = (dispatch) => {
  return {
    loadLocalization: () => {
      dispatch(loadLocalization(defaultLanguage()));
    }
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(App);
