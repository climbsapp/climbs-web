import React, { Component } from 'react';
import { connect } from 'react-redux';
import { defineMessages, injectIntl } from 'react-intl';
import Navigation from '../../components/navigation/Navigation';
import OverlayDialog from '../../components/modal/OverlayDialog';
import Notifications from '../../views/notifications/Notifications';
import { resetErrorText } from '../../actions/exception';
import { responseAuthenticateFailure } from '../../actions/login';
import { loadLocalization } from '../../actions/i18n';
import { defaultLanguage } from '../../helpers/translate';
import './App.css';

const messages = defineMessages({
  exceptionTitle: {
    id: 'exception.general.title',
    defaultMessage: 'Ops!'
  },
  exceptionContent: {
    id: 'exception.general.content',
    defaultMessage: 'We are sorry, but things went wrong in the system. More specifically: {reason}'
  }
});

class AppLoggedIn extends Component {
  componentWillMount() {
    this.props.loadLocalization();
  }

  renderExceptionOverlay() {
    const { errorText, errorCode, resetErrorTextAndLogout, resetErrorText } = this.props;
    const { formatMessage } = this.props.intl;
    if (!errorText) {
      return undefined;
    } else {
      const title = formatMessage(messages.exceptionTitle);
      const content = formatMessage(messages.exceptionContent, { reason: errorText});
      const onAccept = errorCode === 401 ? resetErrorTextAndLogout : resetErrorText;
      return (
        <OverlayDialog title={title} content={content} onAccept={onAccept} />
      );
    }
  }

  render() {
    const exception = this.renderExceptionOverlay();
    return (
      <div className="App">
        <Navigation />
        <div className="App-content">
          { exception }
          <Notifications />
          {this.props.children}
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state, props) => {
  return {
    errorText: state.exception.errorText,
    errorCode: state.exception.errorCode
  };
}

const mapDispatchToProps = (dispatch) => {
  return {
    resetErrorText: () => {
      dispatch(resetErrorText());
    },
    resetErrorTextAndLogout: () => {
      dispatch(responseAuthenticateFailure());
      dispatch(resetErrorText());
    },
    loadLocalization: () => {
      dispatch(loadLocalization(defaultLanguage()));
    }
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(injectIntl(AppLoggedIn));
