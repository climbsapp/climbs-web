import { Component } from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';
import { fetchProfile } from '../../actions/profile';

class EnsureLoggedInContainer extends Component {
  componentDidUpdate() {
    const { isLoggedIn, isAuthenticating } = this.props;
    if (!isLoggedIn && !isAuthenticating) {
      this.props.router.push('/login');
    }
  }

  componentWillMount() {
    const { isLoggedIn, isAuthenticating } = this.props;
    if (!isLoggedIn && !isAuthenticating) {
      this.props.router.push('/login');
    }
  }

  render() {
    const { isLoggedIn } = this.props;
    if (isLoggedIn) {
      return this.props.children;
    } else {
      return null;
    }
  }
}

const mapStateToProps = (state, props) => {
  const { isAuthenticating, isLoggedIn } = state.profile;
  return {
    isLoggedIn: isLoggedIn,
    isAuthenticating: isAuthenticating
  };
}

const mapDispatchToProps = (dispatch) => {
  return {
    fetchProfile: () => dispatch(fetchProfile())
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(EnsureLoggedInContainer));
