import React, { Component } from 'react';
import { connect } from 'react-redux';
import OverlaySnackbar from '../../components/modal/OverlaySnackbar';

class Notifications extends Component {
  render() {
    const { notifications } = this.props;
    const notificationsList = Object.keys(notifications).map(key => notifications[key]);
    return <OverlaySnackbar content={notificationsList} />;
  }
}

const mapStateToProps = (state, props) => {
  return {
    notifications: state.notifications.notifications
  };
}

export default connect(mapStateToProps)(Notifications);
