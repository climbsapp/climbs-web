import { REQUEST_BOARD, RECEIVE_BOARD } from '../actions/leaderboards';

const initialState = {
  boards: {

  }
};

export function leaderboards(state = initialState, action) {
  let change = {
    boards: {}
  };
  switch (action.type) {
    case REQUEST_BOARD:
      change[action.leaderboardType] = { isLoading: true };
      return Object.assign({}, state, change);
    case RECEIVE_BOARD:
      change[action.leaderboardType] = { isLoading: false, data: action.leaderboardData };
      return Object.assign({}, state, change);
    default:
      return state;
  }
}
