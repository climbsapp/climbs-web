import { RES_LOCALIZATION } from '../actions/i18n';

const initialState = {
  locale: "en",
  catalog: {
  }
};

export function i18n(state = initialState, action) {
  switch(action.type) {
    case RES_LOCALIZATION:
      return Object.assign({}, state, { locale: action.locale, catalog: action.catalog });
    default:
      return state;
    }
}
