import { REQUEST_PROFILE, RECEIVE_PROFILE, UPDATE_PROFILE, FINISH_UPDATE_PROFILE, REQ_CHANGE_PASSWORD, RES_CHANGE_PASSWORD, ERR_CHANGE_PASSWORD } from '../actions';
import { AUTHENTICATE, AUTHENTICATE_FACEBOOK, AUTHENTICATE_SUCCESS, AUTHENTICATE_FAILURE, LOGOUT, REQUEST_PASSWORD, REQUEST_PASSWORD_SUCCESS, REQUEST_PASSWORD_FAILURE } from '../actions/login';
import { RES_EVENT_REGISTER } from '../actions/events';

const initialProfile = {
  isLoading: false,
  isUpdating: false,
  isRequestingPassword: false,
  isAuthenticating: false,
  isLoggedIn: localStorage.getItem('jwtToken') !== null || localStorage.getItem('fbToken') !== null,
  isLoggedInJwt: localStorage.getItem('jwtToken') !== null,
  isLoggedInFacebook: localStorage.getItem('fbToken') !== null,
  data: { email: '', yearOfBirth: '' }
};

export function profile(state = initialProfile, action) {
  switch(action.type) {
    case REQUEST_PASSWORD:
      return Object.assign({}, state, { isRequestingPassword: true });
    case REQUEST_PASSWORD_SUCCESS:
    case REQUEST_PASSWORD_FAILURE:
      return Object.assign({}, state, { isRequestingPassword: false });
    case AUTHENTICATE:
      return Object.assign({}, state, { isAuthenticating: true });
    case AUTHENTICATE_FACEBOOK:
      localStorage.setItem('fbToken', true);
      localStorage.removeItem('jwtToken');
      return Object.assign({}, state, { isAuthenticating: true, isLoggedInJwt: false });
    case AUTHENTICATE_SUCCESS:
      localStorage.setItem('jwtToken', action.data);
      localStorage.removeItem('fbToken');
      return Object.assign({}, state, { isLoggedIn: true, isLoggedInJwt: true, isAuthenticating: false, isLoggedInFacebook: false });
    case AUTHENTICATE_FAILURE:
    case LOGOUT:
      localStorage.removeItem('jwtToken');
      localStorage.removeItem('fbToken');
      return Object.assign({}, state, { isLoggedIn: false, isLoggedInJwt: false, isAuthenticating: false, isLoggedInFacebook: false });
    case REQUEST_PROFILE:
    case REQ_CHANGE_PASSWORD:
      return Object.assign({}, state, { isLoading: true });
    case RECEIVE_PROFILE:
      return Object.assign({}, state, {
        isLoading: false,
        data: mapProfile(action.profile)
      });
    case UPDATE_PROFILE:
      return Object.assign({}, state, {
        isUpdating: true
      });
    case FINISH_UPDATE_PROFILE:
      // TODO: Handle error case if action.success == false
      return Object.assign({}, state, {
        isUpdating: false
      });
    case RES_EVENT_REGISTER:
      return updateProfileFromEventRegistration(state, action.data);
    case RES_CHANGE_PASSWORD:
    case ERR_CHANGE_PASSWORD:
      return Object.assign({}, state, { isLoading: false });
    default:
      return state;
  }
}

/**
 * Upon registering to an event, user gives year of birth and gender info. This
 * information is then automatically saved on user profile at backend. This is why
 * we want to save this information to the user profile here as well.
 * 
 * @param {*} state 
 * @param {*} registration 
 */
function updateProfileFromEventRegistration(state, registration) {
  const data = Object.assign({}, state.data, {
    yearOfBirth: registration.yearOfBirth,
    gender: registration.gender
  });
  return Object.assign({}, state, { data: data });
}

function mapProfile(profile) {
  return Object.assign({}, profile, {
    gender: profile.gender.toUpperCase()
  });
}
