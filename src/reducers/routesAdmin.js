import { REQUEST_LOCATIONS, RECEIVE_LOCATIONS,
         REQUEST_LOCATION, RECEIVE_LOCATION,
         REQUEST_POST_NEW_AREA, RESPONSE_POST_NEW_AREA,
         REQUEST_EDIT_AREA, RESPONSE_EDIT_AREA,
         REQUEST_POST_NEW_WALL, RESPONSE_POST_NEW_WALL,
         REQUEST_EDIT_WALL, RESPONSE_EDIT_WALL,
         REQUEST_DELETE_WALL, RESPONSE_DELETE_WALL,
         REQUEST_POST_NEW_ROUTE, RESPONSE_POST_NEW_ROUTE,
         REQUEST_EDIT_ROUTE, RESPONSE_EDIT_ROUTE,
         REQUEST_DELETE_AREA, RESPONSE_DELETE_AREA,
         REQUEST_UPLOAD_FILE, RESPONSE_UPLOAD_WALL_FILE, RESPONSE_UPLOAD_LOCATION_FILE, FAILURE_UPLOAD_FILE,
         REQUEST_EDIT_LOCATION, RESPONSE_EDIT_LOCATION,
         RESPONSE_MOVE_WALL, RESPONSE_MOVE_AREA,
         RESPONSE_ROUTE_TAGS,
         EDIT_REQUEST_FAILED,
         RESPONSE_DELETE_ROUTE } from '../actions/routesAdmin';
import { REQUEST_POST_LOCATION, RESPONSE_POST_LOCATION } from '../actions/createGym';
import { REQUEST_ADD_ROUTESETTER, RESPONSE_ADD_ROUTESETTER,
         RESPONSE_ADD_ROUTESETTER_FAILED,
         REQUEST_DELETE_ROUTESETTER, RESPONSE_DELETE_ROUTESETTER } from '../actions/adminRouteSetters';
import Immutable from 'seamless-immutable';

const initialState = Immutable({
  isAddingNewWall: false,
  isAddingNewRoute: false,
  isLoadingLocations: false,
  isLoading: false,
  isLoadingLocation: false,
  locations: {},
  areas: {},
  walls: {},
  routes: {},
  routeTypes: {},
  colors: {},
  routeSetters: {},
  gradingSystems: {}
});

export function routesAdmin(state = initialState, action) {
  switch (action.type) {
    case REQUEST_LOCATIONS:
      return Object.assign({}, state, { isLoadingLocations: true });
    case RECEIVE_LOCATIONS:
      return receiveLocations(state, action.data, action.defaultLocationId);
    case REQUEST_LOCATION:
      return Object.assign({}, state, { isLoadingLocation: true });
    case RECEIVE_LOCATION:
      return mapLocationResponse(state, action.locationData);
    case REQUEST_POST_NEW_AREA:
      return Object.assign({}, state, { isAddingNewArea: true });
    case RESPONSE_POST_NEW_AREA:
      return addNewArea(state, action.data);
    case REQUEST_ADD_ROUTESETTER:
    case REQUEST_EDIT_AREA:
    case REQUEST_EDIT_ROUTE:
    case REQUEST_DELETE_ROUTESETTER:
    case REQUEST_UPLOAD_FILE:
    case REQUEST_EDIT_LOCATION:
    case REQUEST_DELETE_WALL:
    case REQUEST_EDIT_WALL:
    case REQUEST_POST_LOCATION:
      return Object.assign({}, state, { isLoading: true });
    case RESPONSE_EDIT_LOCATION:
      return Object.assign({}, editLocation(state, action.data), { isLoading: false });
    case EDIT_REQUEST_FAILED:
    case RESPONSE_ADD_ROUTESETTER_FAILED:
    case FAILURE_UPLOAD_FILE:
      return Object.assign({}, state, { isLoading: false });
    case RESPONSE_EDIT_AREA:
      return editArea(state, action.data);
    case RESPONSE_EDIT_ROUTE:
      return editRoute(state, action.data);
    case REQUEST_POST_NEW_WALL:
      return Object.assign({}, state, { isAddingNewWall: true });
    case RESPONSE_POST_NEW_WALL:
      return Object.assign({}, addNewWall(state, action.data), { isAddingNewWall: false });
    case RESPONSE_EDIT_WALL:
      return editWall(state, action.data);
    case RESPONSE_DELETE_WALL:
      return deleteWall(state, action.areaId, action.wallId);
    case REQUEST_POST_NEW_ROUTE:
      return Object.assign({}, state, { isAddingNewRoute: true });
    case RESPONSE_POST_NEW_ROUTE:
      return Object.assign({}, addNewRoute(state, action.data), { isAddingNewRoute: false });
    case RESPONSE_UPLOAD_WALL_FILE:
      return Object.assign({}, setWallPicture(state, action.wallId, action.pictureData), { isLoading: false });
    case RESPONSE_UPLOAD_LOCATION_FILE:
    return Object.assign({}, setLocationPicture(state, action.locationId, action.pictureData), { isLoading: false });
    case RESPONSE_DELETE_AREA:
      return deleteArea(state, action.locationId, action.areaId);
    case RESPONSE_DELETE_ROUTE:
      return deleteRoute(state, action.wallId, action.routeId)
    case RESPONSE_POST_LOCATION:
      return mapPostLocationResponse(state, action.data);
    case RESPONSE_DELETE_ROUTESETTER:
      return Object.assign({}, deleteRouteSetter(state, action.routeSetterId), { isLoading: false });
    case RESPONSE_ADD_ROUTESETTER:
      return Object.assign({}, addRouteSetter(state, action.data), { isLoading: false });
    case RESPONSE_ROUTE_TAGS:
      return mapRouteTags(state, action.routeId, action.data);
    case RESPONSE_MOVE_WALL:
      return moveWall(state, action.data);
    case RESPONSE_MOVE_AREA:
      return moveArea(state, action.data);
    case REQUEST_DELETE_AREA:
    default:
      return state;
  }
}

function moveArea(state, changedAreas) {
  for (let i = 0; i < changedAreas.length; i++) {
    const area = changedAreas[i];
    state = Immutable.setIn(state, ['areas', area.areaId, 'rowOrder'], area.rowOrder);
  }
  return state;
}

function moveWall(state, changedWalls) {
  for (let i = 0; i < changedWalls.length; i++) {
    const wall = changedWalls[i];
    state = Immutable.setIn(state, ['walls', wall.wallId, 'rowOrder'], wall.rowOrder);
  }
  return state;
}

function editLocation(state, data) {
  const { name, gradingSystem, gradingSystemBoulder, latitude, longitude, active } = mapLocation(data);
  const merged = Immutable.merge(state.locations[data.locationId], { name, gradingSystem, gradingSystemBoulder, latitude, longitude, active });
  return Immutable.setIn(state, ['locations', data.locationId], merged);
}

function mapRouteTags(state, routeId, tagsData) {
  const tags = tagsData.map(tagData => tagData.tagKey);
  return Immutable.setIn(state, ['routes', routeId, 'tags'], tags);
}

function mapPostLocationResponse(state, locationData) {
  const location = mapLocation(locationData);
  return Immutable.setIn(state, ['locations', location.locationId], location);
}

function receiveLocations(state, locationsRaw, defaultLocationId) {
  let locations = {};
  for (let i = 0; i < locationsRaw.length; i++) {
    let locationRaw = locationsRaw[i];
    let newLocation = mapLocation(locationRaw);
    locations[locationRaw.locationId] = newLocation;
  }

  return Immutable.set(state, 'locations', locations);
}

function deleteRouteSetter(state, routeSetterId) {
  return Immutable.set(state, 'routeSetters', Immutable.without(state.routeSetters, ""+routeSetterId));
}

function deleteArea(state, locationId, areaId) {
  return Immutable.updateIn(state, ['locations', locationId, 'areas'], removeValue, areaId);
}

function deleteWall(state, areaId, wallId) {
  return Immutable.updateIn(state, ['areas', areaId, 'walls'], removeValue, wallId);
}

function addRouteSetter(state, routeSetter) {
  return Immutable.setIn(state, ['routeSetters', routeSetter.userId], mapUser(routeSetter.user));
}

function removeIndex(array, index) {
  return array.slice(0, index).concat(array.slice(index+1));
}

function setLocationPicture(state, locationId, pictureData) {
  return Immutable.setIn(state, ['locations', locationId, 'picture'], pictureData);
}

function setWallPicture(state, wallId, pictureData) {
  return Immutable.setIn(state, ['walls', wallId, 'picture'], pictureData);
}

function removeValue(array, value) {
  let index = array.indexOf(value);
  return removeIndex(array, index);
}

function prependValue(array, value) {
  return [value].concat(array);
}

function deleteRoute(state, wallId, routeId) {
  // Remove index from routes
  //const tempState = Immutable.updateIn(state, ['routes'], (routes) => routes.without(routeId));

  // Remove from wall
  return Immutable.updateIn(state, ['walls', wallId, 'routes'], removeValue, routeId);
}

function addNewArea(state, areaData) {
  const area = mapArea(areaData);
  const temp = Immutable.setIn(state, ['areas', area.areaId], area);
  return Immutable.updateIn(temp, ['locations', areaData.locationId, 'areas'], prependValue, area.areaId);
}

function editArea(state, areaData) {
  // Don't use map, as areaData has walls entity which is null always when editing
  // and we dont want it to remove old walls.
  const area = Immutable.merge(state.areas[areaData.areaId], { name: areaData.name, info: areaData.info });
  const nextState = Immutable.setIn(state, ['areas', areaData.areaId], area);
  return Immutable.set(nextState, 'isLoading', false);
}

function editWall(state, wallData) {
  // Don't use map, as wallData has routes entity which is null always when editing
  // and we dont want it to remove old routes.
  const editWall = {
    name: wallData.name, info: wallData.info, angle: wallData.angle,
    length: wallData.length, type: wallData.type, numberOfLines: wallData.numberOfLines,
    gradingSystem: wallData.gradingSystem, internalInfo: wallData.internalInfo,
    typeName: state.routeTypes[wallData.type].name, newRouteInterval: wallData.newRouteInterval
  };
  const wall = Immutable.merge(state.walls[wallData.wallId], editWall);
  const nextState = Immutable.setIn(state, ['walls', wallData.wallId], wall);
  return Immutable.set(nextState, 'isLoading', false);
}

function editRoute(state, routeData) {
  const route = mapRoute(routeData);
  const nextState = Immutable.setIn(state, ['routes', routeData.routeId], route);
  return Immutable.set(nextState, 'isLoading', false);
}

function addNewRoute(state, routeData) {
  const wallId = routeData.wallId;
  const route = mapRoute(routeData);
  const temp = Immutable.setIn(state, ['routes', route.routeId], route);
  return Immutable.updateIn(temp, ['walls', wallId, 'routes'], prependValue, route.routeId);
}

function addNewWall(state, wallData) {
  const areaId = wallData.areaId;
  const wall = mapWall(wallData, state.routeTypes);
  const temp = Immutable.setIn(state, ['walls', wall.wallId], wall);
  return Immutable.updateIn(temp, ['areas', areaId, 'walls'], prependValue, wall.wallId);
}

function resolveGradingSystems(data) {
  let gradingSystems = {};
  for (let i = 0; i < data.gradingSystems.length; i++) {
    gradingSystems[data.gradingSystems[i].name] = data.gradingSystems[i];
  }
  return gradingSystems;
}

function resolveRouteTypes(data) {
  let routeTypes = {};
  for (let i = 0; i < data.routeTypes.length; i++) {
    routeTypes[data.routeTypes[i].routeTypeId] = data.routeTypes[i];
  }
  return routeTypes;
}

function resolveLocationColors(data) {
  let colors = {};
  for (let i = 0; i < data.colors.length; i++) {
    colors[data.colors[i].colorId] = data.colors[i];
  }
  return colors;
}

function resolveLocationRouteSetters(data) {
  let routeSetters = {};
  for (let i = 0; i < data.routeSetters.length; i++) {
    routeSetters[data.routeSetters[i].userId] = mapUser(data.routeSetters[i].user);
  }
  return routeSetters;
}

function mapUser(userRaw) {
  return {
    name: userRaw.name,
    email: userRaw.email,
    id: userRaw.id,
    userId: userRaw.userId,
    pictures: userRaw.pictures,
    createDate: userRaw.createDate,
    isRealUser: !/^routeSetter_[\d-]+_\d+@climbsGeneratedEmail\.co$/.test(userRaw.email)
  };
}

function mapLogo(locationRaw) {
  if (locationRaw.logos && locationRaw.logos.length > 0) {
    return {
      urlSmall: locationRaw.logos[0].urlSmall.replace('.test.', '.prod.')
    };
  }
  return null;
}

function mapLocation(locationRaw) {
  return {
    active: locationRaw.active,
    locationId: locationRaw.locationId,
    name: locationRaw.name,
    gradingSystem: locationRaw.gradingSystem,
    // Boulder grading system might be missing - if so, it is same as normal
    gradingSystemBoulder: locationRaw.gradingSystemBoulder || locationRaw.gradingSystem,
    logo: mapLogo(locationRaw),
    latitude: locationRaw.latitude,
    longitude: locationRaw.longitude,
    locationType: locationRaw.locationType,
    areas: [],
    picture: mapPicture(locationRaw.pictureId, locationRaw.pictures)
  };
}

function mapArea(areaRaw) {
  return {
    locationId: areaRaw.locationId,
    areaId: areaRaw.areaId,
    name: areaRaw.name,
    rowOrder: areaRaw.rowOrder,
    walls: []
  };
}

function mapWall(wallRaw, routeTypes) {
  return {
    active: wallRaw.active,
    wallId: wallRaw.wallId,
    areaId: wallRaw.areaId,
    angle: wallRaw.angle,
    length: wallRaw.length,
    info: wallRaw.info || '',
    internalInfo: wallRaw.internalInfo || '',
    name: wallRaw.name,
    type: wallRaw.type,
    typeName: routeTypes[wallRaw.type].name,
    gradingSystem: wallRaw.gradingSystem,
    numberOfLines: wallRaw.numberOfLines,
    routes: [],
    newRouteInterval: wallRaw.newRouteInterval || 0,
    picture: mapPicture(wallRaw.pictureId, wallRaw.pictures),
    rowOrder: wallRaw.rowOrder
  };
}

function mapRoute(routeRaw) {
  return {
    routeId: routeRaw.routeId,
    name: routeRaw.name,
    grade: routeRaw.grade,
    internalGrade: routeRaw.internalGrade,
    info: routeRaw.info,
    colorId: routeRaw.colorId,
    line: routeRaw.line,
    type: routeRaw.type,
    sendCount: routeRaw.sendCount || 0,
    gradingSystem: routeRaw.gradingSystem,
    createDate: new Date(routeRaw.createDate),
    routeCreated: new Date(routeRaw.routeCreated),
    routeSetterId: routeRaw.routeSetterId,
    routeSetter2Id: routeRaw.routeSetter2Id,
    wallId: routeRaw.wallId,
    communityScore: routeRaw.communityScore,
    tags: routeRaw.tags || []
  };
}

function mapPicture(pictureId, pictures) {
  if (pictures && pictures.length > 0) {
    for (let i = 0; i < pictures.length; i++) {
      let picture = pictures[i];
      if (picture.pictureId === pictureId) {
        return mapPictureData(picture);
      }
    }
  }
  return undefined;
}

function mapPictureData(picture) {
  return {
    urlSmall: picture.urlSmall,
    url: picture.url,
    pictureId: picture.pictureId
  };
}

function mapLocationResponse(state, data) {
  let locationData = data.locations[0];
  const colors = resolveLocationColors(data);
  const routeTypes = resolveRouteTypes(data);
  const routeSetters = resolveLocationRouteSetters(data);
  const gradingSystems = resolveGradingSystems(data);

  let location = mapLocation(locationData);
  let areas = {};
  let walls = {};
  let routes = {};

  for (let i = 0; i < locationData.areas.length; i++) {
    let areaData = locationData.areas[i];
    let area = mapArea(areaData);

    location.areas.push(areaData.areaId);
    areas[areaData.areaId] = area;

    for (let y = 0; y < areaData.walls.length; y++) {
      let wallData = areaData.walls[y];
      let wall = mapWall(wallData, routeTypes);

      area.walls.push(wallData.wallId);
      walls[wallData.wallId] = wall;

      for (let z = 0; z < wallData.routes.length; z++) {
        let routeData = wallData.routes[z];
        let route = mapRoute(routeData);

        wall.routes.push(route.routeId);
        routes[route.routeId] = route;
      }
    }
  }

  const nextState = Immutable.merge(state, {
    areas: areas,
    walls: walls,
    routeTypes: routeTypes,
    colors: colors,
    routes: routes,
    routeSetters: routeSetters,
    gradingSystems: gradingSystems,
    isLoading: false
  });

  return Immutable.setIn(nextState, ['locations', location.locationId], location);
}
