import { REQUEST_ALL_GRADES_DATA, RESPONSE_ALL_GRADES_DATA,
         REQUEST_ROUTESETTERS_DATA, RESPONSE_ROUTESETTERS_DATA,
         REQUEST_ROUTES_BY_STYLE_DATA, RESPONSE_ROUTES_BY_STYLE_DATA,
         REQUEST_ROUTE_SENDS, RESPONSE_ROUTE_SENDS } from '../actions/adminCharts';
import Immutable from 'seamless-immutable';

const initialState = Immutable({
  isLoading: false,
  gradeDistributionChart: {},
  routeSettersByMonthChart: {},
  routesByStyleChart: {},
  routeSendCounts: {}
});

export function adminCharts(state = initialState, action) {
  switch (action.type) {
    case REQUEST_ALL_GRADES_DATA:
    case REQUEST_ROUTESETTERS_DATA:
    case REQUEST_ROUTES_BY_STYLE_DATA:
    case REQUEST_ROUTE_SENDS:
      return Object.assign({}, state, { isLoading: true });
    case RESPONSE_ALL_GRADES_DATA:
      return receiveGradeDistributionChartData(state, action.locationId, action.data);
    case RESPONSE_ROUTESETTERS_DATA:
      return receiveRouteSettersByMonthChartData(state, action.locationId, action.data);
    case RESPONSE_ROUTES_BY_STYLE_DATA:
      return receiveRoutesByStyleChartData(state, action.locationId, action.data);
    case RESPONSE_ROUTE_SENDS:
      return receiveRouteSendCount(state, action.routeId, action.data);
    default:
      return state;
  }
}

function receiveGradeDistributionChartData(state, locationId, data) {
  const temp = Immutable.set(state, 'isLoading', false);
  return Immutable.setIn(temp, ['gradeDistributionChart', locationId], data);
}

function receiveRouteSettersByMonthChartData(state, locationId, data) {
  const temp = Immutable.set(state, 'isLoading', false);
  return Immutable.setIn(temp, ['routeSettersByMonthChart', locationId], data);
}

function receiveRoutesByStyleChartData(state, locationId, data) {
  const temp = Immutable.set(state, 'isLoading', false);
  return Immutable.setIn(temp, ['routesByStyleChart', locationId], data);
}

function receiveRouteSendCount(state, routeId, data) {
  const temp = Immutable.set(state, 'isLoading', false);
  return Immutable.setIn(temp, ['routeSendCounts', routeId], data);
}
