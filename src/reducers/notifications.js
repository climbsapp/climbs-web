/* Notifications reducer */
import Immutable from 'seamless-immutable';
import { ADD_NOTIFICATION, REMOVE_NOTIFICATION } from '../actions/notifications';

const initialState = Immutable({
  notifications: {}
});

export function notifications(state = initialState, action) {
  switch (action.type) {
    case ADD_NOTIFICATION:
      return Immutable.setIn(state, ['notifications', action.id], { id: action.id, text: action.text, options: action.options });
    case REMOVE_NOTIFICATION:
      return Immutable.set(state, 'notifications', Immutable.without(state.notifications, ""+action.id));
    default:
      return state;
  }
}
