/* Exceptions reducer */
import Immutable from 'seamless-immutable';
import { REQUEST_EXCEPTION, RESET_ERROR_TEXT } from '../actions/exception';

const initialState = Immutable({
  errorText: null,
  errorCode: null
});

export function exception(state = initialState, action) {
  switch (action.type) {
    case REQUEST_EXCEPTION:
      return { errorText: action.errorText, errorCode: action.errorCode };
    case RESET_ERROR_TEXT:
      return Immutable.set(state, 'errorText', null);
    default:
      return state;
  }
}
