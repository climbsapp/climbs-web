import Immutable from 'seamless-immutable';
import { REQ_LOAD_EVENTS, RES_LOAD_EVENTS, ERR_LOAD_EVENTS,
         REQ_LOAD_EVENT, RES_LOAD_EVENT, ERR_LOAD_EVENT } from '../actions/events';

const initialState = {
  isLoading: false,
  events: {},
  eventResults: {},
  ongoingEvents: [],
  pastEvents: [],
  futureEvents: []
};

export function events(state = initialState, action) {
  switch (action.type) {
    case REQ_LOAD_EVENTS:
      return Immutable.set(state, 'isLoading', true);
    case RES_LOAD_EVENTS:
      return Immutable.set(receiveEvents(state, action.data), 'isLoading', false);
    case ERR_LOAD_EVENTS:
      return Immutable.set(state, 'isLoading', false);
    case REQ_LOAD_EVENT:
      return Immutable.set(state, 'isLoading', true);
    case RES_LOAD_EVENT:
      return Immutable.set(receiveEvent(state, action.data), 'isLoading', false);
    case ERR_LOAD_EVENT:
      return Immutable.set(state, 'isLoading', false);
    default:
      return state;
  }
}

function receiveEvents(state, data) {
  const events = state.events;
  const futureEvents = [];
  const pastEvents = [];
  const ongoingEvents = [];
  const now = Date.now();
  data.forEach(event => {
    events[event.hashId] = mapEvent(event);
    if (event.startDate <= now && event.endDate >= now) {
      futureEvents.push(event.hashId);
      ongoingEvents.push(event.hashId);
    } else if (event.startDate >= now) {
      futureEvents.push(event.hashId);
    } else {
      pastEvents.push(event.hashId);
    }
  });

  let nextState = Immutable.set(state, 'events', events);
  nextState = Immutable.set(nextState, 'futureEvents', futureEvents);
  nextState = Immutable.set(nextState, 'ongoingEvents', ongoingEvents);
  return Immutable.set(nextState, 'pastEvents', pastEvents);
}

function receiveEvent(state, data) {
  const event = mapEvent(data.event, data.categories);
  return Immutable.setIn(state, ['events', event.hashId], event);
}

function mapEvent(event, categories) {
  return {
    eventId: event.eventId,
    name: event.name,
    locationId: event.locationId,
    numberOfParticipants: event.numberOfParticipants,
    hashId: event.hashId,
    userRegistered: event.userRegistered,
    registrationOpen: event.registrationOpen,
    registrationStart: new Date(event.registrationStart),
    registrationEnd: new Date(event.registrationEnd),
    info: event.info,
    startDate: new Date(event.startDate),
    endDate: new Date(event.endDate),
    categories: mapCategories(categories),
    userRegisteredExtraSeriesId: event.userRegisteredExtraSeriesId,
    extraSeries: (event.extraSeries || []).map(mapExtraSeries)
  };
}

function mapExtraSeries(extraSeries) {
  return {
    extraSeriesId: extraSeries.extraSeriesId,
    name: extraSeries.name
  };
}

function mapCategories(categories) {
  if (!categories) {
    return [];
  }
  return categories;
}
