import { combineReducers } from 'redux';
import { profile } from './profile';
import { leaderboards } from './leaderboards';
import { routesAdmin } from './routesAdmin';
import { adminCharts } from './adminCharts';
import { exception } from './exception';
import { notifications } from './notifications';
import { events } from './events';
import { i18n } from './i18n';

const rootReducer = combineReducers({
  leaderboards,
  profile,
  routesAdmin,
  adminCharts,
  exception,
  notifications,
  events,
  i18n
});

export default rootReducer;
