import React, { Component } from 'react';
import { PropTypes } from 'prop-types';
import ReactDOM from 'react-dom';
import GoogleApiComponent from '../../helpers/GoogleApiComponent';
import './MapInput.css';

class MapInput extends Component {

  onChange = (marker) => {
    const center = marker.getPosition();
    const lat = center.lat();
    const lng = center.lng();
    this.props.onChange(lat, lng);
  }

  render() {
    const style = {
      width: '100%',
      height: '150px'
    };

    return (
      <div style={style} className="MapInput">
        <Map google={this.props.google} onMove={this.onChange} initialCenter={this.props.initialCenter} />
      </div>
    );
  }
}

class Map extends Component {
  constructor(props) {
    super(props);

    const { lat, lng } = this.props.initialCenter;
    this.state = {
      lat: lat,
      lng: lng,
      initialized: (null !== lat && null !== lng)
    };
  }

  componentWillReceiveProps(nextProps) {
    if (!this.state.initialized &&
          nextProps.initialCenter.lat !== this.state.lat) {
      const { lat, lng } = nextProps.initialCenter;
      this.setState(Object.assign({}, this.state, { lat: lat, lng: lng, initialized: true }));
      setTimeout(() => this.recenterMap(), 0);
    }
  }

  componentDidUpdate(prevProps, prevState) {
    if (prevProps.google !== this.props.google) {
      this.loadMap();
    }
  }

  recenterMap() {
    const map = this.map;
    const marker = this.marker;
    const { lat, lng } = this.state;
    const google = this.props.google;

    if (!google) {
      return;
    }
    
    const maps = google.maps;

    if (map) {
        let center = new maps.LatLng(lat, lng);
        map.panTo(center)
        marker.setPosition(center);
    }
  }

  componentDidMount() {
    this.loadMap();
  }

  loadMap() {
    if (this.props && this.props.google) {
      // google is available
      const { google } = this.props;
      const maps = google.maps;
      const mapRef = this.refs.map;
      const node = ReactDOM.findDOMNode(mapRef);
      const { lat, lng } = this.state;
      const center = new maps.LatLng(lat, lng);
      const mapConfig = Object.assign({}, {
        center: center,
        zoom: this.props.zoom,
        zoomControl: true
      });
      this.map = new maps.Map(node, mapConfig);
      this.marker = new maps.Marker({
        position: center,
        map: this.map,
        draggable: true
      });

      this.marker.addListener('drag', (evt) => {
        this.props.onMove(this.marker);
      });

      this.marker.addListener('dragend', (evt) => {
        this.props.onMove(this.marker);
      });

      this.map.addListener('click', (evt) => {
        const markerPosition = new maps.LatLng(evt.latLng.lat(), evt.latLng.lng());
        this.marker.setPosition(markerPosition);
        this.props.onMove(this.marker);
      });

      this.currentPositionControl(this.map, google);
    }
  }

  currentPositionControl = (map, google) => {
    let controlDiv = document.createElement('div');
    let controlUI = document.createElement('div');
    controlUI.className = 'MapsPositionControl';
    controlUI.title = 'Center your location';
    controlDiv.appendChild(controlUI);

    controlUI.addEventListener('click', () => {
      if (navigator.geolocation) {
         navigator.geolocation.getCurrentPosition((position) => {
             let initialLocation = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
             map.setCenter(initialLocation);
         });
      }
    });

    controlDiv.index = 1;
    map.controls[google.maps.ControlPosition.TOP_RIGHT].push(controlDiv);
  }

  render() {
    const style = {
      width: '100%',
      height: '100%'
    };

    return (
      <div ref="map" style={style}>
        Loading the map
      </div>
    );
  }

  static propTypes = {
    google: PropTypes.object,
    zoom: PropTypes.number,
    initialCenter: PropTypes.object,
    onMove: PropTypes.func
  }

  static defaultProps = {
    zoom: 11,
    // San Francisco, by default
    initialCenter: {
      lat: 37.774929,
      lng: -122.419416
    },
    onMove: () => {}
  }
}

export default GoogleApiComponent({})(MapInput);
