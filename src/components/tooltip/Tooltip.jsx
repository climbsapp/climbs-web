import React, { Component } from 'react';
import { PropTypes } from 'prop-types';
import './Tooltip.css';

class Tooltip extends Component {
  render() {
    if (!this.props.text) {
      return null;
    }

    return (
      <div className="Tooltip">
        <div className="Tooltip-Content">
          { this.props.text }
        </div>
        <i className="Arrow" />
      </div>
    );
  }

  static propTypes = {
    text: PropTypes.string
  }
}

export default Tooltip;
