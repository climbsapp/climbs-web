import React, { Component } from 'react';
import { withRouter } from 'react-router';
import closeImg from '../../images/icons/close-white.svg';
import './OverlayForm.css';

class OverlayForm extends Component {

  componentDidMount() {
    document.addEventListener('keydown', this.handleKeyPress);

    // XXX: Prevent background scroll in non-react-way
    document.querySelector('body').className = 'noScroll';
  }

  componentWillUnmount() {
    document.removeEventListener('keydown', this.handleKeyPress);

    // XXX: Enable background scroll in non-react-way
    document.querySelector('body').className = '';
  }

  handleKeyPress = (e) => {
    if (e.keyCode === 27) {
      this.close();
    }
  }

  close = () => {
    this.props.router.goBack();
  }

  render() {
    return (
      <div className="Overlay">
        <div className="Overlay-Content">
          <div className="Overlay-Header">
            <div className="Overlay-Title">
              <img src={this.props.iconImg} alt="" />
              <span className="title">{this.props.title}</span>
              <span className="subtitle">{this.props.subtitle}</span>
            </div>
            <div className="Overlay-Actions">
              <img src={closeImg} alt="Close" onClick={this.close} />
            </div>
          </div>

          <div className="Overlay-Form">
            <div className="Row Form-Container">
              <div className="Col S8 Form">
                {this.props.children}
              </div>
              <div className="Col S4 Help">
                {this.props.help}
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default withRouter(OverlayForm, { withRef: true});
