import React, { Component } from 'react';
import { PropTypes } from 'prop-types';
import { defineMessages, injectIntl } from 'react-intl';
import Button from '../buttons/Button';
import alertImg from '../../images/icons/alert-octagram-ffffff.svg';
import closeImg from '../../images/icons/close-white.svg';
import './OverlayDialog.css';

const messages = defineMessages({
  actionOk: {
    id: 'action.title.ok',
    defaultMessage: 'OK'
  },
  actionCancel: {
    id: 'action.title.cancel',
    defaultMessage: 'Cancel'
  }
});

class OverlayDialog extends Component {
  componentDidMount() {
    document.addEventListener('keydown', this.handleKeyPress);

    // XXX: Prevent background scroll in non-react-way
    document.querySelector('body').className = 'noScroll';
  }

  componentWillUnmount() {
    document.removeEventListener('keydown', this.handleKeyPress);

    // XXX: Enable background scroll in non-react-way
    document.querySelector('body').className = '';
  }

  handleKeyPress = (e) => {
    if (e.keyCode === 27) {
      if (this.props.onCancel) {
        this.props.onCancel();
      } else {
        this.props.onAccept();
      }
    }
  }

  close = () => {
    console.log('Close dialog');
  }

  render() {
    const { formatMessage } = this.props.intl;
    return (
      <div className="Dialog">
        <div className="Dialog-Wrapper ">
          <div className="Dialog-Header">
            <div className="Dialog-Title">
              <img src={alertImg} alt="" />
              <span className="title">{this.props.title}</span>
            </div>
            <div className="Dialog-Header-Actions">
              <img src={closeImg} alt="Close" onClick={this.props.onCancel || this.props.onAccept} />
            </div>
          </div>

          <div className="Dialog-Content">
            { this.props.content }
          </div>

          <div className="Dialog-Actions">
            <Button label={formatMessage(messages.actionOk)} onClick={this.props.onAccept} flat={true} />
            { this.props.onCancel && // Show Cancel only if there is handler for it
              <Button label={formatMessage(messages.actionCancel)} onClick={this.props.onCancel} flat={true} />
            }
          </div>
        </div>
      </div>
    );
  }

  static propTypes = {
    onAccept: PropTypes.func.isRequired,
    onCancel: PropTypes.func,
    title: PropTypes.string.isRequired,
    content: PropTypes.string.isRequired
  }
}

export default injectIntl(OverlayDialog);
