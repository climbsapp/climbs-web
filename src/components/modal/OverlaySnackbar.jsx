import React, { Component } from 'react';
import { PropTypes } from 'prop-types';
import './OverlaySnackbar.css';

class OverlaySnackbar extends Component {
  render() {
    const { content } = this.props;
    const className = 'OverlaySnackbar ' + (content.length > 0 ? 'Active' : 'Inactive') ;
    return (
      <div className={className}>
        { content.map((notification, index) => <OverlayRow notification={notification} key={'snack-' + index} />) }
      </div>
    );
  }

  static propTypes = {
    content: PropTypes.arrayOf(PropTypes.object).isRequired
  }

  static defaultProps = {
    content: []
  }
}

class OverlayRow extends Component {
  renderAction() {
    const { options } = this.props.notification;
    if (options.action) {
      return (
        <div className="SnackAction" onClick={ options.action.onClick }>
          { options.action.title }
        </div>
      );
    } else {
      return undefined;
    }
  }
  
  render() {
    const { text } = this.props.notification;
    return (
      <div className="Snack">
        { text }
        { this.renderAction() }
      </div>
    );
  }
}

export default OverlaySnackbar;
