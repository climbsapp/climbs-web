import React, { Component } from 'react';
import { withRouter } from 'react-router';
import closeImg from '../../images/icons/close-white.svg';
import './OverlayContainer.css';

class OverlayContainer extends Component {

  componentDidMount() {
    document.addEventListener('keydown', this.handleKeyPress);

    // XXX: Prevent background scroll in non-react-way
    document.querySelector('body').className = 'noScroll';
  }

  componentWillUnmount() {
    document.removeEventListener('keydown', this.handleKeyPress);

    // XXX: Enable background scroll in non-react-way
    document.querySelector('body').className = '';
  }

  handleKeyPress = (e) => {
    if (e.keyCode === 27) {
      this.close();
    }
  }

  close = () => {
    this.props.router.goBack();
  }

  render() {
    return (
      <div className="OverlayContainer">
        <div className="OverlayContainer-Content">
          <div className="OverlayContainer-Header">
            <div className="OverlayContainer-Title">
              <img src={this.props.iconImg} alt="" />
              <span className="title">{this.props.title}</span>
              <span className="subtitle">{this.props.subtitle}</span>
            </div>
            <div className="OverlayContainer-Actions">
              <img src={closeImg} alt="Close" onClick={this.close} />
            </div>
          </div>

          <div className="OverlayContainer-Form">
            {this.props.children}
          </div>
        </div>
      </div>
    );
  }
}

export default withRouter(OverlayContainer, { withRef: true});
