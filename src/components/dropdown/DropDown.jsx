import React, { Component } from 'react';
import { PropTypes } from 'prop-types';
import ImageAction from '../action/ImageAction';
import './DropDown.css';

export class DropDown extends Component {
  state = {
    open: false
  }

  toggle = () => {
    this.setState({ open: !this.state.open });
  }

  render() {
    const { open } = this.state;

    return (
      <div className="DropDown">
        <ImageAction type="More" onClick={this.toggle} />
        <div className={`DropDownMenu State-${open}`}>
          { this.props.children }
        </div>
        <div className={`DropDownOverlay State-${open}`} onClick={this.toggle} />
      </div>
    );
  }

  static propTypes = {

  }
}

export class DropDownItem extends Component {
  render() {
    const { type, title, disabled, onClick } = this.props;
    return (
      <div className={"DropDownItem " + type + (disabled ? ' Disabled' : '')}
        onClick={(!disabled ? onClick : () => {})}>
        { title }
      </div>
    );
  }

  static propTypes = {
    type: PropTypes.oneOf(['Edit', 'Delete', 'MoveUp', 'MoveDown']).isRequired,
    title: PropTypes.string.isRequired,
    disabled: PropTypes.bool,
    onClick: PropTypes.func.isRequired
  }
}
