import React, { Component } from 'react';
import './Filters.css';

export class SelectableFilter extends Component {
  render() {
    const idTitle = 'checkbox-' + this.props.title.toLowerCase().replace(' ', '-');
    return (
      <div className="SelectableFilter">
        <input type="checkbox" checked={this.props.checked} onChange={this.props.onChange} id={idTitle} />
        <label htmlFor={idTitle}>{this.props.title}</label>
      </div>
    );
  }
}

export class RadioFilterOption extends Component {
  render() {
    const idTitle = 'radio-' + this.props.title.toLowerCase().replace(' ', '-');
    return (
      <div className="RadioFilterOption">
        <input type="radio" className="with-gap" checked={this.props.checked} onChange={this.props.onChange} id={idTitle} />
        <label htmlFor={idTitle}>{this.props.title}</label>
      </div>
    );
  }
}

export class RadioFilterGroup extends Component {
  renderOptions() {
    return this.props.options.map(option => {
      return <RadioFilterOption title={option.title} checked={false} key={option.key} />
    });
  }

  render() {
    return (
      <div className="Filters">
        { this.renderOptions() }
      </div>
    )
  }
}
