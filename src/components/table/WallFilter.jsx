import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { SelectableFilter } from './Filters';

class WallFilter extends Component {
  state = {
    wallIds: {}
  }

  toggle = (wallId) => {
    return () => {
      this.setState(prevState => {
        const nextState = Object.assign({}, prevState);
        if (prevState.wallIds[wallId] === undefined) {
          nextState.wallIds[wallId] = true;
        } else {
          nextState.wallIds[wallId] = !nextState.wallIds[wallId];
        }
        return nextState;
      }, () => {
        this.props.onChange(this.state.wallIds);
      });
    }
  }

  render() {
    const { wallIds } = this.state;
    const { areas, walls } = this.props;
    const elements = [];
    areas.forEach(area => {
      elements.push(<div className="ListHeading" key={`filter-${area.areaId}`}>{ area.name }</div>);
      const wallsSorted = [...walls].sort((w1, w2) => w1.rowOrder - w2.rowOrder);
      elements.push(
        wallsSorted
          .filter(wall => wall.active === true)
          .filter(wall => wall.areaId === area.areaId)
          .map(wall => <SelectableFilter
                          checked={ wallIds[wall.wallId] === true }
                          title={ wall.name }
                          onChange={ this.toggle(wall.wallId) }
                          key={`filter-${area.areaId}-${wall.wallId}`} />));
    });

    return elements;
  }

  static defaultProps = {
    onChange: () => {}
  }

  static propTypes = {
    areas: PropTypes.arrayOf(
      PropTypes.shape({
        areaId: PropTypes.number.isRequired,
        name: PropTypes.string.isRequired
      })
    ),
    walls: PropTypes.arrayOf(
      PropTypes.shape({
        wallId: PropTypes.number.isRequired,
        name: PropTypes.string.isRequired
      })
    ),
    onChange: PropTypes.func
  }
}

export default WallFilter;
