import React, { Component } from 'react';
import { PropTypes } from 'prop-types';
import Tooltip from '../tooltip/Tooltip.jsx';
import './ChipsInput.css';

class ChipsInput extends Component {

  constructor(props) {
    super(props);

    this.state = {
      selected: props.selected !== undefined ? [props.selected] : [],
      type: props.type ? props.type : 'single',
      showOrder: props.showOrder !== undefined ? props.showOrder : false
    };
  }

  componentWillReceiveProps(props) {
    if (props.selected instanceof Array) {
      this.setState({ selected: [].concat(props.selected) });
    } else {
      this.setState({ selected: [props.selected] });
    }
  }

  onSelect = (index, value) => {
    return () => {
      const { selected, type } = this.state;
      let nextSelected;
      if (type === 'single') {
        this.setState({ selected: [value] });
        nextSelected = value;
      } else if (type === 'duo') {
        const isNew = selected.indexOf(value) === -1;

        // Push value last
        if (isNew) {
          nextSelected = [...selected, value];
        } else if (selected.length > 1) {
          const idx = selected.indexOf(value);
          nextSelected = selected.slice(0,idx).concat(selected.slice(idx+1));
        } else {
          nextSelected = selected;
        }

        // Pop first value out if length is over 2
        if (nextSelected.length > 2) {
          nextSelected = nextSelected.slice(1);
        }
        this.setState({ selected: nextSelected });
      } else if (type === 'optional') {
        if (this.state.selected.indexOf(value) === -1) {
          this.setState({ selected: [value] });
          nextSelected = value;
        } else {
          this.setState({ selected: [] });
          nextSelected = undefined;
        }
      } else {
        // TODO: Make immutable
        if (this.state.selected.indexOf(value) === -1) {
          this.state.selected.splice(0,0,value);
          this.setState({ selected: this.state.selected });
        } else {
          this.state.selected.splice(this.state.selected.indexOf(value),1)
          this.setState({ selected: this.state.selected });
        }
      }

      if (this.props.onSelect) {
        this.props.onSelect(value, nextSelected);
      }
    }
  }

  renderOrderElement(order) {
    if (order === 0 || this.state.showOrder === false) {
      return undefined;
    } else {
      return <div className="ChipOrder">{order}</div>;
    }
  }

  render() {
    let chips = [];
    for (let i = 0; i < this.props.options.length; i++) {
      const option = this.props.options[i];
      const selectedIndex = this.state.selected.indexOf(option.value);
      const className = 'chip' + (selectedIndex > -1 ? ' selected' : '');
      const chip = this.props.options[i];
      const orderElement = this.renderOrderElement(selectedIndex + 1);
      chips.push(
        <div className="ChipContainer" key={'chip-' + chip.value}>
          <div className={className} onClick={this.onSelect(i, chip.value)}>
            {chip.label}
          </div>
          { orderElement }
          <Tooltip text={option.tooltip} />
        </div>
      );
    }

    return (
      <div className="chips">
        {chips}
      </div>
    );
  }

  static propTypes = {
    options: PropTypes.array.isRequired,
    selected: PropTypes.oneOfType([PropTypes.number, PropTypes.string, PropTypes.array, PropTypes.bool]),
    onSelect: PropTypes.func,
    type: PropTypes.string,
    showOrder: PropTypes.bool
  }
}

export default ChipsInput;
