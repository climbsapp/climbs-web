import React, { Component } from 'react';
import { PropTypes } from 'prop-types';
import './ImageAction.css';

export default class ImageAction extends Component {
  render() {
    const { type, onClick, disabled, disabledTooltip, title } = this.props;
    const onClickAction = !disabled ? onClick : () => {};
    return (
      <div className={`Action ${type} ${disabled ? 'Disabled' : ''}`} onClick={onClickAction} title={title}>
        <div className="ActionOpacityOverlay" />
        <div className={`ActionTooltip ${disabled ? '' : 'Disabled'}`}>
          { disabledTooltip }
          <i className="Arrow" />
        </div>
      </div>
    );
  }

  static propTypes = {
    type: PropTypes.oneOf(['Delete', 'Edit', 'More', 'Print']).isRequired,
    onClick: PropTypes.func.isRequired,
    disabled: PropTypes.bool,
    disabledTooltip: PropTypes.string,
    title: PropTypes.string
  }

  static defaultProps = {
    disabled: false,
    disabledTooltip: '',
    title: ''
  }
}
