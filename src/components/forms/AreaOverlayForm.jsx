import React, { Component } from 'react';
import OverlayForm from '../modal/OverlayForm';
import { defineMessages, injectIntl } from 'react-intl';
import { Button } from '../';

const messages = defineMessages({
  nameTitle: {
    id: 'view.mygyms.manage.area.form.name',
    defaultMessage: 'Area name'
  },
  infoTitle: {
    id: 'view.mygyms.manage.area.form.info',
    defaultMessage: 'Area info'
  },
  helpOne: {
    id: 'view.mygyms.manage.area.form.help.one',
    defaultMessage: 'Areas are first level of hierarchy in Climbs. Areas contain walls which then again contain routes.'
  },
  helpTwo: {
    id: 'view.mygyms.manage.area.form.help.two',
    defaultMessage: 'How many areas is good? Well, this depends alot on the size and layout of your gym. One can be enough and it is just ok. To get some idea, if you have walls on multiple stories, each story could be its own area. Also you can isolate a separate bouldering area as one area.'
  },
  helpThree: {
    id: 'view.mygyms.manage.area.form.help.three',
    defaultMessage: 'Info text can for example be used to further describe area whose name might not be exactly self-explanatory.'
  }
});

class AreaOverlayForm extends Component {
  constructor(props) {
    super(props);

    this.state = {
      name: '',
      info: ''
    };
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.data) {
      this.setState(Object.assign({}, this.state, nextProps.data));
    }
  }

  isFormValid() {
    return !!this.state.name;
  }

  close = () => {
    this.props.onClose();
  }

  renderHelp() {
    const { formatMessage } = this.props.intl;
    return (
      <div>
        <p>{ formatMessage(messages.helpOne) }</p>
        <p>{ formatMessage(messages.helpTwo) }</p>
        <p>{ formatMessage(messages.helpThree) }</p>
      </div>
    );
  }

  submit = () => {
    const areaData = {
      name: this.state.name,
      info: this.state.info
    };

    this.props.onSubmit(areaData);
  }

  onChange = (prop) => {
    return (event) => {
      let change = {};
      change[prop] = event.target.value;
      this.setState(change);
    }
  }

  render() {
    const { formatMessage } = this.props.intl;
    const helpElement = this.renderHelp();

    return (
      <OverlayForm title={this.props.title} subtitle={this.props.subtitle}
        iconImg={this.props.iconImg} help={helpElement}>

        <div className="row">
          <div className="input-field col s12">
            <input id="name" type="text" autoComplete="off" onChange={this.onChange('name')} value={this.state.name} />
            <label className="active" htmlFor="name">{ formatMessage(messages.nameTitle) } *</label>
          </div>
        </div>

        <div className="row">
          <div className="input-field col s12">
            <input id="info" type="text" onChange={this.onChange('info')} value={this.state.info} />
            <label className="active" htmlFor="info">{ formatMessage(messages.infoTitle) }</label>
          </div>
        </div>

        <div className="Overlay-Footer">
          <Button disabled={!this.isFormValid()} loading={this.props.loading}
            label={this.props.submitTitle} onClick={this.submit} />
        </div>
      </OverlayForm>
    );
  }
}

export default injectIntl(AreaOverlayForm);
