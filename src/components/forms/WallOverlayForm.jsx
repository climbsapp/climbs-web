import React, { Component } from 'react';
import { PropTypes } from 'prop-types';
import { defineMessages, injectIntl, FormattedMessage } from 'react-intl';
import OverlayForm from '../modal/OverlayForm';
import { Button, ChipsInput } from '../';
import './WallOverlayForm.css';

const messages = defineMessages({
  formName: {
    id: 'view.mygyms.manage.wall.form.name',
    defaultMessage: 'Wall name'
  },
  formInfo: {
    id: 'view.mygyms.manage.wall.form.info',
    defaultMessage: 'Wall info'
  },
  formInternal: {
    id: 'view.mygyms.manage.wall.form.internal',
    defaultMessage: 'Internal note'
  },
  formType: {
    id: 'view.mygyms.manage.wall.form.type',
    defaultMessage: 'Wall type'
  },
  formLines: {
    id: 'view.mygyms.manage.wall.form.lines',
    defaultMessage: 'Amount of lines'
  },
  formHeight: {
    id: 'view.mygyms.manage.wall.form.height',
    defaultMessage: 'Wall height'
  },
  formAngle: {
    id: 'view.mygyms.manage.wall.form.angle',
    defaultMessage: 'Wall angle'
  },
  formChangeInterval: {
    id: 'view.mygyms.manage.wall.form.interval',
    defaultMessage: 'Route change interval'
  },
  valueNotSet: {
    id: 'view.mygyms.manage.wall.form.value.notset',
    defaultMessage: 'Not set'
  },
  valueHeight: {
    id: 'view.mygyms.manage.wall.form.value.height',
    defaultMessage: '{unit, select, metres {{amount, plural, one {# metre} other {# metres} }} feet {{amount, plural, one {# foot} other {# feet} }}}'
  },
  valueAngle: {
    id: 'view.mygyms.manage.wall.form.value.angle',
    defaultMessage: '{amount} degrees'
  },
  valueDays: {
    id: 'view.mygyms.manage.wall.form.value.days',
    defaultMessage: '{amount, plural, one {# day} other {# days}}'
  },
  helpGeneral: {
    id: 'view.mygyms.manage.wall.help.general',
    defaultMessage: 'In the grand scheme walls are contained inside of areas and they themselves contain routes. Walls require only for the name to be set.'
  },
  helpWallInfo: {
    id: 'view.mygyms.manage.wall.help.wallinfo',
    defaultMessage: 'Wall info is a text field for some small extra info. It will be printed out to the printout of wall.'
  },
  helpWallType: {
    id: 'view.mygyms.manage.wall.help.walltype',
    defaultMessage: 'Type of the wall is handy feature that defines the default route type that will be set when adding routes.'
  },
  helpWallLines: {
    id: 'view.mygyms.manage.wall.help.lineamount',
    defaultMessage: 'The amount of lines should be set to the amount of bolt lines on the wall. For boulder walls this oftimes is set to one.'
  },
  typeAutobelay: {
    id: 'view.mygyms.manage.wall.form.value.autobelay',
    defaultMessage: 'Autobelay'
  },
  typeBoulder: {
    id: 'view.mygyms.manage.wall.form.value.boulder',
    defaultMessage: 'Boulder'
  },
  typeLead: {
    id: 'view.mygyms.manage.wall.form.value.lead',
    defaultMessage: 'Lead'
  },
  typeLeadAndToprope: {
    id: 'view.mygyms.manage.wall.form.value.leadAndToprope',
    defaultMessage: 'Lead & Toprope'
  },
  typeToprope: {
    id: 'view.mygyms.manage.wall.form.value.toprope',
    defaultMessage: 'Toprope'
  }
});

class WallOverlayForm extends Component {
  state = {
    name: '',
    info: '',
    internalInfo: '',
    type: 1,
    numberOfLines: 1,
    // eslint-disable-next-line
    angle: new Number(0), // New needed for react intl translation
    length: 5,
    heightMin: 0,
    heightMax: 40,
    heightUnit: 'metres',
    newRouteInterval: 0
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.data) {
      this.setState(Object.assign({}, this.state, nextProps.data));
    }
  }

  setName = (event) => {
    this.setState({ name: event.target.value });
  }

  setInfo = (event) => {
    this.setState({ info: event.target.value });
  }

  setInternalInfo = (event) => {
    this.setState({ internalInfo: event.target.value });
  }

  setAngle = (event) => {
    this.setState({ angle:  event.target.value });
  }

  setHeight = (event) => {
    this.setState({ length: event.target.value });
  }

  setNewRouteInterval = (event) => {
    this.setState({ newRouteInterval: event.target.value });
  }

  setType = (value) => {
    this.setState({ type: value });
  }

  setLineAmount = (value) => {
    this.setState({ numberOfLines: value });
  }

  isFormValid() {
    return !!this.state.name;
  }

  close = () => {
    this.props.onClose();
  }

  toggleHeightUnit = () => {
    if (this.state.heightUnit === 'metres') {
      this.setState({ heightUnit: 'feet', heightMin: 0, heightMax: 132, length: Math.round(this.state.length / 0.3048) });
    } else {
      this.setState({ heightUnit: 'metres', heightMin: 0, heightMax: 40, length: Math.round(this.state.length * 0.3048) });
    }
  }

  renderHelp() {
    const { formatMessage } = this.props.intl;
    return (
      <div>
        <p>{ formatMessage(messages.helpGeneral) }</p>
        <p>{ formatMessage(messages.helpWallInfo) }</p>
        <p>{ formatMessage(messages.helpWallType) }</p>
        <p>{ formatMessage(messages.helpWallLines) }</p>
        <p>
          <FormattedMessage
            id="view.mygyms.manage.wall.help.height"
            defaultMessage="Default unit for height is metres, but you can toggle the unit to feet {link}."
            values={{
              link: (
                <a onClick={this.toggleHeightUnit}>
                  <FormattedMessage
                    id="view.mygyms.manage.wall.help.height.linkText"
                    defaultMessage="by clicking here" />
                </a>
              )
            }} />
        </p>
      </div>
    );
  }

  getRouteTypes() {
    const { formatMessage } = this.props.intl;
    return [
      { value: 3, label: formatMessage(messages.typeAutobelay) },
      { value: 4, label: formatMessage(messages.typeBoulder) },
      { value: 1, label: formatMessage(messages.typeLead) },
      { value: 7, label: formatMessage(messages.typeLeadAndToprope) },
      { value: 2, label: formatMessage(messages.typeToprope) }
    ];
  }

  getLineAmountOptions() {
    return [
      { value: 1,  label: '1' }, { value: 2, label: '2' }, { value: 3, label: '3' },
      { value: 4,  label: '4' }, { value: 5, label: '5' }, { value: 6, label: '6' },
      { value: 7,  label: '7' }, { value: 8, label: '8' }, { value: 9, label: '9' },
      { value: 10, label: '10' }
    ]
  }

  submit = () => {
    const { locationGradingSystem, locationGradingSystemBoulder, areaId } = this.props;
    const gradingSystem = this.state.type !== 4 ? locationGradingSystem : locationGradingSystemBoulder;
    const wallData = {
      active: true,
      angle: this.state.angle,
      areaId: areaId,
      info: this.state.info,
      internalInfo: this.state.internalInfo,
      length: this.state.length,
      name: this.state.name,
      numberOfLines: this.state.numberOfLines,
      type: this.state.type,
      gradingSystem: gradingSystem,
      material: 1,
      newRouteInterval: this.state.newRouteInterval === 0 ? null : this.state.newRouteInterval
    };

    this.props.onSubmit(wallData);
  }

  static propTypes = {
    title: PropTypes.string.isRequired,
    subtitle: PropTypes.string.isRequired,
    submitTitle: PropTypes.string.isRequired,
    areaId: PropTypes.number.isRequired,
    iconImg: PropTypes.string.isRequired,
    locationGradingSystem: PropTypes.string.isRequired,
    locationGradingSystemBoulder: PropTypes.string.isRequired,
    isLoading: PropTypes.bool.isRequired
  }

  render() {
    const { formatMessage } = this.props.intl;
    const helpElement = this.renderHelp();
    const { newRouteInterval } = this.state;
    return (
      <OverlayForm title={this.props.title} subtitle={this.props.subtitle}
        iconImg={this.props.iconImg} help={helpElement}>
        <div className="row">
          <div className="input-field col s12">
            <input id="name" type="text" value={this.state.name} onChange={this.setName} autoComplete="off" />
            <label className="active" htmlFor="name">{ formatMessage(messages.formName) } *</label>
          </div>
        </div>

        <div className="row">
          <div className="input-field col s12">
            <input id="info" type="text" value={this.state.info} onChange={this.setInfo} />
            <label className="active" htmlFor="info">{ formatMessage(messages.formInfo) }</label>
          </div>
        </div>

        <div className="row">
          <div className="input-field col s12">
            <input id="internalInfo" type="text" value={this.state.internalInfo} onChange={this.setInternalInfo} />
            <label className="active" htmlFor="internalInfo">{ formatMessage(messages.formInternal) }</label>
          </div>
        </div>

        <div className="row">
          <div className="input-field col s12">
            <ChipsInput selected={this.state.type} options={this.getRouteTypes()} onSelect={this.setType} />
            <label className="active" htmlFor="type">{ formatMessage(messages.formType) }</label>
          </div>
        </div>

        <div className="row">
          <div className="input-field col s12">
            <ChipsInput selected={this.state.numberOfLines} options={this.getLineAmountOptions()} onSelect={this.setLineAmount} />
            <label className="active" htmlFor="lines">{ formatMessage(messages.formLines) }</label>
          </div>
        </div>

        <div className="row">
          <div className="input-field col s6">
            <div className="range-field">
              <input type="range" id="height" min={this.state.heightMin} max={this.state.heightMax} step="1"
                value={this.state.length} onChange={this.setHeight} />
              <span>{ formatMessage(messages.valueHeight, { amount: this.state.length, unit: this.state.heightUnit }) }</span>
            </div>
            <label className="active" htmlFor="height">{ formatMessage(messages.formHeight) }</label>
          </div>

          <div className="input-field col s6">
            <div className="range-field">
              <input type="range" id="angle" min="-100" max="30" step="5"
                value={this.state.angle} onChange={this.setAngle} />
              <span>{ formatMessage(messages.valueAngle, { amount: this.state.angle }) }</span>
            </div>
            <label className="active" htmlFor="angle">{ formatMessage(messages.formAngle) }</label>
          </div>
        </div>

        <div className="row">
          <div className="input-field col s6">
            <div className="range-field">
              <input type="range" id="newRouteInterval" min={0} max={180} step="1"
                value={newRouteInterval} onChange={this.setNewRouteInterval} />
              <span>{(newRouteInterval === 0 ? formatMessage(messages.valueNotSet) : formatMessage(messages.valueDays, { amount: newRouteInterval }) )}</span>
            </div>
            <label className="active" htmlFor="newRouteInterval">{ formatMessage(messages.formChangeInterval) }</label>
          </div>
        </div>

        <div className="Overlay-Footer">
          <Button disabled={!this.isFormValid()} loading={this.props.isLoading}
            label={this.props.submitTitle} onClick={this.submit} />
        </div>
      </OverlayForm>
    );
  }
}

export default injectIntl(WallOverlayForm);
