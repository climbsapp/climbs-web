import React, { Component } from 'react';
import OverlayForm from '../modal/OverlayForm';
import { defineMessages, injectIntl } from 'react-intl';
import { SingleDatePicker } from 'react-dates';
import moment from 'moment';
import { Button, ChipsInput } from '../';
import { getColorStyle } from '../../helpers/style';
import { messages as commonMessages } from '../../helpers/commonOptions';
import './RouteOverlayForm.css';

const messages = defineMessages({
  labelGradeColor: {
    id: 'view.mygyms.manage.route.form.gradeAndColor',
    defaultMessage: 'Grade and color'
  },
  labelLine: {
    id: 'view.mygyms.manage.route.form.line',
    defaultMessage: 'Line'
  },
  labelRouteSetter: {
    id: 'view.mygyms.manage.route.form.routeSetter',
    defaultMessage: 'Route setter'
  },
  labelRouteStyle: {
    id: 'view.mygyms.manage.route.form.routeStyle',
    defaultMessage: 'Route style'
  },
  labelName: {
    id: 'view.mygyms.manage.route.form.name',
    defaultMessage: 'Route name'
  },
  labelInfo: {
    id: 'view.mygyms.manage.route.form.info',
    defaultMessage: 'Route info'
  },
  labelSetDate: {
    id: 'view.mygyms.manage.route.form.setDate',
    defaultMessage: 'Route set date'
  },
  helpGradeColor: {
    id: 'view.mygyms.manage.route.help.gradeAndColor',
    defaultMessage: 'Route grade and hold color are the most important properties for you to add.'
  },
  helpLine: {
    id: 'view.mygyms.manage.route.help.line',
    defaultMessage: 'The line number describes at which bolt line of the wall the route is situated at. When adding route to walls with only one line (boulder walls) this option is not available.'
  },
  helpRouteSetter: {
    id: 'view.mygyms.manage.route.help.routeSetter',
    defaultMessage: 'Route setters are the persons that have been set as route setters for this location. If somebody is missing, add them at route setters view.'
  },
  helpStyle: {
    id: 'view.mygyms.manage.route.help.routeStyle',
    defaultMessage: 'Style tags are imporant statistics tool for you, so keeping route styles up to date will help you develop your gym to next level!'
  },
  helpName: {
    id: 'view.mygyms.manage.route.help.name',
    defaultMessage: 'Route name and additional info are redundant if you don\'t name your routes. Info is useful for adding something extra for climbers, ie. "arete included".'
  },
  styleArete: {
    id: 'view.mygyms.manage.route.value.style.arete',
    defaultMessage: 'Arete'
  },
  styleCrimps: {
    id: 'view.mygyms.manage.route.value.style.crimps',
    defaultMessage: 'Crimps'
  },
  styleDyno: {
    id: 'view.mygyms.manage.route.value.style.dyno',
    defaultMessage: 'Dyno'
  },
  styleMantle: {
    id: 'view.mygyms.manage.route.value.style.mantle',
    defaultMessage: 'Mantle'
  },
  stylePockets: {
    id: 'view.mygyms.manage.route.value.style.pockets',
    defaultMessage: 'Pockets'
  },
  stylePinches: {
    id: 'view.mygyms.manage.route.value.style.pinches',
    defaultMessage: 'Pinches'
  },
  styleTraverse: {
    id: 'view.mygyms.manage.route.value.style.traverse',
    defaultMessage: 'Traverse'
  },
  styleUnderclings: {
    id: 'view.mygyms.manage.route.value.style.underclings',
    defaultMessage: 'Underclings'
  },
  styleEndurance: {
    id: 'view.mygyms.manage.route.value.style.endurance',
    defaultMessage: 'Endurance'
  },
  stylePower: {
    id: 'view.mygyms.manage.route.value.style.power',
    defaultMessage: 'Power'
  },
  styleBalance: {
    id: 'view.mygyms.manage.route.value.style.balance',
    defaultMessage: 'Technical'
  },
  styleCircuit: {
    id: 'view.mygyms.manage.route.value.style.circuit',
    defaultMessage: 'Circuit'
  },
  styleSlopers: {
    id: 'view.mygyms.manage.route.value.style.slopers',
    defaultMessage: 'Slopers'
  },
  color: {
    id: 'view.mygyms.manage.route.value.color',
    defaultMessage: '{color, select, red {red} green {green} blue {blue} black {black} white {white} yellow {yellow} purple {purple} pink {pink} grey {grey} orange {orange} undefined {only wall} orange_(bright) {bright orange} orange_(beige) {dim orange} other {#} }'
  }
});

class RouteOverlayForm extends Component {
  state = {
    name: '',
    info: '',
    line: 1,
    colorId: -1,
    grade: '',
    gradeIndex: 0,
    routeSetters: [],
    tags: [],
    routeCreated: moment(),
    routeCreatedFocused: false
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.data) {
      const data = nextProps.data;
      const extra = { routeSetters: [data.routeSetterId], routeCreated: moment(data.routeCreated) };
      if (data.routeSetter2Id) {
        extra.routeSetters.push(data.routeSetter2Id);
      }
      this.setState(Object.assign({}, this.state, nextProps.data, extra));
    }
  }

  setName = (event) => {
    this.setState({ name: event.target.value });
  }

  setInfo = (event) => {
    this.setState({ info: event.target.value });
  }

  setType = (value) => {
    this.setState({ type: value });
  }

  setGrade = (event) => {
    const gradeIndex = parseInt(event.target.value, 10);
    this.setState({ grade: this.props.grades[gradeIndex], gradeIndex: gradeIndex });
  }

  stepDownGrade = () => {
    this.setState((prevState, props) => {
      const gradeIndex = Math.max(0, prevState.gradeIndex - 1);
      return {
        grade: props.grades[gradeIndex], gradeIndex: gradeIndex
      };
    });
  }

  stepUpGrade = () => {
    this.setState((prevState, props) => {
      const gradeIndex = Math.min(props.grades.length - 1, prevState.gradeIndex + 1);
      return {
        grade: props.grades[gradeIndex], gradeIndex: gradeIndex
      };
    });
  }

  setLine = (value) => {
    this.setState({ line: value });
  }

  setRouteStyle = (tag) => {
    if (this.state.tags.indexOf(tag) < 0) {
      this.setState({ tags: [tag].concat(this.state.tags) });
    } else {
      this.setState({ tags: this.state.tags.filter(value => value !== tag) });
    }
  }

  isFormValid() {
    return !!this.state.grade;
  }

  close = () => {
    this.props.router.goBack();
  }

  renderHelp() {
    const { formatMessage } = this.props.intl;
    return (
      <div>
        <p>{ formatMessage(messages.helpGradeColor) }</p>
        <p>{ formatMessage(messages.helpLine) }</p>
        <p>{ formatMessage(messages.helpRouteSetter) }</p>
        <p>{ formatMessage(messages.helpStyle) }</p>
        <p>{ formatMessage(messages.helpName) }</p>
      </div>
    );
  }

  setRouteSetter = (routeSetterId, selectedValues) => {
    this.setState({ routeSetters: selectedValues });
  }

  setColor = (colorId) => {
    return () => {
      this.setState({ colorId: colorId });
    };
  }

  setRouteCreated = routeCreated => {
    this.setState({ routeCreated });
  }

  onRouteCreatedFocusChange = ({ focused }) => {
    this.setState({ routeCreatedFocused: focused });
  }

  renderColors() {
    const { formatMessage } = this.props.intl;
    let colorElements = [];
    for (let colorId in this.props.colors) {
      if (this.props.colors.hasOwnProperty(colorId)) {
        const color = this.props.colors[colorId];
        const colorStyle = getColorStyle(color);
        const colorClass = 'ColorInput' + (!color.hex ? ' Transparent' : '');
        const name = formatMessage(messages.color, { color: color.name.replace(' ', '_') });
        colorElements.push(
          <div key={'color-' + colorId} className={colorClass} title={name}
            style={colorStyle} onClick={this.setColor(colorId)} />);
      }
    }
    return colorElements;
  }

  getLineOptions() {
    let lines = [];
    const numberOfLines = this.props.wall ? this.props.wall.numberOfLines : 1;
    for (let i = 0; i < numberOfLines; i++) {
      lines.push({ value: i+1, label: ''+(i+1) });
    }
    return lines;
  }

  getRouteStyleOptions() {
    const { formatMessage } = this.props.intl;
    return [
      { value: 'ROUTE_STYLE_ARETE', label: formatMessage(messages.styleArete) }, 
      { value: 'ROUTE_STYLE_CRIMPS', label: formatMessage(messages.styleCrimps) },
      { value: 'ROUTE_STYLE_DYNO', label: formatMessage(messages.styleDyno) },
      { value: 'ROUTE_STYLE_MANTLE', label: formatMessage(messages.styleMantle) },
      { value: 'ROUTE_STYLE_POCKETS', label: formatMessage(messages.stylePockets) }, 
      { value: 'ROUTE_STYLE_PINCHES', label: formatMessage(messages.stylePinches) },
      { value: 'ROUTE_STYLE_TRAVERSE', label: formatMessage(messages.styleTraverse) },
      { value: 'ROUTE_STYLE_UNDERCLINGS', label: formatMessage(messages.styleUnderclings) },
      { value: 'ROUTE_STYLE_ENDURANCE', label: formatMessage(messages.styleEndurance) },
      { value: 'ROUTE_STYLE_POWER', label: formatMessage(messages.stylePower) },
      { value: 'ROUTE_STYLE_BALANCE', label: formatMessage(messages.styleBalance) },
      { value: 'ROUTE_STYLE_CIRCUIT', label: formatMessage(messages.styleCircuit) },
      { value: 'ROUTE_STYLE_SLOPERS', label: formatMessage(messages.styleSlopers) }
    ];
  }

  getRouteSetterOptions() {
    let routeSetters = [];
    for (let userId in this.props.routeSetters) {
      if (this.props.routeSetters.hasOwnProperty(userId)) {
        let routeSetter = this.props.routeSetters[userId];
        let name = routeSetter.name.indexOf(' ') > 0 ? routeSetter.name.substr(0, routeSetter.name.indexOf(' ') + 2) + '.' : routeSetter.name;
        routeSetters.push({ value: routeSetter.userId, label: name });
      }
    }
    routeSetters.sort((r1, r2) => r1.label.localeCompare(r2.label));
    return routeSetters;
  }

  isMobileSafari() {
    /* https://stackoverflow.com/questions/3007480/determine-if-user-navigated-from-mobile-safari/29696509#29696509 */
    const ua = window.navigator.userAgent;
    const iOS = !!ua.match(/iPad/i) || !!ua.match(/iPhone/i);
    const webkit = !!ua.match(/WebKit/i);
    const iOSSafari = iOS && webkit && !ua.match(/CriOS/i);
    return iOSSafari;
  }

  submit = () => {
    const routeData = {
      active: true,
      info: this.state.info,
      name: this.state.name,
      line: this.state.line,
      colorId: this.state.colorId,
      grade: this.state.grade,
      routeSetterId: this.state.routeSetters[0],
      routeSetter2Id: this.state.routeSetters[1] || null,
      wallId: this.props.wall.wallId,
      gradingSystem: this.props.wall.gradingSystem,
      routeCreated: this.state.routeCreated.format('x'),
      type: this.props.wall.type
    };

    const tagsData = this.state.tags.map(value => {
      return {
        active: true,
        targetName: 'ROUTE',
        tagKey: value
      };
    });

    this.props.onSubmit(routeData, tagsData);
  }

  getGradeBallStyle() {
    if (this.state.colorId < 0) {
      return {
        backgroundColor: '#fff',
        color: '#000'
      };
    } else {
      const color = this.props.colors[this.state.colorId];
      return getColorStyle(color);
    }
  }

  isDayInPast = (date) => {
    return !date.isBefore(moment().add(1, 'day'), "day");
  }

  render() {
    const { formatMessage } = this.props.intl;
    const color = this.props.colors[this.state.colorId];
    const helpElement = this.renderHelp();
    const colorElements = this.renderColors();
    const gradeBallClassName = 'GradeBall' + (!color || !color.hex ? ' Transparent' : '');
    const gradeBallStyle = this.getGradeBallStyle();
    const wallAmountOfLines = this.props.wall ? this.props.wall.numberOfLines : 1;
    const gradeIndex = this.props.grades ? this.props.grades.indexOf(this.state.grade) : -1;
    const mobileSafariClassName = !this.isMobileSafari() ? 'NotMobileSafari' : '';

    return (
      <OverlayForm title={this.props.title} subtitle={this.props.subtitle}
        iconImg={this.props.iconImg} help={helpElement}>

        <div className="Row">
          <div className="Col S2 GradeBallColumn">
            <div className="GradeBallContainer">
              <div className={gradeBallClassName} style={gradeBallStyle}>
                <div className="GradeBallContent">{this.state.grade}</div>
              </div>
            </div>
          </div>
          <div className={"Col S10 " + mobileSafariClassName}>
            <div className="input-field">
              <div className="range-control StepDown" onClick={this.stepDownGrade} />
              <div className="range-field">
                <input type="range" id="grade" min={0} max={this.props.grades.length-1} step="1"
                  value={gradeIndex} onChange={this.setGrade} />
              </div>
              <div className="range-control StepUp" onClick={this.stepUpGrade} />
              <label className="active" htmlFor="grade">{ formatMessage(messages.labelGradeColor) }</label>
            </div>
            <div className="Colors">
              {colorElements}
            </div>
          </div>
        </div>

        <div className={'row' + (wallAmountOfLines === 1 ? ' hidden' : '')}>
          <div className="input-field col s12">
            <ChipsInput selected={this.state.line} options={this.getLineOptions()} onSelect={this.setLine} />
            <label className="active" htmlFor="line">{ formatMessage(messages.labelLine) }</label>
          </div>
        </div>

        <div className="row">
          <div className="input-field col s12">
            <ChipsInput selected={this.state.routeSetters} options={this.getRouteSetterOptions()} onSelect={this.setRouteSetter} type="duo" showOrder={true} />
            <label className="active" htmlFor="routeSetter">{ formatMessage(messages.labelRouteSetter) }</label>
          </div>
        </div>

        <div className="row">
          <div className="input-field col s12">
            <ChipsInput selected={this.state.tags} options={this.getRouteStyleOptions()} onSelect={this.setRouteStyle} type="multi" />
            <label className="active" htmlFor="routeStyle">{ formatMessage(messages.labelRouteStyle) }</label>
          </div>
        </div>

        <div className="row">
          <div className="input-field col s12">
            <input id="name" type="text" value={this.state.name} onChange={this.setName} autoComplete="off" />
            <label className="active" htmlFor="name">{ formatMessage(messages.labelName) }</label>
          </div>
        </div>

        <div className="row">
          <div className="input-field col s12">
            <input id="info" type="text" value={this.state.info} onChange={this.setInfo} />
            <label className="active" htmlFor="info">{ formatMessage(messages.labelInfo) }</label>
          </div>
        </div>

        <div className="row">
          <div className="input-field col s12">
            <SingleDatePicker date={this.state.routeCreated}
              onDateChange={this.setRouteCreated}
              focused={this.state.routeCreatedFocused}
              onFocusChange={this.onRouteCreatedFocusChange} isOutsideRange={this.isDayInPast}
              displayFormat={ formatMessage(commonMessages.dateFormat) } openDirection="up"
              required={true} numberOfMonths={1} hideKeyboardShortcutsPanel={true} />
            <label className="active" htmlFor="setDate">{ formatMessage(messages.labelSetDate) }</label>
          </div>
        </div>

        <div className="Overlay-Footer">
          <Button disabled={!this.isFormValid()} loading={this.props.isLoading}
            label={this.props.submitTitle} onClick={this.submit} />
        </div>
      </OverlayForm>
    );
  }
}

export default injectIntl(RouteOverlayForm);
