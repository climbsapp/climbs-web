import React, { Component } from 'react';
import { PropTypes } from 'prop-types';
import { ChipsInput } from '../';

export class FormInput extends Component {
  renderInput() {
    const { onChange, value, label, name, disabled, type, valid, errorMessage } = this.props;
    const inputClassName = 'validate' + (!valid ? ' invalid' : '');
    const labelAttributes = { 'data-error': errorMessage };
    return (
      <div className="input-field col s12">
        <input disabled={disabled} id={name} type={type} className={inputClassName}
          onChange={ onChange } value={ value } />
        <label {...labelAttributes} className="active" htmlFor={name}>{ label }</label>
      </div>
    );
  }

  render() {
    const { rowWrap } = this.props;
    const inputElement = this.renderInput();
    if (rowWrap) {
      return (<div className="row">{inputElement}</div>);
    } else {
      return inputElement;
    }
  }

  static propTypes = {
    onChange: PropTypes.func,
    value: PropTypes.oneOfType([ PropTypes.string, PropTypes.number ]).isRequired,
    label: PropTypes.string.isRequired,
    name: PropTypes.string.isRequired,
    valid: PropTypes.bool,
    errorMessage: PropTypes.string,
    rowWrap: PropTypes.bool
  }

  static defaultProps = {
    disabled: false,
    type: 'text',
    onChange: () => {},
    valid: true,
    errorMessage: '',
    rowWrap: false
  }
}

export class FormChipsInput extends Component {
  renderInput() {
    const { onSelect, selected, options, label, name, type } = this.props;
    return (
      <div className="input-field col s12">
        <ChipsInput selected={selected} options={options} onSelect={onSelect} type={type} />
        <label className="active" htmlFor={name}>{label}</label>
      </div>
    );
  }

  render() {
    const { rowWrap, shouldRender } = this.props;
    if (!shouldRender) {
      return null;
    }

    const inputElement = this.renderInput();
    if (rowWrap) {
      return (<div className="row">{inputElement}</div>);
    } else {
      return inputElement;
    }
  }

  static defaultProps = {
    onSelect: () => {},
    rowWrap: false,
    type: 'single',
    shouldRender: PropTypes.bool
  }
}

export class FormTextOutput extends Component {
  renderTextOutput() {
    const { name, label, value } = this.props;
    return (
      <div className="input-field col s12">
        <div id={name} className="TextField">
          {value}
        </div>
        <label className="active" htmlFor={name}>{label}</label>
      </div>
    );
  }

  render() {
    const { rowWrap } = this.props;
    const outputElement = this.renderTextOutput();
    if (rowWrap) {
      return (<div className="row">{outputElement}</div>);
    } else {
      return outputElement;
    }
  }

  static propTypes = {
    name: PropTypes.string.isRequired,
    label: PropTypes.string.isRequired,
    value: PropTypes.oneOfType([ PropTypes.string, PropTypes.number ]).isRequired,
    rowWrap: PropTypes.bool
  }

  static defaultProps = {
    rowWrap: false
  }
}

export class FormChipOutput extends Component {
  renderTextOutput() {
    const { name, label, value } = this.props;
    return (
      <div className="input-field col s12">
        <div id={name} className="TextField">
          <div className="chip">{value}</div>
        </div>
        <label className="active" htmlFor={name}>{label}</label>
      </div>
    );
  }

  render() {
    const { rowWrap, shouldRender } = this.props;
    if (!shouldRender) {
      return null;
    }

    const outputElement = this.renderTextOutput();
    if (rowWrap) {
      return (<div className="row">{outputElement}</div>);
    } else {
      return outputElement;
    }
  }

  static propTypes = {
    name: PropTypes.string.isRequired,
    label: PropTypes.string.isRequired,
    value: PropTypes.string.isRequired,
    rowWrap: PropTypes.bool,
    shouldRender: PropTypes.bool
  }

  static defaultProps = {
    rowWrap: false,
    shouldRender: true
  }
}
