import React, { Component } from 'react';
import OverlayForm from '../modal/OverlayForm';

class PictureOverlayForm extends Component {
  render() {
    return (
      <OverlayForm title={this.props.title} subtitle={this.props.subtitle}
        iconImg={this.props.iconImg} help={this.props.helpElement}>
        {this.props.children}
      </OverlayForm>
    );
  }
}

export default PictureOverlayForm;
