import React, { Component } from 'react';
import { defineMessages, injectIntl } from 'react-intl';
import OverlayForm from '../modal/OverlayForm';
import { Button, ChipsInput, MapInput } from '../';

const messages = defineMessages({
  nameTitle: {
    id: 'view.mygyms.manage.location.form.gymName',
    defaultMessage: 'Gym name'
  },
  namePlaceholder: {
    id: 'view.mygyms.manage.location.form.gymName.placeholder',
    defaultMessage: 'e.g. MonkeyBiz'
  },
  gradingSystemTitle: {
    id: 'view.mygyms.manage.location.form.grading.routes',
    defaultMessage: 'Route grading system'
  },
  boulderGradingSystemTitle: {
    id: 'view.mygyms.manage.location.form.grading.boulders',
    defaultMessage: 'Boulder grading system'
  },
  locationTitle: {
    id: 'view.mygyms.manage.location.form.location',
    defaultMessage: 'Gym location'
  },
  nameHelp: {
    id: 'view.mygyms.manage.location.help.name',
    defaultMessage: 'Gym name is something that is shown to climbers using their Climbs apps and is for them and you to recognize the gym. If you operate in multiple locations and use the same gym brand name, include some info about the location in the name.'
  },
  gradingSystemHelp: {
    id: 'view.mygyms.manage.location.help.grading',
    defaultMessage: 'Grading systems are something that will be automatically be set for the walls based on their type when you create them.'
  },
  locationHelp: {
    id: 'view.mygyms.manage.location.help.location',
    defaultMessage: 'Location of the gym helps climbers to locate their closest Climbs gyms and helps them to see where you are located at.'
  },
  gradingTitleFrench: {
    id: 'view.mygyms.manage.location.grading.french',
    defaultMessage: 'French'
  },
  gradingTooltipFrench: {
    id: 'view.mygyms.manage.location.grading.french.tooltip',
    defaultMessage: '6a, 6b,..'
  },
  gradingTitleYDS: {
    id: 'view.mygyms.manage.location.grading.yds',
    defaultMessage: 'YDS'
  },
  gradingTooltipYDS: {
    id: 'view.mygyms.manage.location.grading.yds.tooltip',
    defaultMessage: '5.10a, 5.10b,..'
  },
  gradingTitleBritish: {
    id: 'view.mygyms.manage.location.grading.british',
    defaultMessage: 'British'
  },
  gradingTooltipBritish: {
    id: 'view.mygyms.manage.location.grading.british.tooltip',
    defaultMessage: 'E4, E5..'
  },
  gradingTitleUIAA: {
    id: 'view.mygyms.manage.location.grading.uiaa',
    defaultMessage: 'UIAA'
  },
  gradingTooltipUIAA: {
    id: 'view.mygyms.manage.location.grading.uiaa.tooltip',
    defaultMessage: 'VI, VII,..'
  },
  gradingTitleFont: {
    id: 'view.mygyms.manage.location.grading.font',
    defaultMessage: 'Font'
  },
  gradingTooltipFont: {
    id: 'view.mygyms.manage.location.grading.font.tooltip',
    defaultMessage: '6A, 6B+..'
  },
  gradingTitleHueco: {
    id: 'view.mygyms.manage.location.grading.hueco',
    defaultMessage: 'Hueco'
  },
  gradingTooltipHueco: {
    id: 'view.mygyms.manage.location.grading.hueco.tooltip',
    defaultMessage: 'V5, V6..'
  },
  activeTitle: {
    id: 'view.mygyms.manage.location.active',
    defaultMessage: 'Gym visibility'
  },
  activeTrue: {
    id: 'view.mygyms.manage.location.active.true',
    defaultMessage: 'Visible'
  },
  activeTrueTooltip: {
    id: 'view.mygyms.manage.location.active.true.tooltip',
    defaultMessage: 'Visible to all climbers'
  },
  activeFalse: {
    id: 'view.mygyms.manage.location.active.false',
    defaultMessage: 'Hidden'
  },
  activeFalseTooltip: {
    id: 'view.mygyms.manage.location.active.false.tooltip',
    defaultMessage: 'Only visible to you'
  }
});

class LocationOverlayForm extends Component {
  state = {
    name: '',
    gradingSystem: '',
    gradingSystemBoulder: null,
    latitude: null,
    longitude: null,
    active: null
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.data) {
      this.setState(Object.assign({}, this.state, nextProps.data));
    }
  }

  isFormValid() {
    return !!this.state.name;
  }

  close = () => {
    this.props.onClose();
  }

  renderHelp() {
    const { formatMessage } = this.props.intl;
    return (
      <div>
        <p>{ formatMessage(messages.nameHelp) }</p>
        <p>{ formatMessage(messages.gradingSystemHelp) }</p>
        <p>{ formatMessage(messages.locationHelp) }</p>
      </div>
    );
  }

  submit = () => {
    const locationData = {
      name: this.state.name,
      gradingSystem: this.state.gradingSystem,
      gradingSystemBoulder: this.state.gradingSystemBoulder,
      latitude: this.state.latitude,
      longitude: this.state.longitude
    };

    console.log('submit', this.state.active, null !== this.state.active);
    if (null !== this.state.active) {
      console.log('yes');
      locationData.active = this.state.active;
    }

    console.log('hmm', locationData);

    this.props.onSubmit(locationData);
  }

  onChange = (prop) => {
    return (event) => {
      let change = {};
      change[prop] = event.target.value;
      this.setState(change);
    }
  }

  getRouteGradingSystems = () => {
    const { formatMessage } = this.props.intl;
    let defaultGradingSystems = [
      { value: 'French', label: formatMessage(messages.gradingTitleFrench), tooltip: formatMessage(messages.gradingTooltipFrench) },
      { value: 'YDS USA', label: formatMessage(messages.gradingTitleYDS), tooltip: formatMessage(messages.gradingTooltipYDS) },
      { value: 'British Adj', label: formatMessage(messages.gradingTitleBritish), tooltip: formatMessage(messages.gradingTooltipBritish) },
      { value: 'UIAA', label: formatMessage(messages.gradingTitleUIAA), tooltip: formatMessage(messages.gradingTooltipUIAA) }
    ];

    // Dirty
    if (this.state.gradingSystem === 'French Kiipeilyareena') {
      defaultGradingSystems.unshift({ value: this.state.gradingSystem, label: this.state.gradingSystem });
    }

    return defaultGradingSystems;
  }

  getBoulderGradingSystems = () => {
    const { formatMessage } = this.props.intl;
    let defaultGradingSystems = [
      { value: 'Font', label: formatMessage(messages.gradingTitleFont), tooltip: formatMessage(messages.gradingTooltipFont) },
      { value: 'Hueco (USA)', label: formatMessage(messages.gradingTitleHueco), tooltip: formatMessage(messages.gradingTooltipHueco) }
    ];

    // Dirty
    if (this.state.gradingSystemBoulder === 'French Kiipeilyareena') {
      defaultGradingSystems.unshift({ value: this.state.gradingSystemBoulder, label: this.state.gradingSystemBoulder });
    }

    return defaultGradingSystems;
  }

  getActiveOptions = () => {
    const { formatMessage } = this.props.intl;
    return [
      { value: true, label: formatMessage(messages.activeTrue), tooltip: formatMessage(messages.activeTrueTooltip) },
      { value: false, label: formatMessage(messages.activeFalse), tooltip: formatMessage(messages.activeFalseTooltip) }
    ];
  }

  setGradingSystem = (value) => {
    this.setState({ gradingSystem: value });
  }

  setGradingSystemBoulder = (value) => {
    this.setState({ gradingSystemBoulder: value });
  }

  setActiveValue = (value) => {
    this.setState({ active: value });
  }

  getLocation = () => {
    return {
      lat: this.state.latitude,
      lng: this.state.longitude
    };
  }

  setLocation = (lat, lng) => {
    this.setState({ latitude: lat, longitude: lng });
  }

  render() {
    const { formatMessage } = this.props.intl;
    const helpElement = this.renderHelp();
    const location = (Math.round(this.state.latitude * 10000) / 10000) + ', ' + (Math.round(this.state.longitude * 10000) / 10000);

    return (
      <OverlayForm title={this.props.title} subtitle={this.props.subtitle}
        iconImg={this.props.iconImg} help={helpElement}>

        <div className="row">
          <div className="input-field col s12">
            <input id="location_name" type="text" placeholder={formatMessage(messages.namePlaceholder)}
              value={this.state.name} onChange={this.onChange('name')} />
            <label className="active" htmlFor="location_name">{formatMessage(messages.nameTitle)} *</label>
          </div>
        </div>

        <div className="row">
          <div className="input-field col s12">
            <ChipsInput selected={this.state.gradingSystem}
              options={this.getRouteGradingSystems()} onSelect={this.setGradingSystem} />
            <label className="active">{formatMessage(messages.gradingSystemTitle)}</label>
          </div>
        </div>

        <div className="row">
          <div className="input-field col s12">
            <ChipsInput selected={this.state.gradingSystemBoulder}
              options={this.getBoulderGradingSystems()} onSelect={this.setGradingSystemBoulder} />
            <label className="active">{formatMessage(messages.boulderGradingSystemTitle)}</label>
          </div>
        </div>

        <div className="row">
          <div className="input-field col s12">
            <ChipsInput selected={this.state.active}
              options={this.getActiveOptions()} onSelect={this.setActiveValue} />
            <label className="active">{formatMessage(messages.activeTitle)}</label>
          </div>
        </div>

        <div className="row">
          <div className="input-field col s12">
            <MapInput onChange={this.setLocation} initialCenter={this.getLocation()} />
            <label className="active" htmlFor="location">{formatMessage(messages.locationTitle)} <span className="value">({location})</span></label>
          </div>
        </div>

        <div className="Overlay-Footer">
          <Button disabled={!this.isFormValid()} loading={this.props.loading}
            label={this.props.submitTitle} onClick={this.submit} />
        </div>
      </OverlayForm>
    );
  }
}

export default injectIntl(LocationOverlayForm);
