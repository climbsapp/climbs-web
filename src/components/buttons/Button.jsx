import React, { Component } from 'react';
import { PropTypes } from 'prop-types';
import './Button.css';

class Button extends Component {
  render() {
    let buttonContents;
    if (this.props.loading) {
      buttonContents = (
        <div className="spinner">
          <div className="bounce1"></div>
          <div className="bounce2"></div>
          <div className="bounce3"></div>
        </div>
      );
    }
    const disabled = this.props.disabled || this.props.loading;
    const labelStyle = this.props.loading ? {visibility: 'hidden'} : {};
    const className = 'btn primary' + (disabled ? ' disabled' : '') + (this.props.flat ? ' btn-flat waves-teal' : '');
    return (
      <button className={ className } onClick={this.props.onClick}>
        { buttonContents }
        <span style={ labelStyle }>{ this.props.label }</span>
      </button>
    );
  }

  static defaultProps = {
    loading: false,
    disabled: false,
    onClick: () => {}
  }

  static propTypes = {
    label: PropTypes.string.isRequired,
    loading: PropTypes.bool,
    disabled: PropTypes.bool,
    onClick: PropTypes.func,
    flat: PropTypes.bool
  }
}

export default Button;
