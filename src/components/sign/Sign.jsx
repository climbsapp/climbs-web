import React, { Component } from 'react';
import { PropTypes } from 'prop-types';
import fbLogin from '../../images/fbloginimg.png';
import './Sign.css';

export class Sign extends Component {
  render() {
    const { children, onFacebookLogin } = this.props;
    return (
      <div className="Sign">
        <div className="SignContainer">
            <div className="Cover">
              <div className="CoverContainer">
                <div className="Logo">
                  <div className="TagLine">
                    The laid back climbing tracker
                  </div>
                </div>
                <div className="Footer">
                  The premium climbing tracker for gyms and climbers.
                </div>
              </div>
            </div>
            <div className="Content">
              <div className="Heading">
                <span className="Title">{ this.props.title }</span>
                <span className="HeadingDivider">/</span>
                <span className="Alternative" onClick={this.props.onAlternativeClick}>{ this.props.alternative}</span>
              </div>
              <div className="Form">
                <div className="row">
                  {children}
                </div>
                <div className="FacebookLogin" onClick={onFacebookLogin}>
                  <img src={fbLogin} alt="Login with Facebook" />
                </div>
              </div>
            </div>
          </div>
      </div>
    );
  }

  static propTypes = {
    title: PropTypes.string.isRequired,
    onFacebookLogin: PropTypes.func.isRequired,
    alternative: PropTypes.string.isRequired,
    onAlternativeClick: PropTypes.func.isRequired
  }
}

export class SignFormInput extends Component {
  render() {
    const { name, type, placeholder, value, onChange, error, errorMessage } = this.props;
    const errorClass = error ? 'validate invalid' : '';
    return (
      <div className="col s12 input-field">
        <input type={type} name={name} placeholder={placeholder} value={value} onChange={onChange} className={errorClass} />
        <label htmlFor={name} data-error={errorMessage} />
      </div>
    );
  }
}
