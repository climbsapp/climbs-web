import React, { Component } from 'react';
import { PropTypes } from 'prop-types';
import Tooltip from '../tooltip/Tooltip';
import './ViewHeader.css';

/**
 * View header for all pages.
 */
export class ViewHeader extends Component {
  render() {
    return (
      <div className="Row ViewHeader">
        <div className="Col S12">
          <div className="HeaderText">
            <h3>{this.props.title}</h3>
          </div>
          <div className="ViewHeader-Actions">
            { this.props.children }
          </div>
        </div>
      </div>
    );
  }

  static propTypes = {
    title: PropTypes.string.isRequired
  }
}

export class ViewHeaderAction extends Component {
  render() {
    const { type, disabled, onClick, title } = this.props;
    const tooltipText = this.getTooltipText();
    const actionClass = 'Action ' + type + (disabled ? ' Disabled' : '');
    return (
      <div className={actionClass} onClick={(!disabled ? onClick : undefined)} title={title}>
        <div className="ActionOpacityOverlay" />
        <Tooltip text={tooltipText} />
      </div>
    );
  }

  getTooltipText() {
    if (this.props.disabled && this.props.disabledTooltip) {
      return this.props.disabledTooltip;
    } else {
      return null;
    }
  }

  static propTypes = {
    onClick: PropTypes.func.isRequired,
    type: PropTypes.oneOf(['Delete', 'Edit', 'Image', 'MoveUp', 'MoveDown']).isRequired,
    disabled: PropTypes.bool,
    disabledTooltip: PropTypes.string,
    title: PropTypes.string
  }
}
