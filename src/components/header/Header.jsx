import React, { Component } from 'react';
import './Header.css';

/**
 * Fixed navigation on left or on top. Depending on device width.
 */
class Header extends Component {
  render() {
    return (
      <header className="Climbs-header">
        <div className="Header-logo"></div>
      </header>
    );
  }
}

export default Header;
