import React, { Component } from 'react';
import { withRouter } from 'react-router';
import { Link } from 'react-router';
import './Nav.css';

class Nav extends Component {
  render() {
    const { pathname } = this.props.location;
    const pathStart = this.props.path.split('/')[0];
    const path = pathname.indexOf('/') === 0 ? '/' + pathStart : pathStart;
    const selectedStyle = pathname.indexOf(path) === 0 ? ' selected' : '';
    return (
      <li>
        <Link to={this.props.path} className={"Nav-item " + this.props.icon + selectedStyle }>
          {this.props.title}
        </Link>
      </li>
    );
  }
}

export default withRouter(Nav);
