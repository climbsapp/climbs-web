import React, { Component } from 'react';
import { connect } from 'react-redux';
import { withRouter, Link } from 'react-router';
import { defineMessages, injectIntl } from 'react-intl';
import { fetchLocations } from '../../actions/routesAdmin';
import { fetchProfile } from '../../actions';
import { loadLocalization } from '../../actions/i18n';
import { defaultLanguage, saveLanguage } from '../../helpers/translate';
import Nav from './Nav.jsx';
import ImgNav from './ImgNav';
import profileImg from '../../images/icons/account-ffffff.svg';
import './Navigation.css';

const messages = defineMessages({
  navTitleHome: {
    id: 'view.nav.home.title',
    defaultMessage: 'My Climbs'
  },
  navTitleEvents: {
    id: 'view.nav.events.title',
    defaultMessage: 'EVENTS'
  },
  navTitleProfile: {
    id: 'view.nav.profile.title',
    defaultMessage: 'Profile'
  },
  navTitleAddGym: {
    id: 'view.nav.addgym.title',
    defaultMessage: 'ADD GYM'
  },
  navTitleMyGyms: {
    id: 'view.nav.mygyms.title',
    defaultMessage: 'MY GYMS'
  }
});

class Navigation extends Component {
  componentWillMount() {
    this.props.fetchLocations(this.props.params.locationId);
    this.props.fetchProfile();
  }

  componentWillReceiveProps(nextProps) {
    if (this.props.userLanguage !== nextProps.userLanguage) {
      saveLanguage(nextProps.userLanguage);
      this.props.loadLocalization(nextProps.userLanguage);
    }
  }

  renderMyGymsNav() {
    const { formatMessage } = this.props.intl;
    const title = formatMessage(messages.navTitleMyGyms);
    const paramLocationId = this.props.params.locationId;
    const firstAdministeredLocationId = this.props.administeredLocationIds.find(() => true);
    const locationId = paramLocationId || firstAdministeredLocationId;
    if (locationId !== undefined) {
      return <Nav title={title} icon="homelocation" path={'mygyms/' + locationId} />;
    } else {
      return null;
    }
  }

  renderAddGym() {
    if (this.props.isLoggedIn) {
      const { formatMessage } = this.props.intl;
      const title = formatMessage(messages.navTitleAddGym);
      return <Nav title={title} icon="location" path="newgym" />;
    } else {
      return null;
    }
  }

  renderEvents() {
    if (this.props.isLoggedIn) {
      const { formatMessage } = this.props.intl;
      const title = formatMessage(messages.navTitleEvents);
      return <Nav title={title} icon="calendar" path="events" />;
    } else {
      return null;
    }
  }

  renderProfile() {
    if (this.props.isLoggedIn) {
      const { formatMessage } = this.props.intl;
      const title = formatMessage(messages.navTitleProfile);
      return (
        <ImgNav path="profile" imgSrc={this.props.profileImg} title={title} />
      );
    } else {
      return null;
    }
  }

  logout = () => {
    this.props.logout();
  }

  render() {
    const { formatMessage } = this.props.intl;
    const homeTitle = formatMessage(messages.navTitleHome);
    return (
      <div className="Navigation-wrapper">
        <header className="Climbs-header">
          <Link to="home" title={homeTitle}>
            <div className="Header-logo" />
          </Link>
          <div className="Navigation">
            <ul className="Navigation-list">
              { this.renderEvents() }
              { this.renderAddGym() }
              { this.renderMyGymsNav() }
            </ul>
          </div>
          <div className="SubNavigation">
            <ul className="SubNavigation-list">
              { this.renderProfile() }
            </ul>
          </div>
          {/*
          <div className="Action Logout" onClick={this.logout}>
            <span>{ formatMessage(messages.actionLogout) }</span>
          </div>
           */}
        </header>
      </div>
    );
  }
}

function resolveProfileImg(user, defaultImg) {
  if (user && user.pictures && user.pictures.length > 0) {
    return user.pictures[0].urlSmall.replace('.dev.', '.prod.');
  } else {
    return defaultImg;
  }
}

const mapStateToProps = (state, props) => {
  return {
    isLoggedIn: state.profile.isLoggedIn,
    administeredLocationIds: Object.keys(state.routesAdmin.locations),
    userLanguage: state.profile.data.localeLang || defaultLanguage(),
    profileImg: resolveProfileImg(state.profile.data, profileImg)
  };
}

const mapDispatchToProps = (dispatch) => {
  return {
    fetchLocations: (defaultLocationId) => {
      dispatch(fetchLocations(defaultLocationId));
    },
    fetchProfile: () => {
      dispatch(fetchProfile());
    },
    loadLocalization: (locale) => dispatch(loadLocalization(locale))
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(injectIntl(Navigation)));
