import React, { Component } from 'react';
import { PropTypes } from 'prop-types';
import { withRouter } from 'react-router';
import { Link } from 'react-router';
import './ImgNav.css';

class ImgNav extends Component {
  render() {
    const { path, imgSrc, title } = this.props;
    const style = {
      backgroundImage: 'url(' + imgSrc + ')'
    };
    return (
      <li>
        <Link to={path} className={"Nav-item Img"} style={style} title={title} />
      </li>
    );
  }

  static propTypes = {
    path: PropTypes.string.isRequired,
    title: PropTypes.string.isRequired
  }
}

export default withRouter(ImgNav);
