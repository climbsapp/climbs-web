import React, { Component } from 'react';
import { PropTypes } from 'prop-types';
import { Link } from 'react-router';
import './SubNav.css';

export class SubNav extends Component {
  render() {
    return (
      <ul className="SubNav">
        {this.props.children}
      </ul>
    );
  }
}

export class SubNavSelector extends Component {
  getLogoStyle() {
    if (this.props.logo && this.props.logo.urlSmall) {
      return {
        backgroundImage: 'url(' + this.props.logo.urlSmall + ')'
      };
    } else {
      return {};
    }
  }

  render() {
    const style = this.getLogoStyle();
    const className = 'SubNavSelector' + (this.props.onClick ? ' Selectable' : '');
    const onClick = this.props.onClick || (() => {});
    return (
      <li className={className} onClick={onClick} title={this.props.text}>
        <div className="logo" style={style} />
        <span>{this.props.text}</span>
      </li>
    );
  }

  static propTypes = {
    text: PropTypes.string.isRequired,
    logo: PropTypes.object,
    onClick: PropTypes.func
  }
}

export class SubNavDivider extends Component {
  render() {
    return (
      <li className="SubNavDivider" />
    );
  }
}

export class SubNavHeader extends Component {
  render() {
    return (
      <li className="SubNavHeader">
        { this.props.title }
      </li>
    );
  }

  static propTypes = {
    title: PropTypes.string.isRequired
  }
}

export class SubNavItem extends Component {
  isActive = () => {
    const { location, path } = this.props;
    const index = location.pathname.indexOf(path);
    return index >= 0 &&
      (location.pathname.length === path.length + index ||
        location.pathname.charAt(path.length + index) === '/');
  }

  render() {
    const itemClassName = 'SubNavItem ' + (this.isActive() ? 'active': '');
    return (
      <li className={itemClassName}>
        <Link to={this.props.path}>
          <img src={this.props.img} className="SubNavItem-img" alt="navIcon" />
          <span>{this.props.text}</span>
        </Link>
        { this.renderChildren() }
      </li>
    );
  }

  renderChildren = () => {
    if (this.props.children && this.isActive()) {
      return (
        <ul className="SubNavOptions">
          { this.props.children }
        </ul>
      );
    }
  }

  static propTypes = {
    img: PropTypes.string.isRequired,
    text: PropTypes.string.isRequired
  }
}

export class SubNavAction extends Component {
  render() {
    const itemClassName = 'SubNavItem';
    return (
      <li className={itemClassName} onClick={this.props.onClick}>
        <img src={this.props.img} className="SubNavItem-img" alt="navIcon" />
        <span>{this.props.text}</span>
      </li>
    );
  }

  static propTypes = {
    img: PropTypes.string.isRequired,
    text: PropTypes.string.isRequired,
    onClick: PropTypes.func.isRequired
  }
}
