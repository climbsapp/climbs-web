import fetch from 'isomorphic-fetch';
//import ReactGA from 'react-ga';
import { auth } from '../helpers/fetch';

export const REQ_LOAD_EVENTS = 'REQ_LOAD_EVENTS',
             RES_LOAD_EVENTS = 'RES_LOAD_EVENTS',
             ERR_LOAD_EVENTS = 'ERR_LOAD_EVENTS';

export const REQ_LOAD_EVENT = 'REQ_LOAD_EVENT',
             RES_LOAD_EVENT = 'RES_LOAD_EVENT',
             ERR_LOAD_EVENT = 'ERR_LOAD_EVENT';

export const REQ_EVENT_REGISTER = 'REQ_EVENT_REGISTER',
             RES_EVENT_REGISTER = 'RES_EVENT_REGISTER',
             ERR_EVENT_REGISTER = 'ERR_EVENT_REGISTER';

export const REQ_UNREGISTER = 'REQ_UNREGISTER',
             RES_UNREGISTER = 'RES_UNREGISTER',
             ERR_UNREGISTER = 'ERR_UNREGISTER';

function requestLoadEvents() {
  return {
    type: REQ_LOAD_EVENTS
  };
}

function receiveLoadEvents(data) {
  return {
    type: RES_LOAD_EVENTS,
    data: data
  };
}

function errorLoadEvents(statusText, status) {
  return {
    type: ERR_LOAD_EVENTS,
    statusText: statusText,
    status: status
  };
}

function requestLoadEvent() {
  return {
    type: REQ_LOAD_EVENT
  };
}

function receiveLoadEvent(data) {
  return {
    type: RES_LOAD_EVENT,
    data: data
  };
}

function errorLoadEvent(statusText, status) {
  return {
    type: ERR_LOAD_EVENT,
    statusText: statusText,
    status: status
  };
}

function requestRegisterToEvent(eventId) {
  return {
    type: REQ_EVENT_REGISTER,
    eventId: eventId
  };
}

function successRegisterToEvent(eventId, registrationInfo) {
  return {
    type: RES_EVENT_REGISTER,
    eventId: eventId,
    data: registrationInfo
  };
}

function errorRegisterToEvent(statusText, status) {
  return {
    type: ERR_EVENT_REGISTER,
    statusText: statusText,
    status: status
  };
}

function requestUnregister(eventId) {
  return {
    type: REQ_UNREGISTER,
    eventId: eventId
  };
}

function successUnregister(eventId) {
  return {
    type: RES_UNREGISTER,
    eventId: eventId
  };
}

function errorUnregister(statusText, status) {
  return {
    type: ERR_UNREGISTER,
    statusText: statusText,
    status: status
  };
}

export function loadEvents() {
  return dispatch => {
    dispatch(requestLoadEvents());
    const url = '/private/locationEvents/1'; // XXX: Hard coded location id
    return fetch(url, auth())
      .then(response => {
        if (response.ok) {
          return response.json();
        }
        // eslint-disable-next-line
        throw { statusText: response.statusText, status: response.status };
      })
      .then(json => dispatch(receiveLoadEvents(json)))
      .catch(error => {
        dispatch(errorLoadEvents(error.statusText, error.status));
      });
  };
}

export function loadEvent(eventHash) {
  return dispatch => {
    dispatch(requestLoadEvent());
    const url = '/public/eventResults/' + eventHash;
    return fetch(url, auth())
      .then(response => {
        if (response.ok) {
          return response.json();
        }
        // eslint-disable-next-line
        throw { statusText: response.statusText, status: response.status };
      })
      .then(json => dispatch(receiveLoadEvent(json)))
      .catch(error => {
        dispatch(errorLoadEvent(error.statusText, error.status));
      });
  };
}

export function registerToEvent(eventId, registration, extraSeriesId) {
  return dispatch => {
    dispatch(requestRegisterToEvent(eventId));
    const url = '/private/registerToEvent/' + eventId + (extraSeriesId ? '?extraSeriesId=' + extraSeriesId : '');
    return fetch(url, auth({
        method: 'PUT',
        headers: {
          'Content-Type': 'application/json'
        },
        body: JSON.stringify(registration)
      }))
      .then(response => {
        if (response.ok) {
          // Call to reload the events
          loadEvents()(dispatch);
          return response.text();
        }
        // eslint-disable-next-line
        throw { statusText: response.statusText, status: response.status };
      })
      .then(text => dispatch(successRegisterToEvent(eventId, registration)))
      .catch(error => {
        dispatch(errorRegisterToEvent(error.statusText, error.status));
      });
  };
}

export function unregister(eventId) {
  return dispatch => {
    dispatch(requestUnregister(eventId));
    const url = '/private/eventParticipant/' + eventId;
    return fetch(url, auth({
        method: 'DELETE'
      }))
      .then(response => {
        if (response.ok) {
          // Call to reload the events
          loadEvents()(dispatch);
          return response.text();
        }
        // eslint-disable-next-line
        throw { statusText: response.statusText, status: response.status };
      })
      .then(text => dispatch(successUnregister(eventId)))
      .catch(error => {
        dispatch(errorUnregister(error.statusText, error.status));
      });
  };
}
