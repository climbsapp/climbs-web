import fetch from 'isomorphic-fetch';
import ReactGA from 'react-ga';
import { requestFailed } from './exception';
import { auth } from '../helpers/fetch';
export const REQUEST_DELETE_ROUTESETTER = 'REQUEST_DELETE_ROUTESETTER';
export const RESPONSE_DELETE_ROUTESETTER = 'RESPONSE_DELETE_ROUTESETTER';
export const REQUEST_ADD_ROUTESETTER = 'REQUEST_ADD_ROUTESETTER';
export const RESPONSE_ADD_ROUTESETTER = 'RESPONSE_ADD_ROUTESETTER';
export const RESPONSE_ADD_ROUTESETTER_FAILED = 'RESPONSE_ADD_ROUTESETTER_FAILED';

function requestDeleteArea(locationId, routeSetterId) {
  return {
    type: REQUEST_DELETE_ROUTESETTER,
    locationId: locationId,
    routeSetterId: routeSetterId
  };
}

function onDeleteRouteSetter(locationId, routeSetterId) {
  return {
    type: RESPONSE_DELETE_ROUTESETTER,
    locationId: locationId,
    routeSetterId: routeSetterId
  };
}

function requestAddRouteSetter() {
  return {
    type: REQUEST_ADD_ROUTESETTER
  };
}

function onAddRouteSetter(routeSetterData) {
  return {
    type: RESPONSE_ADD_ROUTESETTER,
    data: routeSetterData
  };
}

function onAddRouteSetterFailed() {
  return {
    type: RESPONSE_ADD_ROUTESETTER_FAILED
  };
}

export function addRouteSetter(routeSetterData) {
  return dispatch => {
    dispatch(requestAddRouteSetter());
    return fetch('/private/routesetter?email=' + encodeURIComponent(routeSetterData.email), auth({
        method: 'POST',
        headers: {
          'Content-Type': 'application/json'
        },
        body: JSON.stringify(routeSetterData)
      }))
      .then(response => {
        if (response.ok) {
          return response.json();
        }

        dispatch(onAddRouteSetterFailed());
        dispatch(requestFailed('Failed to add route setter, it seems they have not yet created Climbs account or given email was wrong.'));
        // eslint-disable-next-line
        throw { statusText: response.statusText, status: response.status };
      })
      .then(json => dispatch(onAddRouteSetter(json)))
      .then(() => {
        ReactGA.event({
          category: 'Admin',
          action: 'Add routesetter by email',
          locationId: routeSetterData.locationId
        });
      });
  };
}

export function addRouteSetterByName(routeSetterData) {
  return dispatch => {
    dispatch(requestAddRouteSetter());
    return fetch('/private/routeSetterWithName?name=' + encodeURIComponent(routeSetterData.name), auth({
        method: 'POST',
        headers: {
          'Content-Type': 'application/json'
        },
        body: JSON.stringify(routeSetterData)
      }))
      .then(response => response.json())
      .then(json => dispatch(onAddRouteSetter(json)))
      .then(() => {
        ReactGA.event({
          category: 'Admin',
          action: 'Add routesetter by name',
          locationId: routeSetterData.locationId
        });
      });
  };
}

export function deleteRouteSetter(locationId, routeSetterId) {
  return dispatch => {
    dispatch(requestDeleteArea(locationId, routeSetterId));
    return fetch('/private/location/' + locationId + '/routesetter/' + routeSetterId, auth({
        method: 'DELETE',
        headers: {
          'Content-Type': 'application/json'
        }
      }))
      .then(response => {
        if (response.ok) {
          dispatch(onDeleteRouteSetter(locationId, routeSetterId))
        }
      })
      .then(() => {
        ReactGA.event({
          category: 'Admin',
          action: 'Delete route setter',
          locationId: locationId
        });
      });
  };
}
