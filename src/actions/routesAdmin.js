import fetch from 'isomorphic-fetch';
import ReactGA from 'react-ga';
import { requestFailed } from './exception';
import { auth } from '../helpers/fetch';

export const REQUEST_LOCATIONS = 'REQUEST_LOCATIONS';
export const RECEIVE_LOCATIONS = 'RECEIVE_LOCATIONS';
export const REQUEST_LOCATION  = 'REQUEST_LOCATION';
export const RECEIVE_LOCATION  = 'RECEIVE_LOCATION';
export const SELECT_AREA_WALL = 'SELECT_AREA_WALL';
export const REQUEST_DELETE_AREA = 'REQUEST_DELETE_AREA',
             RESPONSE_DELETE_AREA = 'RESPONSE_DELETE_AREA';
export const REQUEST_DELETE_WALL = 'REQUEST_DELETE_WALL',
             RESPONSE_DELETE_WALL = 'RESPONSE_DELETE_WALL';
export const REQUEST_POST_NEW_AREA = 'REQUEST_POST_NEW_AREA',
             RESPONSE_POST_NEW_AREA = 'RESPONSE_POST_NEW_AREA';
export const REQUEST_EDIT_AREA = 'REQUEST_EDIT_AREA',
             RESPONSE_EDIT_AREA = 'RESPONSE_EDIT_AREA';
export const REQUEST_POST_NEW_WALL = 'REQUEST_POST_NEW_WALL',
             RESPONSE_POST_NEW_WALL = 'RESPONSE_POST_NEW_WALL';
export const REQUEST_EDIT_WALL = 'REQUEST_EDIT_WALL',
             RESPONSE_EDIT_WALL = 'RESPONSE_EDIT_WALL';
export const REQUEST_POST_NEW_ROUTE = 'REQUEST_POST_NEW_ROUTE',
             RESPONSE_POST_NEW_ROUTE = 'RESPONSE_POST_NEW_ROUTE';
export const RESPONSE_DELETE_ROUTE = 'RESPONSE_DELETE_ROUTE';
export const REQUEST_EDIT_ROUTE = 'REQUEST_EDIT_ROUTE',
             RESPONSE_EDIT_ROUTE = 'RESPONSE_EDIT_ROUTE';
export const REQUEST_UPLOAD_FILE = 'REQUEST_UPLOAD_FILE',
             RESPONSE_UPLOAD_WALL_FILE = 'RESPONSE_UPLOAD_WALL_FILE',
             RESPONSE_UPLOAD_LOCATION_FILE = 'RESPONSE_UPLOAD_LOCATION_FILE',
             FAILURE_UPLOAD_FILE = 'FAILURE_UPLOAD_FILE';
export const REQUEST_ROUTE_TAGS = 'REQUEST_ROUTE_TAGS',
             RESPONSE_ROUTE_TAGS = 'RESPONSE_ROUTE_TAGS';
export const EDIT_REQUEST_FAILED = 'EDIT_REQUEST_FAILED';
export const REQUEST_EDIT_LOCATION = 'REQUEST_EDIT_LOCATION',
             RESPONSE_EDIT_LOCATION = 'RESPONSE_EDIT_LOCATION';
export const REQUEST_MOVE_WALL_UP = 'REQUEST_MOVE_WALL_UP',
             RESPONSE_MOVE_WALL_UP = 'RESPONSE_MOVE_WALL_UP';
export const REQUEST_MOVE_WALL = 'REQUEST_MOVE_WALL',
             RESPONSE_MOVE_WALL = 'RESPONSE_MOVE_WALL';
export const REQUEST_MOVE_AREA = 'REQUEST_MOVE_AREA',
             RESPONSE_MOVE_AREA = 'RESPONSE_MOVE_AREA';

function requestEditLocation() {
  return {
    type: REQUEST_EDIT_LOCATION
  };
}

function responseEditLocation(locationData) {
  return {
    type: RESPONSE_EDIT_LOCATION,
    data: locationData
  };
}

export function requestLocations() {
  return {
    type: REQUEST_LOCATIONS
  };
}

export function receiveLocations(locations, defaultLocationId) {
  return {
    type: RECEIVE_LOCATIONS,
    data: locations,
    defaultLocationId: defaultLocationId
  };
}

export function requestLocation(locationId) {
  return {
    type: REQUEST_LOCATION,
    locationId: locationId
  };
}

export function receiveLocation(locationId, locationData) {
  return {
    type: RECEIVE_LOCATION,
    locationId: locationId,
    locationData: locationData
  };
}

export function selectAreaWall(areaId, wallId) {
  return {
    type: SELECT_AREA_WALL,
    areaId: areaId,
    wallId: wallId
  };
}

export function putEditArea(data) {
  return {
    type: REQUEST_EDIT_AREA,
    data: data
  };
}

export function putEditAreaSuccess(data) {
  return {
    type: RESPONSE_EDIT_AREA,
    data: data
  };
}

export function putEditWall(data) {
  return {
    type: REQUEST_EDIT_WALL,
    data: data
  };
}

export function putEditWallSuccess(data) {
  return {
    type: RESPONSE_EDIT_WALL,
    data: data
  };
}

export function putEditRoute() {
  return {
    type: REQUEST_EDIT_ROUTE
  };
}

export function putEditRouteSuccess(routeData, tagsData) {
  return {
    type: RESPONSE_EDIT_ROUTE,
    data: routeData,
    tags: tagsData
  };
}

export function editRequestFailed() {
  return {
    type: EDIT_REQUEST_FAILED
  };
}

export function postNewArea(data) {
  return {
    type: REQUEST_POST_NEW_AREA,
    data: data
  };
}

export function postNewAreaSuccess(data) {
  return {
    type: RESPONSE_POST_NEW_AREA,
    data: data
  };
}

export function postNewWall(data) {
  return {
    type: REQUEST_POST_NEW_WALL,
    data: data
  };
}

export function postNewWallSuccess(data) {
  return {
    type: RESPONSE_POST_NEW_WALL,
    data: data
  };
}

export function postNewRoute() {
  return {
    type: REQUEST_POST_NEW_ROUTE
  };
}

export function postNewRouteSuccess(data) {
  return {
    type: RESPONSE_POST_NEW_ROUTE,
    data: data
  };
}

export function requestDeleteArea(locationId, areaId) {
  return {
    type: REQUEST_DELETE_AREA,
    locationId: locationId,
    areaId: areaId
  };
}

export function deleteAreaOnSuccess(locationId, areaId) {
  return {
    type: RESPONSE_DELETE_AREA,
    locationId: locationId,
    areaId: areaId
  };
}

export function requestDeleteWall() {
  return {
    type: REQUEST_DELETE_WALL
  };
}

export function deleteWallOnSuccess(areaId, wallId) {
  return {
    type: RESPONSE_DELETE_WALL,
    areaId: areaId,
    wallId: wallId
  };
}

export function deleteRouteOnSuccess(wallId, routeId) {
  return {
    type: RESPONSE_DELETE_ROUTE,
    wallId: wallId,
    routeId: routeId
  };
}

function requestUploadFile(formData) {
  return {
    type: REQUEST_UPLOAD_FILE,
    data: formData
  };
}

function responseUploadWallPicture(wallId, pictureData) {
  return {
    type: RESPONSE_UPLOAD_WALL_FILE,
    wallId: wallId,
    pictureData: pictureData
  };
}

function failureUploadWallPicture(wallId) {
  return {
    type: FAILURE_UPLOAD_FILE,
    wallId: wallId
  };
}

function responseUploadLocationPicture(locationId, pictureData) {
  return {
    type: RESPONSE_UPLOAD_LOCATION_FILE,
    locationId: locationId,
    pictureData: pictureData
  };
}

function failureUploadLocationPicture(locationId) {
  return {
    type: FAILURE_UPLOAD_FILE,
    locationId: locationId
  };
}

function requestRouteTags() {
  return {
    type: REQUEST_ROUTE_TAGS
  };
}

function getRouteTagsOnSuccess(routeId, tagsData) {
  return {
    type: RESPONSE_ROUTE_TAGS,
    routeId: routeId,
    data: tagsData
  };
}

function moveWallSuccess(data) {
  return {
    type: RESPONSE_MOVE_WALL,
    data: data
  };
}

function moveAreaSuccess(data) {
  return {
    type: RESPONSE_MOVE_AREA,
    data: data
  };
}

export function fetchLocation(locationId) {
  return dispatch => {
    // Dispatch action that we are requesting a location
    dispatch(requestLocation(locationId));

    let url = '/private/locationRoutes/' + locationId + '?showEmptyWalls=1&includeHidden=1';

    // Request the location
    return fetch(url, auth())
      .then(response => {
        if (response.ok) {
          return response.json();
        }
        // eslint-disable-next-line
        throw { statusText: response.statusText, status: response.status };
      })
      .then(json => dispatch(receiveLocation(locationId, json)))
      .catch(error => {
        dispatch(requestFailed(error.statusText, error.status));
      });
  };
}

export function fetchLocations(defaultLocationId) {
  return dispatch => {
    // Dispatch action that we are requesting locations
    dispatch(requestLocations());

    return fetch('/private/adminLocations', auth())
      .then(response => {
        if (response.ok) {
          return response.json();
        }
        // eslint-disable-next-line
        throw { statusText: response.statusText, status: response.status };
      })
      .then(json => dispatch(receiveLocations(json, defaultLocationId)))
      .catch(error => {
        dispatch(requestFailed(error.statusText, error.status));
      });
  };
}

export function addNewArea(areaData, locationId) {
  return dispatch => {
    dispatch(postNewArea(areaData));
    return fetch('/private/area', auth({
        method: 'POST',
        headers: {
          'Content-Type': 'application/json'
        },
        body: JSON.stringify(areaData)
      }))
      .then(response => response.json())
      .then(json => dispatch(postNewAreaSuccess(json)))
      .then(() => {
        ReactGA.event({
          category: 'Admin',
          action: 'Created area',
          locationId: locationId
        });
      });
  };
}

export function editArea(areaData, locationId) {
  return dispatch => {
    dispatch(putEditArea(areaData));
    return fetch('/private/area', auth({
        method: 'PUT',
        headers: {
          'Content-Type': 'application/json'
        },
        body: JSON.stringify(areaData)
      }))
      .then(response => response.json())
      .then(json => dispatch(putEditAreaSuccess(json)))
      .then(() => {
        ReactGA.event({
          category: 'Admin',
          action: 'Modified area',
          locationId: locationId
        });
      });
  };
}

export function editWall(wallData, locationId) {
  return dispatch => {
    dispatch(putEditWall(wallData));
    return fetch('/private/wall', auth({
        method: 'PUT',
        headers: {
          'Content-Type': 'application/json'
        },
        body: JSON.stringify(wallData)
      }))
      .then(response => response.json())
      .then(json => dispatch(putEditWallSuccess(json)))
      .then(() => {
        ReactGA.event({
          category: 'Admin',
          action: 'Modified wall',
          locationId: locationId
        });
      });
  };
}

export function addNewWall(wallData, locationId) {
  return dispatch => {
    dispatch(postNewWall(wallData));
    return fetch('/private/wall', auth({
        method: 'POST',
        headers: {
          'Content-Type': 'application/json'
        },
        body: JSON.stringify(wallData)
      }))
      .then(response => response.json())
      .then(json => dispatch(postNewWallSuccess(json)))
      .then(() => {
        ReactGA.event({
          category: 'Admin',
          action: 'Created wall',
          locationId: locationId
        });
      });
  };
}

export function editRoute(routeData, locationId) {
  return dispatch => {
    dispatch(putEditRoute());
    return fetch('/private/route', auth({
      method: 'PUT',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(routeData)
    }))
    .then(response => {
      if (response.ok) {
        return response.json();
      }
      // eslint-disable-next-line
      throw { statusText: response.statusText, status: response.status };
    })
    .then(json => dispatch(putEditRouteSuccess(json)))
    .then(() => {
      ReactGA.event({
        category: 'Admin',
        action: 'Modified route',
        locationId: locationId
      });
    })
    .catch(error => {
      dispatch(editRequestFailed(error.statusText, error.status));
      throw error;
    })
  };
}

export function editLocation(locationData) {
  return dispatch => {
    dispatch(requestEditLocation());
    return fetch('/private/location', auth({
      method: 'PUT',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(locationData)
    }))
    .then(response => {
      if (response.ok) {
        return response.json();
      }
      // eslint-disable-next-line
      throw { statusText: response.statusText, status: response.status };
    })
    .then(json => dispatch(responseEditLocation(json)))
    .then(() => {
      ReactGA.event({
        category: 'Admin',
        action: 'Modified location',
        locationId: locationData.locationId
      });
    })
    .catch(error => {
      console.log('Error editing location', error);
      dispatch(requestFailed(error.statusText, error.status));
    })
  };
}

export function addNewRoute(routeData, tagsData, locationId) {
  return dispatch => {
    dispatch(postNewRoute());
    return new Promise((resolve, reject) => {
      fetch('/private/route', auth({
        method: 'POST',
        headers: {
          'Content-Type': 'application/json'
        },
        body: JSON.stringify(routeData)
      }))
      .then(response => {
        if (response.ok) {
          resolve(response.json());
        } else {
          reject(response);
        }
      })
    })
    /* Add tags after route was added */
    .then((routeData) => {
      // Add the target id after we know it from the route data.
      tagsData.forEach(tag => {
        tag.targetId = routeData.routeId
      });
      fetch('/private/tags', auth({
        method: 'PUT',
        headers: {
          'Content-Type': 'application/json'
        },
        body: JSON.stringify(tagsData)
      }))
      .then(response => response.json())
      .then(json => dispatch(postNewRouteSuccess(routeData, tagsData)))
    })
    .then(() => {
      ReactGA.event({
        category: 'Admin',
        action: 'Created route',
        locationId: locationId
      });
    })
    .catch((error) => {
      console.log('Error adding route!', error);
      throw error;
    });
  };
}

export function uploadLocationPicture(locationId, formData) {
  return dispatch => {
    dispatch(requestUploadFile(formData));
    return fetch('/private/upload/' + locationId, auth({
        method: 'POST',
        body: formData
      }))
      .then(response => {
        if (response.ok) {
          return response.json();
        }
        dispatch(failureUploadLocationPicture(locationId));
        // eslint-disable-next-line
        throw { statusText: response.statusText, status: response.status };
      })
      .then(json => dispatch(responseUploadLocationPicture(locationId, json)))
      .then(() => {
        ReactGA.event({
          category: 'Admin',
          action: 'Added location header picture',
          locationId: locationId
        });
      });
  };
}

export function uploadWallPicture(wallId, formData, locationId) {
  return dispatch => {
    dispatch(requestUploadFile(formData));
    return fetch('/private/upload/' + wallId, auth({
        method: 'POST',
        body: formData
      }))
      .then(response => {
        if (response.ok) {
          return response.json();
        }
        dispatch(failureUploadWallPicture(wallId));
        // eslint-disable-next-line
        throw { statusText: response.statusText, status: response.status };
      })
      .then(json => dispatch(responseUploadWallPicture(wallId, json)))
      .then(() => {
        ReactGA.event({
          category: 'Admin',
          action: 'Added wall picture',
          locationId: locationId
        });
      });
  };
}

export function deleteArea(locationId, areaId) {
  return dispatch => {
    dispatch(requestDeleteArea(locationId, areaId));
    return fetch('/private/area/' + areaId, auth({
        method: 'DELETE',
        headers: {
          'Content-Type': 'application/json'
        }
      }))
      .then(response => {
        if (response.ok) {
          return dispatch(deleteAreaOnSuccess(locationId, areaId))
        }
        // eslint-disable-next-line
        throw { statusText: response.statusText, status: response.status };
      })
      .then(() => {
        ReactGA.event({
          category: 'Admin',
          action: 'Removed area',
          locationId: locationId
        });
      });
  };
}

export function deleteWall(locationId, areaId, wallId) {
  return dispatch => {
    dispatch(requestDeleteWall());
    return fetch('/private/wall/' + wallId, auth({
        method: 'DELETE',
        headers: {
          'Content-Type': 'application/json'
        }
      }))
      .then(response => {
        if (response.ok) {
          return dispatch(deleteWallOnSuccess(areaId, wallId))
        } else {
          // eslint-disable-next-line
          throw { statusText: response.statusText, status: response.status };
        }
      })
      .then(() => {
        ReactGA.event({
          category: 'Admin',
          action: 'Removed wall',
          locationId: locationId
        });
      })
      .catch(error => {
        dispatch(requestFailed(error.statusText, error.status));
        throw error;
      });
  };
}

export function deleteRoute(route, locationId) {
  const { wallId, routeId } = route;
  return dispatch => {
    return fetch('/private/route/', auth({
        method: 'PUT',
        headers: {
          'Content-Type': 'application/json'
        },
        body: JSON.stringify(route)
      }))
      .then(response => {
        if (response.ok) {
          dispatch(deleteRouteOnSuccess(wallId, routeId))
        } else {
          // eslint-disable-next-line
          throw { statusText: response.statusText, status: response.status };
        }
      })
      .then(() => {
        ReactGA.event({
          category: 'Admin',
          action: 'Removed route',
          locationId: locationId
        });
      })
      .catch(error => {
        dispatch(requestFailed(error.statusText, error.status));
        throw error;
      });
  };
}

export function undoDeleteRoute(route, locationId) {
  return dispatch => {
    return fetch('/private/route/', auth({
        method: 'PUT',
        headers: {
          'Content-Type': 'application/json'
        },
        body: JSON.stringify(route)
      }))
      .then(response => {
        if (response.ok) {
          return response.json();
        } else {
          // eslint-disable-next-line
          throw { statusText: response.statusText, status: response.status };
        }
      })
      .then(data => dispatch(postNewRouteSuccess(data)))
      .then(() => {
        ReactGA.event({
          category: 'Admin',
          action: 'Undid remove route',
          locationId: locationId
        });
      })
      .catch(error => {
        dispatch(requestFailed(error.statusText, error.status));
        throw error;
      });
  };
}

export function undoLastDelete(wallId, locationId) {
  return dispatch => {
    return fetch('/private/route/undoLastDelete/' + wallId, auth({
        method: 'POST',
        headers: {
          'Content-Type': 'application/json'
        }
      }))
      .then(response => {
        if (response.ok) {
          return response.json();
        } else {
          // eslint-disable-next-line
          throw { statusText: response.statusText, status: response.status };
        }
      })
      .then(data => dispatch(postNewRouteSuccess(data)))
      .then(() => {
        ReactGA.event({
          category: 'Admin',
          action: 'Undo last delete',
          locationId: locationId
        });
      })
      .catch(error => {
        dispatch(requestFailed(error.statusText, error.status));
        throw error;
      });
  };
}

export function loadWallTopoPdf(wallId) {
  return dispatch => {
    return fetch('/private/print/wall/' + wallId, auth())
      .then(response => {
        if (response.ok) {
          return response.blob();
        } else {
          // eslint-disable-next-line
          throw { statusText: response.statusText, status: response.status };
        }
      });
  };
}

export function getRouteTags(routeId) {
  return dispatch => {
    dispatch(requestRouteTags());
    return fetch('/private/tag/ROUTE/' + routeId, auth())
      .then(response => response.json())
      .then(json => dispatch(getRouteTagsOnSuccess(routeId, json)));
  };
}

export function moveWallUp(wallId, locationId) {
  return moveWall('up', wallId, locationId);
}

export function moveWallDown(wallId, locationId) {
  return moveWall('down', wallId, locationId);
}

function moveWall(direction, wallId, locationId) {
  return dispatch => {
    return fetch(`/private/wall/order/${direction}/${wallId}`, auth({
        method: 'PUT',
        headers: {
          'Content-Type': 'application/json'
        }
      }))
      .then(response => {
        if (response.ok) {
          return response.json();
        } else {
          // eslint-disable-next-line
          throw { statusText: response.statusText, status: response.status };
        }
      })
      .then(data => dispatch(moveWallSuccess(data)))
      .then(() => {
        ReactGA.event({
          category: 'Admin',
          action: `Move wall ${direction}`,
          locationId: locationId
        });
      })
      .catch(error => {
        dispatch(requestFailed(error.statusText, error.status));
        throw error;
      });
  };
}

export function moveAreaUp(areaId, locationId) {
  return moveArea('up', areaId, locationId);
}

export function moveAreaDown(areaId, locationId) {
  return moveArea('down', areaId, locationId);
}

function moveArea(direction, areaId, locationId) {
  return dispatch => {
    return fetch(`/private/area/order/${direction}/${areaId}`, auth({
        method: 'PUT',
        headers: {
          'Content-Type': 'application/json'
        }
      }))
      .then(response => {
        if (response.ok) {
          return response.json();
        } else {
          // eslint-disable-next-line
          throw { statusText: response.statusText, status: response.status };
        }
      })
      .then(data => dispatch(moveAreaSuccess(data)))
      .then(() => {
        ReactGA.event({
          category: 'Admin',
          action: `Move area ${direction}`,
          locationId: locationId
        });
      })
      .catch(error => {
        dispatch(requestFailed(error.statusText, error.status));
        throw error;
      });
  };
}
