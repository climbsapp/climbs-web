import fetch from 'isomorphic-fetch';
import { requestFailed } from './exception';
import { auth } from '../helpers/fetch';
import { logout } from './login';

export const REQUEST_PROFILE = 'REQUEST_PROFILE';
export const RECEIVE_PROFILE = 'RECEIVE_PROFILE';
export const UPDATE_PROFILE = 'UPDATE_PROFILE';
export const FINISH_UPDATE_PROFILE = 'FINISH_UPDATE_PROFILE';

export const REQ_CHANGE_PASSWORD = 'REQ_CHANGE_PASSWORD',
             RES_CHANGE_PASSWORD = 'RES_CHANGE_PASSWORD',
             ERR_CHANGE_PASSWORD = 'ERR_CHANGE_PASSWORD';

export function requestProfile() {
  return {
    type: REQUEST_PROFILE
  };
}

export function receiveProfile(profileData) {
  return {
    type: RECEIVE_PROFILE,
    profile: profileData
  };
}

export function beginUpdateProfile() {
  return {
    type: UPDATE_PROFILE
  };
}

function requestChangePassword() {
  return {
    type: REQ_CHANGE_PASSWORD
  };
}

function successChangePassword() {
  return {
    type: RES_CHANGE_PASSWORD
  };
}

function errorChangePassword(error) {
  return {
    type: ERR_CHANGE_PASSWORD,
    error: error
  };
}

export function finishUpdateProfile(success, error) {
  return {
    type: FINISH_UPDATE_PROFILE,
    success: success,
    error: error
  };
}

export function fetchProfile() {
  return dispatch => {
    // Dispatch action that we are requesting profile
    dispatch(requestProfile());

    // Request the profile
    return fetch('/private/user', auth())
      .then(response => {
        if (response.ok) {
          return response.json();
        } else if (response.status === 401) {
          return logout()(dispatch);
        }
        // eslint-disable-next-line
        throw { statusText: response.statusText, status: response.status };
      })
      .then(json => dispatch(receiveProfile(json)))
      .catch(error => {
        dispatch(requestFailed(error.statusText, error.status));
      });
  };
}

export function updateProfile(profileData) {
  return dispatch => {
    dispatch(beginUpdateProfile());

    return fetch('/private/user', auth({
      method: 'PUT',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(profileData)
    })).then(response => response.json())
       .then(json => dispatch(receiveProfile(json)))
       .then(() => dispatch(finishUpdateProfile(true))); // TODO: Also handle failure
  };
}

export function changePassword(oldPassword, newPassword) {
  return dispatch => {
    dispatch(requestChangePassword());
    return fetch('/private/user/changePassword', auth({
      method: 'PUT',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({ oldPassword: oldPassword, newPassword: newPassword })
    })).then(response => response.json())
       .then(json => {
         if (json.errorCode) {
           dispatch(errorChangePassword(json));
           throw json;
         } else {
           dispatch(successChangePassword());
           return true;
         }
       })
       .catch(error => {
         dispatch(errorChangePassword(error));
         throw error;
       });
  };
}
