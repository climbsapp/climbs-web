import fetch from 'isomorphic-fetch';

export const REQUEST_BOARD = 'REQUEST_BOARD';
export const RECEIVE_BOARD = 'RECEIVE_BOARD';

export function requestBoard(leaderboardType) {
  return {
    type: REQUEST_BOARD,
    leaderboardType: leaderboardType
  };
}

export function receiveBoard(leaderboardType, leaderboardData) {
  return {
    type: RECEIVE_BOARD,
    leaderboardType: leaderboardType,
    leaderboardData: leaderboardData
  };
}

export function fetchLeaderboardSendsInPastYear() {
  return fetchBoard('SENDS_IN_PAST_YEAR');
}

export function fetchLeaderboardSendsInPastMonth() {
  return fetchBoard('SENDS_IN_PAST_MONTH');
}

export function fetchLeaderboardSendsInPastWeek() {
  return fetchBoard('SENDS_IN_PAST_WEEK');
}

function fetchBoard(leaderboardType) {
  return dispatch => {
    // Dispatch action that we are requesting a leaderboard
    dispatch(requestBoard(leaderboardType));

    let url;
    if (leaderboardType === 'SENDS_IN_PAST_YEAR') {
      url = '/private/leaderboards/basic?locationId=1&days=365&distinct=null';
    } else if (leaderboardType === 'SENDS_IN_PAST_MONTH') {
      url = '/private/leaderboards/basic?locationId=1&days=30&distinct=null';
    } else if (leaderboardType === 'SENDS_IN_PAST_WEEK') {
      url = '/private/leaderboards/basic?locationId=1&days=7&distinct=null';
    }

    // Request the leaderboard
    return fetch(url, { credentials: 'include' })
      .then(response => response.json())
      .then(json => dispatch(receiveBoard(leaderboardType, json)));
  };
}
