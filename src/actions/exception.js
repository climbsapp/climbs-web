export const REQUEST_EXCEPTION = 'REQUEST_EXCEPTION';
export const RESET_ERROR_TEXT = 'RESET_ERROR_TEXT';

export function requestFailed(statusText, statusCode) {
  return {
    type: REQUEST_EXCEPTION,
    errorText: statusText,
    errorCode: statusCode
  };
}

export function resetErrorText() {
  return {
    type: RESET_ERROR_TEXT
  };
}
