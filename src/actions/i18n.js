import fetch from 'isomorphic-fetch';
import { auth } from '../helpers/fetch';
export const RES_LOCALIZATION = "RES_LOCALIZATION";

function receiveLocalization(locale, catalog) {
  return {
    type: RES_LOCALIZATION,
    locale: locale,
    catalog: catalog
  };
}

export function loadLocalization(locale) {
  return dispatch => {
    return fetch(process.env.PUBLIC_URL + '/i18n/' + locale + '.json', auth({
          method: 'GET',
          headers: {
            'Content-Type': 'application/json'
          }
        }))
        .then(response => response.json())
        .then(catalog => dispatch(receiveLocalization(locale, catalog)));
  };
}
