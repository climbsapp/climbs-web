import fetch from 'isomorphic-fetch';
import ReactGA from 'react-ga';
import { auth } from '../helpers/fetch';

export const REQUEST_POST_LOCATION = 'REQUEST_POST_LOCATION',
             RESPONSE_POST_LOCATION = 'RESPONSE_POST_LOCATION';

export function requestPostLocation(locationData) {
  return {
    type: REQUEST_POST_LOCATION,
    data: locationData
  };
}

export function responsePostLocation(locationData) {
  return {
    type: RESPONSE_POST_LOCATION,
    data: locationData
  };
}

export function postLocation(data, successCb) {
  return (dispatch) => {
    dispatch(requestPostLocation(data));
    return fetch('/private/locationTemplate', auth({
        method: 'POST',
        headers: {
          'Content-Type': 'application/json'
        },
        body: JSON.stringify(data)
      }))
      .then(response => response.json())
      .then(json => {
        dispatch(responsePostLocation(json));
        return json;
      })
      .then(newLocation => successCb(newLocation))
      .then(() => {
        ReactGA.event({
          category: 'User',
          action: 'Created location'
        });
      });
  };
}
