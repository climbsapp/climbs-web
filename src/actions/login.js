import fetch from 'isomorphic-fetch';
import { notify } from './notifications';

export const REQUEST_PROFILE = 'REQUEST_PROFILE';
export const RECEIVE_PROFILE = 'RECEIVE_PROFILE';

export const AUTHENTICATE = 'AUTHENTICATE';
export const AUTHENTICATE_FACEBOOK = 'AUTHENTICATE_FACEBOOK';
export const AUTHENTICATE_SUCCESS = 'AUTHENTICATE_SUCCESS';
export const AUTHENTICATE_FAILURE = 'AUTHENTICATE_FAILURE';

export const REGISTER = 'REGISTER';
export const REGISTER_SUCCESS = 'REGISTER_SUCCESS';
export const REGISTER_FAILURE = 'REGISTER_FAILURE';

export const REQUEST_PASSWORD = 'REQUEST_PASSWORD';
export const REQUEST_PASSWORD_SUCCESS = 'REQUEST_PASSWORD_SUCCESS';
export const REQUEST_PASSWORD_FAILURE = 'REQUEST_PASSWORD_FAILURE';

export const LOGOUT = 'LOGOUT';

export function requestProfile() {
  return { type: REQUEST_PROFILE };
}

export function requestRegister() {
  return { type: REGISTER };
}

export function requestPasswordReset() {
  return { type: REQUEST_PASSWORD };
}

export function requestPasswordResetSuccess() {
  return { type: REQUEST_PASSWORD_SUCCESS };
}

export function requestPasswordResetFailed() {
  return { type: REQUEST_PASSWORD_FAILURE };
}

export function receiveProfile(profileData) {
  return {
    type: RECEIVE_PROFILE,
    profile: profileData
  };
}

export function receiveProfileFailed() {
  return {
    type: RECEIVE_PROFILE,
    profile: null
  };
}

export function requestAuthenticate() {
  return { type: AUTHENTICATE };
}

export function requestAuthenticateFacebook() {
  return { type: AUTHENTICATE_FACEBOOK };
}

export function responseAuthenticateSuccess(token) {
  return { type: AUTHENTICATE_SUCCESS, data: token };
}

export function responseAuthenticateFailure() {
  return { type: AUTHENTICATE_FAILURE };
}

export function registerFailed() {
  return { type: REGISTER_FAILURE };
}

export function requestLogout() {
  return { type: LOGOUT };
}

export function logout() {
  return dispatch => {
    dispatch(requestLogout());
  }
}

export function authenticateFacebook() {
  return dispatch => {
    dispatch(requestAuthenticateFacebook());
  };
}

export function authenticate(credentials) {
  return dispatch => {
    dispatch(requestAuthenticate());
    return fetch('/private/authenticate', {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify(credentials)
      })
      .then(response => {
        if (response.ok) {
          return response.text();
        } else {
          throw Error("Login failed");
        }
      })
      .then((token) => dispatch(responseAuthenticateSuccess(token)))
      .catch(error => {
        dispatch(responseAuthenticateFailure());
        throw error;
      });
  };
}

export function register(account) {
  return dispatch => {
    dispatch(requestRegister());
    return fetch('/public/registerUser', {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify(account)
      })
      .then(response => {
        if (response.ok) {
          const { email, password } = account;
          return authenticate({ username: email , password: password })(dispatch);
        } else {
          return response.json();
        }
      })
      .then(data => {
        if (data.errorCode) {
          throw data;
        } else {
          return data;
        }
      })
      .catch(error => {
        dispatch(registerFailed());
        throw error;
      });
  };
}

export function requestPassword(email) {
  return dispatch => {
    dispatch(requestPasswordReset());
    return fetch('/public/passwordToken/' + encodeURIComponent(email), {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' }
      })
      .then(response => {
        if (response.ok) {
          dispatch(requestPasswordResetSuccess());
          return response.text(); // Backend responds with text only
        } else {
          return response.json();
        }
      })
      .then(data => {
        if (data.errorCode) {
          throw data;
        } else {
          dispatch(notify(data));
          return data;
        }
      })
      .catch(error => {
        dispatch(requestPasswordResetFailed());
        throw error;
      })
  };
}
