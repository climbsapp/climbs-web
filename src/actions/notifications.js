export const ADD_NOTIFICATION = 'ADD_NOTIFICATION';
export const REMOVE_NOTIFICATION = 'REMOVE_NOTIFICATION';

function addNotification(id, text, options) {
  return {
    type: ADD_NOTIFICATION,
    id: id,
    text: text,
    options: options
  };
}

function removeNotification(id) {
  return {
    type: REMOVE_NOTIFICATION,
    id: id
  };
}

export function notify(text, options = {}) {
  return (dispatch) => {
    const id = Date.now() + Math.floor(Math.random() * 1000);
    dispatch(addNotification(id, text, options));
    setTimeout(() => dispatch(removeNotification(id)), 5000);
    return id;
  };
}

export function unNotify(id) {
  return dispatch => dispatch(removeNotification(id));
}
