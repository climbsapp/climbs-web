import fetch from 'isomorphic-fetch';
import { auth } from '../helpers/fetch';
import { requestFailed } from './exception';

export const REQUEST_ALL_GRADES_DATA = 'REQUEST_ALL_GRADES_DATA',
             RESPONSE_ALL_GRADES_DATA = 'RESPONSE_ALL_GRADES_DATA';

export const REQUEST_ROUTESETTERS_DATA = 'REQUEST_ROUTESETTERS_DATA',
             RESPONSE_ROUTESETTERS_DATA = 'RESPONSE_ROUTESETTERS_DATA';

export const REQUEST_ROUTES_BY_STYLE_DATA = 'REQUEST_ROUTES_BY_STYLE_DATA',
             RESPONSE_ROUTES_BY_STYLE_DATA = 'RESPONSE_ROUTES_BY_STYLE_DATA';

export const REQUEST_ROUTE_SENDS = 'REQUEST_ROUTE_SENDS',
             RESPONSE_ROUTE_SENDS = 'RESPONSE_ROUTE_SENDS';

function requestRouteSends(routeId) {
  return {
    type: REQUEST_ROUTE_SENDS,
    routeId: routeId
  };
}

function responseRouteSends(routeId, data) {
  return {
    type: RESPONSE_ROUTE_SENDS,
    routeId: routeId,
    data: data
  };
}

export function requestRoutesByStyleData(locationId) {
  return {
    type: REQUEST_ROUTES_BY_STYLE_DATA,
    locationId: locationId
  };
}

export function responseRoutesByStyleData(locationId, data) {
 return {
   type: RESPONSE_ROUTES_BY_STYLE_DATA,
   locationId: locationId,
   data: data
 };
}

export function requestRouteSettersData(locationId) {
  return {
    type: REQUEST_ROUTESETTERS_DATA,
    locationId: locationId
  };
}

export function responseRouteSettersData(locationId, data) {
  return {
    type: RESPONSE_ROUTESETTERS_DATA,
    locationId: locationId,
    data: data
  };
}

export function requestAllGradesData(locationId) {
 return {
   type: REQUEST_ALL_GRADES_DATA,
   locationId: locationId
 };
}

export function responseAllGradesData(locationId, data) {
 return {
   type: RESPONSE_ALL_GRADES_DATA,
   locationId: locationId,
   data: data
 };
}

export function fetchGradeDistributionChart(locationId) {
  return (dispatch) => {
    dispatch(requestAllGradesData(locationId));
    return fetch('/private/chart/locationSends/' + locationId, auth({
        method: 'GET',
        headers: {
          'Content-Type': 'application/json'
        }
      }))
      .then(response => response.json())
      .then(json => dispatch(responseAllGradesData(locationId, json)));
  };
}

export function fetchRouteSettingChart(locationId) {
  return (dispatch) => {
    dispatch(requestRouteSettersData(locationId));
    return fetch('/private/chart/routeSettersByMonth/' + locationId, auth({
        method: 'GET',
        headers: {
          'Content-Type': 'application/json'
        }
      }))
      .then(response => response.json())
      .then(json => dispatch(responseRouteSettersData(locationId, json)));
  };
}

export function fetchRoutesByStyleChart(locationId) {
  return (dispatch) => {
    dispatch(requestRoutesByStyleData(locationId));
    return fetch('/private/chart/locationRouteStyles/' + locationId, auth({
        method: 'GET',
        headers: {
          'Content-Type': 'application/json',
          'Authorization': 'Bearer ' + localStorage.getItem('jwtToken')
        }
      }))
      .then(response => response.json())
      .then(json => dispatch(responseRoutesByStyleData(locationId, json)));
  };
}

export function fetchRouteSends(routeId) {
  return dispatch => {
    // Dispatch action that we are requesting locations
    dispatch(requestRouteSends());

    return fetch(`/private/routeSendSeries/${routeId}`, auth())
      .then(response => {
        if (response.ok) {
          return response.json();
        }
        // eslint-disable-next-line
        throw { statusText: response.statusText, status: response.status };
      })
      .then(json => dispatch(responseRouteSends(routeId, json)))
      .catch(error => {
        dispatch(requestFailed(error.statusText, error.status));
      });
  };
}
